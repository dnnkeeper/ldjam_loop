﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

using UnityEditor;
using UnityEditor.Animations;
using Unity.EditorCoroutines.Editor;

public class DistanceCurveBuilder : EditorWindow
{
    public Transform transform;
    public List<Vector3> rootMotion = new List<Vector3>();
    public AnimationCurve distanceCurve;
    public List<Vector3> leftFootPositions = new List<Vector3>();
    public List<Vector3> rightFootPositions = new List<Vector3>();
    public float targetTime;

    public Transform leftFootTransform, rightFootTransform;
    public bool leftFootDown, rightFootDown;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/DistanceCurveBuilder")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        DistanceCurveBuilder window = (DistanceCurveBuilder)EditorWindow.GetWindow(typeof(DistanceCurveBuilder));
        window.Show();
    }

    Animator animator;
    AnimatorState selectedState;
    public bool reverse;
    public bool optimize;
    void OnGUI()
    {
        GUILayout.Label("Distance Curve Builder", EditorStyles.boldLabel);
        
        animator = (Animator)EditorGUILayout.ObjectField("Animator", animator, typeof(Animator), true);
        
        if (animator == null)
        {
            if (Selection.activeGameObject != null)
            {
                var selectedAnimator = Selection.activeGameObject.GetComponent<Animator>();
                if (selectedAnimator != null)
                {
                    animator = selectedAnimator;
                    if (selectedAnimator.isHuman) {
                        leftFootTransform = selectedAnimator.GetBoneTransform(HumanBodyBones.LeftFoot);
                        rightFootTransform = selectedAnimator.GetBoneTransform(HumanBodyBones.RightFoot);
                    }
                }
            }
        }

        if (animator != null)
        {
            transform = animator.transform;

            leftFootTransform = (Transform)EditorGUILayout.ObjectField("leftFootTransform", leftFootTransform, typeof(Transform), true);
            rightFootTransform = (Transform)EditorGUILayout.ObjectField("rightFootTransform", rightFootTransform, typeof(Transform), true);
            floorPositionY = EditorGUILayout.FloatField("Floor Height", floorPositionY);
            
            reverse = EditorGUILayout.Toggle("Reverse", reverse);

            selectedState = (AnimatorState)EditorGUILayout.ObjectField("Animator State", selectedState, typeof(Transform), true);
            if (Selection.activeObject != null)
            {
                optimize = EditorGUILayout.Toggle("Optimize Curve", optimize);
                //GUILayout.Label(Selection.activeObject.name + " type: " + Selection.activeObject.GetType());

                selectedState = Selection.activeObject as AnimatorState;

                //if (selectedState != null && selectedState.motion != null)
                //{
                //    GUILayout.Label(" motion: " + selectedState.motion.name);
                //}
            }

            if (selectedState != null && selectedState.motion != null && GUILayout.Button("Build Curve")){

                BuildCurve();
            }
            
        }

        //AnimationClip clip = Selection.activeObject as AnimationClip;

        //if (Selection.activeObject != null && clip != null)
        //{
        //    if (GUILayout.Button("Create"))
        //    {
        //         GameObject.Instantiate(clip);
        //    }
        //}
    }

    float directionSign;
    IEnumerator BuildCurve_Routine()
    {
        Animator animator = transform.GetComponent<Animator>();

        var runtimeController = animator.runtimeAnimatorController as AnimatorController;

        Debug.Log(string.Format("Layer Count: {0}", animator.layerCount));

        //for (int i = 0; i < animator.layerCount; i++)
        //{
        //    Debug.Log(string.Format("Layer {0}: {1}", i, animator.GetLayerName(i)));
        //    Debug.Log("---");
        //}
        var runtimeControllerLayers = runtimeController.layers;
        int stateLayerIndex = 0;
        string stateName = selectedState.name;
        //AnimatorState state = null;
        //foreach (AnimatorControllerLayer layer in runtimeControllerLayers) //for each layer
        //{
        //    foreach (ChildAnimatorState childState in layer.stateMachine.states) //for each state
        //    {
        //        state = childState.state;
                
        //        Debug.Log("Added "+ stateLayerIndex + " "+ layer.name + "." + childState.state.name);

        //        stateName = childState.state.name;
        //        break;
        //    }

        //    //use first valid state
        //    if (state != null)
        //        break;

        //    stateLayerIndex++;
        //}
        //animator.Play(stateName, stateLayerIndex);
        leftFootPositions.Clear();
        
        rightFootPositions.Clear();
        
        rootMotion.Clear();

        directionSign = +1f;

        int directionChangeFrames = 0;

        Vector3 rootMotionOffset = Vector3.zero;
        
        distanceCurve = new AnimationCurve();
        
        var speedCurve = new AnimationCurve();

        Vector3 initialPosition = transform.position;
        
        float t = 0;
        
        Debug.Log("animator.Play " + stateName + " " + stateLayerIndex);
        
        animator.StopPlayback();

        yield return null;

        animator.Play(stateName, stateLayerIndex, 0);

        yield return null;

        float clipLength = 0;

        AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
        foreach (AnimationClip clip in clips)
        {
            if (clip.name == selectedState.motion.name)
            {
                Debug.Log(clip.name+" length = " + clip.length);
                clipLength = clip.length;
                break;
            }
        }


        var stateInfo = animator.GetCurrentAnimatorStateInfo(stateLayerIndex);

        Debug.Log("stateInfo.length = "+ stateInfo.length);
        
        Vector3 leftFootPosOld = transform.InverseTransformPoint(leftFootTransform.position);

        Vector3 rightFootPosOld = transform.InverseTransformPoint(rightFootTransform.position);

        Vector3 velocity = Vector3.zero;

        int keyframeIndex = 0;

        float lastDistanceFromStart = 0f;

        float lastDelta = 0f;

        distanceCurve.AddKey(0, 0);

        speedCurve.AddKey(0, 0);

        float distanceFromStart = 0f;

        while (t <= clipLength)
        {
            float dt = 1f/60f;

            t += dt;

            Debug.Log(t);

            animator.Update(dt);

            Vector3 leftFootPos = transform.InverseTransformPoint(leftFootTransform.position);

            Vector3 rightFootPos = transform.InverseTransformPoint(rightFootTransform.position);

            leftFootDown = leftFootPos.y < floorPositionY;

            rightFootDown = rightFootPos.y < floorPositionY;

            Vector3 offset = Vector3.zero;

            if (leftFootDown || rightFootDown)
            {
                Vector3 diffLeft = leftFootDown? transform.TransformVector( (leftFootPosOld - leftFootPos)) : Vector3.zero;

                Vector3 diffRight = rightFootDown? transform.TransformVector( (rightFootPosOld - rightFootPos)) : Vector3.zero;

                Vector3 diff = (diffLeft.sqrMagnitude > diffRight.sqrMagnitude) ? diffLeft : diffRight;

                offset = Vector3.ProjectOnPlane(diff, transform.up);

                velocity = Vector3.Lerp(velocity, offset, 3f * dt);
            }
            else
            {
                transform.position += velocity;

                velocity = Vector3.Lerp(velocity, Vector3.zero, 3f * dt);

                offset = velocity;
            }

            offset = Vector3.Project(offset, animator.transform.forward);

            transform.position += offset;

            leftFootPosOld = leftFootPos;

            rightFootPosOld = rightFootPos;

            leftFootPositions.Add(leftFootTransform.position);

            rightFootPositions.Add(rightFootTransform.position);

            float sign = Mathf.Sign(Vector3.Dot(animator.transform.forward, offset));

            distanceFromStart = distanceFromStart + offset.magnitude; //(transform.position - initialPosition).magnitude;

            if (!optimize || Mathf.Abs(distanceFromStart - lastDistanceFromStart + lastDelta) > 0.2f)
            {
                if (directionSign != sign)
                {
                    directionChangeFrames++;

                    if (directionChangeFrames > 3)
                    {
                        directionSign = sign;

                        Debug.Log("<color=red>directionSign changed to " + (directionSign * offset.magnitude) + " </color> at " + keyframeIndex);

                        Keyframe[] newKeys = new Keyframe[keyframeIndex];

                        for (int i = 0; i < keyframeIndex; i++)
                        {
                            float v = (distanceCurve[i].value - distanceFromStart);
                            Debug.Log("keyframeIndex " + i + " <color=red> " + distanceCurve[i].time+ "</color > = " + v);
                            //distanceCurve.AddKey(distanceCurve[i].time, v);
                            //Debug.Log(distanceCurve[i].value);
                            //distanceCurve.keys[i] = new Keyframe(distanceCurve[i].time, v);
                            newKeys[i] = new Keyframe(distanceCurve[i].time, v);
                        }

                        distanceCurve.keys = newKeys;

                        distanceFromStart = 0f;
                    }
                }
                else
                    directionChangeFrames = 0;

                lastDelta = distanceFromStart - lastDistanceFromStart;
                lastDistanceFromStart = distanceFromStart;
                keyframeIndex++;
                Debug.Log("<color=green> " + t + "</color> " + distanceFromStart);
                distanceCurve.AddKey(t, distanceFromStart);

                speedCurve.AddKey(t, lastDelta);
            }

            yield return new EditorWaitForSeconds(dt);
        }

        distanceCurve.AddKey(t, distanceFromStart);

        for (int i = 0; i < distanceCurve.length; i++)
        {
            AnimationUtility.SetKeyLeftTangentMode(distanceCurve, i, AnimationUtility.TangentMode.Linear);

            AnimationUtility.SetKeyRightTangentMode(distanceCurve, i, AnimationUtility.TangentMode.Linear);
        }

        // for (int i = 1; i <= 100; i++)
        // {
        //     animator.Play(stateName, stateLayerIndex, i / 100f);
        //     animator.Update(0);
        //     Vector3 leftFootPos = animator.GetBoneTransform(HumanBodyBones.LeftFoot).position;
        //     Vector3 leftFootPosOld = leftFootPositions.Count > 1 ? leftFootPositions[leftFootPositions.Count - 2] : leftFootPos;

        //     if (leftFootPos.y < floorPositionY)
        //     {
        //         leftFootPositions.Add(leftFootPosOld);

        //         rootMotionOffset = leftFootPosOld - leftFootPos;

        //         rootMotion.Add(rootMotionOffset);

        //         rootMotionOffsetDistance = rootMotionOffset.magnitude;

        //         distanceCurve.AddKey(i, rootMotionOffsetDistance);
        //     }
        //     else
        //     {
        //         rootMotion.Add(Vector3.zero);

        //         leftFootPositions.Add(rootMotionOffset + leftFootPos);
        //     }
        // }

        Debug.Log("play end "+t+">"+stateInfo.length);

        transform.position = initialPosition;

        leftFootDown = false;

        rightFootDown = false;
        
        yield return null;

        string assetPath = AssetDatabase.GetAssetPath(selectedState.motion);

        int nameIndex = assetPath.LastIndexOf('/');

        string baseAssetPath = assetPath.Substring(0, nameIndex);

        string baseAssetName = assetPath.Split('/').Last<string>().Split('.').First<string>();

        string newPath = baseAssetPath + "/" + MakeValidFileName(baseAssetName+"-"+ selectedState.motion.name) + "_DistanceCurve.asset";

        //Directory.CreateDirectory(baseAssetPath + "/" + baseAssetName + "_DistanceCurves");

        var curveObject = AssetDatabase.LoadAssetAtPath<AnimationCurveObject>(newPath);

        if (curveObject == null)
        {
            curveObject = ScriptableObject.CreateInstance<AnimationCurveObject>();

            AssetDatabase.CreateAsset(curveObject, newPath);

            Selection.activeObject = curveObject;
        }

        //float startingValue = distanceCurve[0].value;

        if (reverse)
        {
            Keyframe[] newKeys = new Keyframe[keyframeIndex];

            for (int i = 0; i < keyframeIndex; i++)
            {
                float v = (distanceCurve[i].value - distanceFromStart);
                newKeys[i] = new Keyframe(distanceCurve[i].time, v);
            }

            distanceCurve.keys = newKeys;
        }
        //for (int i = 0; i < distanceCurve.length; i++)
        //{
        //    distanceCurve.AddKey(distanceCurve[i].time, Mathf.Abs(distanceCurve[i].value - startingValue));
        //}

        curveObject.distanceCurve = distanceCurve;

        curveObject.speedCurve = speedCurve;

        EditorUtility.SetDirty(curveObject);

        AssetDatabase.SaveAssets();

        //importer.SaveAndReimport();
    }

    [ContextMenu("Build Curve")]
    public void BuildCurve()
    {
        EditorCoroutineUtility.StartCoroutine(BuildCurve_Routine(), this);
    }

    private static string MakeValidFileName(string name)
    {
        string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
        string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

        return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
    }

    public float floorPositionY = 0f;
    /// <summary>
    /// Callback to draw gizmos only if the object is selected.
    /// </summary>

    // Window has been selected
    void OnFocus()
    {
        // Remove delegate listener if it has previously
        // been assigned.
        SceneView.duringSceneGui -= this.OnSceneGUI;
        // Add (or re-add) the delegate.
        SceneView.duringSceneGui += this.OnSceneGUI;
    }

    void OnDestroy()
    {
        // When the window is destroyed, remove the delegate
        // so that it will no longer do any drawing.
        SceneView.duringSceneGui -= this.OnSceneGUI;
    }

    void OnSceneGUI(SceneView sceneView)
    {        
        //Handles.BeginGUI();
        if (transform != null)
        {
            //Handles.Label(transform.position, "LABEL TEXT");
            //Debug.LogError("Draw");
            // Do your drawing here using Handles.

            Vector3 floorPosition = new Vector3(0, floorPositionY, 0);
            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;
            //Handles.color = new Color(0, 0, 1, 1f);
            Vector3 p = transform.TransformPoint(floorPosition);
            float range = 1f;
            Vector3[] verts = new Vector3[]
            {
                new Vector3(p.x - range, p.y, p.z - range),
                new Vector3(p.x - range, p.y, p.z + range),
                new Vector3(p.x + range, p.y, p.z + range),
                new Vector3(p.x + range, p.y, p.z - range)
            };

            Handles.DrawSolidRectangleWithOutline(verts, new Color(0f, 0f, 1f, 0.25f), new Color(0, 0, 0, 1));
            //(0, transform.TransformPoint(floorPosition), transform.rotation, 1f, EventType.Repaint);

            if (leftFootPositions != null)
            {
                Vector3 pos = Vector3.zero;
                Handles.color = Color.yellow;
                for (int i = 0; i < leftFootPositions.Count; i++)
                {
                    Vector3 lastPos = pos;
                    //Vector3 rootMotionOffset = rootMotion.Count > i ? rootMotion[i] : Vector3.zero;
                    pos = (leftFootPositions[i]);
                    Handles.SphereHandleCap(0, pos, Quaternion.identity, 0.005f, EventType.Repaint);
                    if (lastPos != pos && lastPos.sqrMagnitude > 0.01f)
                    {
                        Handles.DrawLine(lastPos, pos);
                    }
                }
                pos = Vector3.zero;
                Handles.color = Color.red;
                for (int i = 0; i < rightFootPositions.Count; i++)
                {
                    Vector3 lastPos = pos;
                    //Vector3 rootMotionOffset = rootMotion.Count > i ? rootMotion[i] : Vector3.zero;
                    pos = (rightFootPositions[i]);
                    Handles.SphereHandleCap(0, pos, Quaternion.identity, 0.005f, EventType.Repaint);
                    if (lastPos != pos && lastPos.sqrMagnitude > 0.01f)
                    {
                        Handles.DrawLine(lastPos, pos);
                    }
                }
            }
        }
        //Handles.EndGUI();
    }

    void OnDrawGizmosSelected()
    {
        Vector3 floorPosition = new Vector3(0, floorPositionY, 0);
        Gizmos.color = new Color(0, 0, 1, 0.5f);
        Gizmos.DrawCube( transform.TransformPoint(floorPosition), new Vector3(1, 0.002f, 1));
        
        if (leftFootPositions != null){
            Vector3 pos = Vector3.zero;
            Gizmos.color = Color.yellow;
            for (int i = 0; i < leftFootPositions.Count; i++)
            {
                Vector3 lastPos = pos;
                //Vector3 rootMotionOffset = rootMotion.Count > i ? rootMotion[i] : Vector3.zero;
                pos = (leftFootPositions[i]);
                Gizmos.DrawSphere(pos, 0.005f);
                if (lastPos != pos && lastPos.sqrMagnitude > 0.01f){
                    Gizmos.DrawLine(lastPos, pos);
                }
            }
            pos = Vector3.zero;
            Gizmos.color = Color.blue;
            for (int i = 0; i < rightFootPositions.Count; i++)
            {
                Vector3 lastPos = pos;
                //Vector3 rootMotionOffset = rootMotion.Count > i ? rootMotion[i] : Vector3.zero;
                pos = (rightFootPositions[i]);
                Gizmos.DrawSphere(pos, 0.005f);
                if (lastPos != pos && lastPos.sqrMagnitude > 0.01f)
                {
                    Gizmos.DrawLine(lastPos, pos);
                }
            }
        }
    }
}
