﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Reflection;
using System;

public class DistanceMatchState : StateMachineBehaviour
{

    public AnimationCurveObject distanceCurveObject;

    public float dampTime = 0f;

    public float maxTime = 1.0f;

    public void MatchDistance(Animator animator, int matchingTimeHash, AnimatorStateInfo animationState, int layer, float targetDistance, AnimationCurve distanceCurve, bool reversed = false, float dampTime = 0f)
    {
        float stateLength = distanceCurve.keys[distanceCurve.length - 1].time;

        if (float.IsInfinity(stateLength))
        {
            //Debug.LogWarning("stateLength is INFINITY");
            return;
        }

        /*

        var normalizedTime = 0f;//animationState.normalizedTime%1f;

        float currentDistance = 0f;//distanceCurveOnject.curve.Evaluate(normalizedTime);

        //Debug.Log(animationClip);

        if (targetDistance > currentDistance)
        {
            float delta = 0.032f;// / stateInfo.length;

            float targetTime = normalizedTime * stateLength;

            float maxTime = stateLength;

            //Debug.Log("targetTime : "+ targetTime+ "; stateInfo.length: "+ stateLength + "; maxTime: " + maxTime);

            while (targetDistance > currentDistance && targetTime <= maxTime)
            {
                targetTime += delta;

                currentDistance = distanceCurve.Evaluate(targetTime);

                //Debug.Log("targetTime: " + targetTime+" value: "+ currentDistance);
            }

            float targetNormalizedTime = targetTime / stateLength;

            //Debug.Log("targetTime: " + targetTime + " / "+ maxTime);

            //Debug.Log("Distance: " + currentDistance + " / "+ targetDistance);

            //Debug.Log("targetNormalizedTime: " + targetNormalizedTime+ " stateLength: "+ stateLength);

            //animator.ForceStateNormalizedTime(targetNormalizedTime);

            //animator.Play(animationState.shortNameHash, 0, targetNormalizedTime);

            //animator.Update(0f);

            animator.SetFloat(Animator.StringToHash("MatchingTime"), targetNormalizedTime);
        }
        */

        float normalizedTimeFromDistanceCurve = 0;
        //if (reversed)
        //{
        //    float lastValue = distanceCurve[distanceCurve.keys.Length - 1].value;
        //    targetNormalizedTime = distanceCurve.BinarySearch(lastValue-targetDistance) / stateLength;
        //}
        //else
        //{
        normalizedTimeFromDistanceCurve = distanceCurve.BinarySearch(targetDistance) / stateLength;
        //}
        if (normalizedTimeFromDistanceCurve >= 1f)
        {
            normalizedTimeFromDistanceCurve = Mathf.Clamp01(normalizedTimeFromDistanceCurve);

            //animator.GetComponent<DistanceMatching>().DistanceMatchEnd();
            //animator.SendMessage("DistanceMatchEnd", SendMessageOptions.DontRequireReceiver);
        }
        //animator.SetFloat(Animator.StringToHash("MatchingTime"), reversed? 1f - targetNormalizedTime : targetNormalizedTime, 0.1f, Time.deltaTime);
        float targetNormalizedTime = normalizedTimeFromDistanceCurve;

        if (normalizedTimeFromDistanceCurve < maxTime)
        {
            targetNormalizedTime = Mathf.Max(animator.GetFloat(matchingTimeHash), normalizedTimeFromDistanceCurve);

        }
        else
        {
            targetNormalizedTime = animator.GetFloat(matchingTimeHash) + Time.deltaTime / stateLength;
            //Debug.Log("targetNormalizedTime " + timeFromDistanceCurve);
        }

        animator.SetFloat(matchingTimeHash, targetNormalizedTime, dampTime, Time.deltaTime);
    }

    public float testValue;

    [ContextMenu("BinaryTest")]
    void BinaryTest()
    {
        Debug.Log(distanceCurveObject.distanceCurve.BinarySearch(testValue));
    }

    //public string behaviourName;

    public bool reverse;

    int matchingTimeHash;

    public string matchingTimeParameter = "MatchingTime";

    public string targetDistanceParameter = "TargetDistance";

    int targetDistanceHash;

    bool entered;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (Application.isPlaying)
            animator.SendMessage("OnDistanceMatchPredict", SendMessageOptions.DontRequireReceiver);

        entered = true;

        targetDistanceHash = Animator.StringToHash(targetDistanceParameter);

        matchingTimeHash = Animator.StringToHash(matchingTimeParameter);

        //Debug.Log("DistanceMatch Enter targetDistance: " + animator.GetFloat(targetDistanceHash) + " matchingTimeParameter: " + animator.GetFloat(matchingTimeParameter));

        animator.SetFloat(matchingTimeHash, 0f);

        animator.SetFloat(targetDistanceHash, reverse? -100f : 0f );
        //Dont change bool while in transition to prevent sudden animation flip
        //if (!animator.IsInTransition(0))
        //{
            CheckFootBool(animator);
        //}

        //Debug.Log("OnStateEnter");
        //animator.SendMessage("SetTargetDistanceHash", targetDistanceHash, SendMessageOptions.DontRequireReceiver);
        //animator.SendMessage("SetTargetTimeHash", matchingTimeHash, SendMessageOptions.DontRequireReceiver);
        if (!Application.isPlaying)
            return;
        animator.SendMessage("OnDistanceMatch", reverse, SendMessageOptions.DontRequireReceiver);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Debug.Log("OnStateExit");
        //animator.SendMessage("OnDistanceMatch", false, SendMessageOptions.DontRequireReceiver);
        CheckFootBool(animator);
        if (Application.isPlaying)
            animator.SendMessage("DistanceMatchEnd", SendMessageOptions.DontRequireReceiver);
        animator.SetFloat(matchingTimeHash, 0f);
    }
    void CheckFootBool(Animator animator)
    {
        float footPlant = animator.GetFloat(Animator.StringToHash("FootPlant"));
        if (footPlant > 0f)
        {
            animator.SetBool(Animator.StringToHash("FootPlantBool"), true);
        }
        else if (footPlant < 0f)
            animator.SetBool(Animator.StringToHash("FootPlantBool"), false);

    }
    public override void OnStateMove(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Debug.Log("OnStateMove "+ animatorStateInfo.normalizedTime);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        if (distanceCurveObject != null)
            MatchDistance(animator, matchingTimeHash, animatorStateInfo, layerIndex, animator.GetFloat(targetDistanceHash), distanceCurveObject.distanceCurve, reverse, dampTime);

        if (!Application.isPlaying)
            return;

        if (!entered)
        {
            Debug.LogError("State Update before entered!");
        }else
            animator.SendMessage("OnDistanceMatchUpdate", SendMessageOptions.DontRequireReceiver); 
    }

}
