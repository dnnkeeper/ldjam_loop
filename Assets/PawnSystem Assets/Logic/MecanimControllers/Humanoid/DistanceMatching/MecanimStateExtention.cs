﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MecanimStateExtention : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        this.animator = animator;
        Debug.Log("OnStateEnter "+animatorStateInfo.shortNameHash);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        Debug.Log("OnStateUpdate " + animatorStateInfo.shortNameHash);
    }

    public Animator animator;

    public string stateName;

    [ContextMenu("TestPlayback")]
    void TestPlayback()
    {
        animator.Play(stateName, 0, animator.GetCurrentAnimatorStateInfo(0).normalizedTime + 0.1f);
    }
}
