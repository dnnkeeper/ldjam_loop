﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*namespace BinarySearch
{
    class BinaryArray
    {
        private int[] data; // array of values
        private static System.Random generator = new System.Random();

        // Create an array of given size fill with random integers
        // Binary Search constructor
        public BinaryArray(int size)
        {
            data = new int[size]; // allocate space for array

            // fill array with random ints
            for (int i = 0; i < size; i++)
            {
                data[i] = generator.Next(10, 100);
            }
            Array.Sort(data);
        }

        // perform a binary search on array
        public int BinarySearch(int searchElement)
        {
            int low = 0; // 0 is always going to be the first element
            int high = data.Length - 1; // Find highest element
            int middle = (low + high + 1) / 2; // Find middle element
            int location = -1; // Return value -1 if not found

            do // Search for element
            {
                // Print remaining elements in array
                Console.Write(RemainingElements(low, high));

                // output spaces for alignment
                for (int i = 0; i < middle; i++)
                    Console.Write("   ");

                Console.WriteLine(" * "); // Indicate current middle

                // if element is found at middle
                if (searchElement == data[middle])
                    location = middle; // location is current middle

                // middle element is too high
                else if (searchElement < data[middle])
                    high = middle - 1; // eliminate lower half
                else // middle element is too low
                    low = middle + 1; // eleminate lower half

                middle = (low + high + 1) / 2; // recalculate the middle  
            } while ((low <= high) && (location == -1));

            return location; // return location of search key
        }

        public string RemainingElements(int low, int high)
        {
            string temporary = string.Empty;

            // output spaces for alignment
            for (int i = 0; i < low; i++)
                temporary += "    ";

            // output elements left in array 
            for (int i = low; i <= high; i++)
                temporary += data[i] + " ";

            temporary += "\n";
            return temporary;
        }

        // Method to output values in array
        public override string ToString()
        {
            return RemainingElements(0, data.Length - 1);
        }
    }
}
*/
[RequireComponent(typeof(Animator))]
public class DistanceMatching : MonoBehaviour {

    Animator animator;

        //public AnimationCurveObject distanceCurveObject;
    
        public float targetDistance;
    /*
        [ContextMenu("Match")]
        void Match()
        {
            float t = distanceCurveObject.curve.BinarySearch(targetDistance);
            Debug.Log(t);
        }
        */
    //    /*animator = GetComponent<Animator>();

    //    var animationState = animator.GetCurrentAnimatorStateInfo(0);
    //    var animationClip = animator.GetCurrentAnimatorClipInfo(0)[0].clip;

    //    //animator.speed = 1f;
    //    float stateLength = animationClip.length;
    //    //animator.speed = 0f;

    //    var normalizedTime = 0f;//animationState.normalizedTime%1f;

    //    float currentDistance = 0f;//distanceCurveOnject.curve.Evaluate(normalizedTime);

    //    //Debug.Log(animationClip);

    //    if (targetDistance > currentDistance)
    //    {
    //        float delta = 0.032f;// / stateInfo.length;

    //        float targetTime = normalizedTime * stateLength;

    //        float maxTime = distanceCurveOnject.curve.keys[distanceCurveOnject.curve.length - 1].time;

    //        //Debug.Log("targetTime : "+ targetTime+ "; stateInfo.length: "+ stateLength + "; maxTime: " + maxTime);

    //        while (targetDistance > currentDistance && targetTime < maxTime)
    //        {
    //            targetTime += delta;

    //            currentDistance = distanceCurveOnject.curve.Evaluate(targetTime);

    //            //Debug.Log("targetTime: " + targetTime+" value: "+ currentDistance);
    //        }

    //        float targetNormalizedTime = targetTime / animationClip.length;

    //        //Debug.Log("targetNormalizedTime: " + targetNormalizedTime);

    //        //Debug.Log("Distance: " + currentDistance);

    //        //animator.ForceStateNormalizedTime(targetNormalizedTime);

    //        animator.Play(animationState.shortNameHash, 0, targetNormalizedTime);

    //        animator.Update(0f);
    //    }*/
    //    DistanceMatchState.MatchDistance(animator, animator.GetCurrentAnimatorStateInfo(0), 0, targetDistance, distanceCurveOnject.curve);
    //}
    // [ContextMenu("Match")]
    // void Match()
    // {
    //     animator = GetComponent<Animator>();

    //     float targetNormalizedTime = distanceCurveObject.curve.BinarySearch(targetDistance) / animator.GetCurrentAnimatorStateInfo(0).length;

    //     animator.SetFloat(Animator.StringToHash("MatchingTime"), distanceMatchReverse ? 1f - targetNormalizedTime : targetNormalizedTime);
    // }

    //public float MatchingTime;

    //public string matchingTimeParameter = "MatchingTime";

    //public string targetDistanceParameter = "TargetDistance";

    int targetDistanceStartHash, targetDistanceStopHash, matchingTimeStartHash, matchingTimeStopHash;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        //TargetDistanceHash = Animator.StringToHash(targetDistanceParameter);
        //MatchingTimeHash = Animator.StringToHash(matchingTimeParameter);
        targetDistanceStartHash = Animator.StringToHash("TargetDistanceStart");
        targetDistanceStopHash = Animator.StringToHash("TargetDistanceStop");
        matchingTimeStartHash = Animator.StringToHash("MatchingTimeStart");
        matchingTimeStopHash = Animator.StringToHash("MatchingTimeStop");
        //animator.speed = 0f;
    }

    float toMatchStartPath;
	void OnDistanceMatchUpdate () {

        /*if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
            startPos = transform.position;
            */

        //if (_targetDistance != targetDistance)
        //{
        //    _targetDistance = targetDistance;
        //    //Match();
        //}

        //if (reverse)
        //    matchPositionStart = matchPositionStop;
        //&& matchPositionStart.sqrMagnitude != 0f
        if (targetDistanceStartHash != 0 ) {

            Vector3 toMatchStartPosition = Vector3.ProjectOnPlane(matchPositionStart - transform.position, transform.up);

            toMatchStartPath += toMatchStartPosition.magnitude;

            Debug.DrawLine(transform.position, matchPositionStart, Color.green, 1f);

            matchPositionStart = transform.position;

            //Debug.DrawRay(transform.position, toMatchStartPosition, Color.green);

            Vector3 toMatchStopPosition = Vector3.ProjectOnPlane(matchPositionStop - transform.position, transform.up);


            Debug.DrawRay(transform.position, toMatchStopPosition, Color.red);

            Rigidbody rb = GetComponentInParent<Rigidbody>();

            Vector3 forward = transform.forward;
            if (rb != null)
            {
                forward = rb.velocity.normalized;
            }

            matchPositionStop = transform.position + forward * toMatchStopPosition.magnitude;

            animator.SetFloat(targetDistanceStartHash, Mathf.Sign(-Vector3.Dot(forward, toMatchStartPosition)) * toMatchStartPath);
            
            animator.SetFloat(targetDistanceStopHash, Mathf.Sign(-Vector3.Dot(forward, toMatchStopPosition)) * toMatchStopPosition.magnitude);

        }
        //MatchingTime = animator.GetFloat(MatchingTimeHash);
    }

    public Vector3 matchPositionStart, matchPositionStop;

    public bool distanceMatchReverse;

    void SetTargetDistanceHash(int targetDistanceParamHash)
    {
        targetDistanceStartHash = targetDistanceParamHash;
    }

    void SetTargetTimeHash(int matchingTimeParamHash)
    {
        matchingTimeStartHash = matchingTimeParamHash;
    }

    public void DistanceMatchEnd()
    {
        //matchPositionStart = Vector3.zero;

        //animator.SetFloat(targetDistanceStartHash, 100f);

        //animator.SetFloat(targetDistanceStopHash, -100f);

        Debug.Log("Distance Match Ended");
    }

    bool reverse;

    void OnDistanceMatch(bool reverse)
    {
        toMatchStartPath = 0f;
        //Debug.Log("OnDistanceMatch.reverse " + reverse);
        distanceMatchReverse = reverse;
        if (reverse)
        {
            this.reverse = reverse;
            //matchPosition = transform.parent.position;
        }
        else
        {
            //matchPositionStart = transform.parent.position;
        }
    }

    private void OnDrawGizmosSelected()
    {
        var capsuleSize = transform.parent.GetComponentInChildren<CapsuleCollider>().bounds.size;

        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(matchPositionStart + Vector3.up * capsuleSize.y/2f, capsuleSize);

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(matchPositionStop + Vector3.up * capsuleSize.y / 2f, capsuleSize);
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireSphere(transform.position, 0.5f);
    //}

    /*public float BinarySearch(float searchElement)
    {
        var data = distanceCurveObject.curve.keys;

        int low = 0; // 0 is always going to be the first element
        int high = data.Length - 1; // Find highest element
        int middle = (low + high + 1) / 2; // Find middle element

        //Debug.Log("keys: " + high);

        bool found = false;
        float location = -1f; // Return value -1 if not found

        while ((low <= high) && !found)
        {
            // if element is found at middle
            if (searchElement == data[middle].value)
            {
                //Debug.Log("Found " + searchElement + " at key: " + middle);
                location = middle; // location is current middle
                found = true;
            }
            // middle element is too high
            else if (searchElement < data[middle].value)
            {
                //Debug.Log(searchElement+" < " + data[middle].value + " at " + middle);
                high = middle - 1; // eliminate lower half
            }
            else // middle element is too low
            {
                //Debug.Log(searchElement + " >= " + data[middle].value + " at " + middle);
                low = middle + 1; // eleminate lower half
            }

            middle = (low + high + 1) / 2; // recalculate the middle  
        } 

        if (!found)
        {
            if (low > data.Length - 1)
            {
                location = data[low-1].value;
            }
            else if (high < 0)
            {
                location = data[0].value;
            }
            else
            {
                //Debug.Log("low:" + low + " high:" + high);
                float length = (data[low].time - data[high].time);
                float diff = (data[low].value - data[high].value);
                //Debug.Log("length:" + length + " diff:" + diff);
                float progess = 1f - (data[low].value - searchElement) / diff;
                //Debug.Log("progess: " + progess);
                location = data[high].time + length * progess;
            }
        }

        return location; // return location of search key
    }*/

    /*
    // perform a binary search on array
    public float BinarySearch(float searchElement)
    {
        float low = 0f; // 0 is always going to be the first element
        float high = distanceCurve.keys[distanceCurve.length].time; // Find highest element
        float middle = (low + high) / 2f; // Find middle element
        float location = -1f; // Return value -1 if not found

        do // Search for element
        {
            // if element is found at middle
            if (searchElement == distanceCurve.Evaluate(middle))
                location = middle; // location is current middle
            // middle element is too high
            else if (searchElement < distanceCurve.Evaluate(middle))
                high = middle; // eliminate lower half
            else // middle element is too low
                low = middle; // eleminate lower half

            middle = (low + high + 1) / 2; // recalculate the middle  
        } while ((low <= high) && (location == -1));

        return location; // return location of search key
    }*/
}
