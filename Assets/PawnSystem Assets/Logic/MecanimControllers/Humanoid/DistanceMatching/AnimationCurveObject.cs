﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCurveObject : ScriptableObject {

    public AnimationCurve distanceCurve;
    public AnimationCurve speedCurve;

}
public static class AnimationCurveExtensions
{
    public static float BinarySearch(this AnimationCurve curve, float searchElement)
    {
        var data = curve.keys;

        int low = 0; // 0 is always going to be the first element
        int high = data.Length - 1; // Find highest element
        int middle = (low + high + 1) / 2; // Find middle element

        //Debug.Log("keys: " + high);

        bool found = false;
        float location = -1f; // Return value -1 if not found

        while ((low <= high) && !found)
        {
            // if element is found at middle
            if (searchElement == data[middle].value)
            {
                //Debug.Log("Found " + searchElement + " at key: " + middle);
                location = middle; // location is current middle
                found = true;
            }
            // middle element is too high
            else if (searchElement < data[middle].value)
            {
                //Debug.Log(searchElement+" < " + data[middle].value + " at " + middle);
                high = middle - 1; // eliminate lower half
            }
            else // middle element is too low
            {
                //Debug.Log(searchElement + " >= " + data[middle].value + " at " + middle);
                low = middle + 1; // eleminate lower half
            }

            middle = (low + high + 1) / 2; // recalculate the middle  
        }

        if (!found)
        {
            if (low > data.Length - 1)
            {
                //Debug.LogError("searchElement "+ searchElement+"; low " + low+" > "+(data.Length - 1)+" time : "+ data[low - 1].time);
                location = data[low - 1].time;
            }
            else if (high < 0)
            {
                location = data[0].time;
            }
            else
            {
                //Debug.Log("low:" + low + " high:" + high);
                float length = (data[low].time - data[high].time);
                float diff = (data[low].value - data[high].value);
                //Debug.Log("length:" + length + " diff:" + diff);
                float progess = 1f - (data[low].value - searchElement) / diff;
                //Debug.Log("progess: " + progess);
                location = data[high].time + length * progess;
            }
        }

        return location; // return location of search key
    }
}

