using UnityEngine;
using System.Collections;

public class HumanoidFootPlacement : MonoBehaviour {

	public bool active = true;

	public float customTranslation = 0f;
	public float bodyTranslationSpeed = 10.0f;
	public float footTranslationSpeed = 10.0f;

	public Animator animator;
	public float footRadius = 0.1f;
	public float maxDown = 0.4f;
	public float maxUp = 0.9f;

    protected float originalTranslation;
	protected Vector3 targetLocalPos = Vector3.zero;
	protected float leftFootTranslation;
	protected float rightFootTranslation;

	protected Transform parent;
	[SerializeField]
    protected float bodyTranslation;
    [SerializeField]
    protected float translationStep = 0.1f;

    public LayerMask collisionMask;

	public float leftWeight = 0f;
	public float rightWeight = 0f;
	Vector3 leftFootTarget, rightFootTarget;
    Vector3 leftFootTargetSmooth, rightFootTargetSmooth;

    public float leftFootHighestLocalTranslation = 0, rightFootHighestLocalTranslation = 0;

	public Vector3 leftFootPos,rightFootPos;

	void Start()
	{
		if (animator == null)
			animator = GetComponent<Animator> ();

        parent = transform.parent;

		if (parent == null) {
			parent = transform;
		}

		leftFootTarget = animator.GetBoneTransform (HumanBodyBones.LeftFoot).position;
		rightFootTarget = animator.GetBoneTransform (HumanBodyBones.RightFoot).position;

        targetLocalPos = transform.localPosition;
        originalTranslation = targetLocalPos.y;
        /*
		Vector3 leftFootTarget =  animator.GetBoneTransform (HumanBodyBones.LeftFoot ).position;
		Vector3 rightFootTarget = animator.GetBoneTransform (HumanBodyBones.RightFoot).position;
		Vector3 midPoint = leftFootTarget + ( rightFootTarget - leftFootTarget ) / 2.0f;

		defaultMidPointHeight = (parent.position - leftFootTarget + ( rightFootTarget - leftFootTarget ) / 2.0f).magnitude;
		//Debug.Log ("defaultMidPointHeight "+defaultMidPointHeight);
		*/
    }

    void OnJump(){
        active = false;
    }

    void OnExitLanding(){
        active = true;
    }

	void OnAnimatorIK(int Layer){

        // if (!isGrounded){
        //     //DisableFootplacement();
        // }
        // else
        // {
        //     //EnableFootplacement();
            if (Layer == 0 && active)
                FootPlacement(animator, maxDown, maxUp);
        //}

    }

	public void SwitchFootPlacement(){
		if (active)
			DisableFootplacement ();
		else
			EnableFootplacement ();
	}

    private void OnDisable() {
        DisableFootplacement();
        targetLocalPos = new Vector3(0f, originalTranslation, 0f);

    }

    public void DisableFootplacement()
    {
		//Debug.Log("DISABLE FP");
		bodyTranslation = originalTranslation;
        leftFootTranslation = 0f;
        rightFootTranslation = 0f;
        active = false;
    }

    public void EnableFootplacement()
    {
        //Debug.Log("ENABLE FP");
        active = true;
    }

    bool isClimbing;
    bool isGrounded;
    void OnClimbing(bool isClimbing){
        this.isClimbing = isClimbing;
    }

	void OnGrounded(bool isGrounded){
        this.isGrounded = isGrounded;
        if (isGrounded && !isClimbing)
            EnableFootplacement();
    }

    //Vector3 oldLeftFootPos;
    //Vector3 oldRightFootPos;

    Ray leftFootRay;
    Ray rightFootRay;

    Vector3 leftFootHighestPoint = Vector3.zero;
    Vector3 rightFootHighestPoint = Vector3.zero;

    Vector3 leftFootNormal;
    Vector3 rightFootNormal;

    public bool leftHit = false, rightHit = false;

    Transform leftFootTransform;
    Transform rightFootTransform;

    public Vector3 leftFootAdditionalRotation;
    public Vector3 rightFootAdditionalRotation;

    void FootPlacement(Animator animator, float maxDown, float maxUp) {
        Vector3 groundNormal = animator.transform.up;
        leftFootHighestLocalTranslation = 0;
		rightFootHighestLocalTranslation = 0;
		float leftWeightNew = 0f;
		float rightWeightNew = 0f;

		float traceLength = maxDown + maxUp;

        leftFootTransform = animator.GetBoneTransform (HumanBodyBones.LeftFoot);
		rightFootTransform = animator.GetBoneTransform (HumanBodyBones.RightFoot);

        leftFootPos = leftFootTransform.position;
        //Vector3 leftFootPosDelta = leftFootPos - oldLeftFootPos;
        //oldLeftFootPos = leftFootPos;

        rightFootPos = rightFootTransform.position;
        //Vector3 rightFootPosDelta = rightFootPos - oldRightFootPos;
        //oldRightFootPos = rightFootPos;

		leftFootRay  = new Ray( leftFootPos
            //+ leftFootTransform.forward * footRadius * 0.5f
            //+ leftFootPosDelta * Time.deltaTime
            + parent.up * (maxUp + footRadius), -parent.up * (maxDown + footRadius) );
		rightFootRay = new Ray( rightFootPos
            //+ rightFootTransform.forward * footRadius * 0.5f
            //+ rightFootPosDelta * Time.deltaTime
            + parent.up* (maxUp + footRadius),  -parent.up * (maxDown + footRadius) );
        //Debug.DrawRay(leftFootRay.origin, leftFootRay.direction, Color.yellow);
        //Debug.DrawRay(rightFootRay.origin, rightFootRay.direction, Color.magenta);

		//Debug.DrawLine (transform.position, leftFootRay.origin);
		//Debug.DrawLine (transform.position, rightFootRay.origin);

        //RaycastHit leftFootHitHighest, rightFootHitHighest;
        
        leftHit = false;
        rightHit = false;

        //RaycastHit leftFootHit, rightFootHit;
        //if (Physics.Raycast (leftFootRay, out leftFootHit, traceLength, collisionMask)) {
        //if (Physics.SphereCast (leftFootRay, footRadius,  out leftFootHit, traceLength, collisionMask)) {
        foreach (RaycastHit leftFootHit in Physics.SphereCastAll(leftFootRay, footRadius, traceLength, collisionMask))
        //foreach (RaycastHit leftFootHit in Physics.RaycastAll(leftFootRay, traceLength, collisionMask))
        //foreach (RaycastHit leftFootHit in Physics.BoxCastAll(leftFootRay.origin, new Vector3(0.075f,0.075f,0.075f), leftFootRay.direction, leftFootTransform.rotation, traceLength, collisionMask))
        {
            if (leftFootHit.collider.tag != "Player")// && Vector3.Angle (leftFootHit.normal, transform.up) < 45.0f) 
            {
                leftHit = true;
                Debug.DrawLine (leftFootRay.origin, leftFootHit.point, Color.green);
                float leftHitLocalTranslation = Vector3.Project((leftFootHit.point - transform.position), groundNormal).magnitude;  //transform.InverseTransformPoint(leftFootHit.point).y ;
                if (leftFootHighestLocalTranslation == 0 || leftHitLocalTranslation > leftFootHighestLocalTranslation)
                {
                    leftFootHighestPoint = Vector3.Lerp(leftFootHighestPoint, leftFootHit.point, 0.5f);
                    leftFootHighestLocalTranslation = Mathf.Min(maxUp, leftHitLocalTranslation);
                    

                    if (Vector3.Angle(leftFootHit.normal, transform.up) < 45.0f)
                    {
                        leftFootNormal = Vector3.Lerp(leftFootNormal, leftFootHit.normal, 10f * Time.deltaTime);
                    }
                    else
                    {
                        leftFootNormal = Vector3.Lerp(leftFootNormal,leftFootTransform.up, 10f * Time.deltaTime);//Vector3.RotateTowards(leftFootNormal, leftFootHit.normal, 10f * Mathf.Deg2Rad, 1f);
                    }
                }
            }
		}

        if (!leftHit)
        {
            //Debug.DrawRay(leftFootRay.origin, leftFootRay.direction * traceLength, Color.grey);
            //leftFootHighestPoint = (leftFootRay.origin + leftFootRay.direction * traceLength);
        }
        
        //if (Physics.Raycast (rightFootRay, out rightFootHit, traceLength, collisionMask)) {
        //if (Physics.SphereCast (rightFootRay, footRadius, out rightFootHit, traceLength, collisionMask)) {
        foreach (RaycastHit rightFootHit in Physics.SphereCastAll(rightFootRay, footRadius, traceLength, collisionMask))
        //foreach (RaycastHit rightFootHit in Physics.RaycastAll(rightFootRay, traceLength, collisionMask))
        //foreach (RaycastHit rightFootHit in Physics.BoxCastAll(rightFootRay.origin, new Vector3(0.075f, 0.001f, 0.075f), rightFootRay.direction, rightFootTransform.rotation, traceLength, collisionMask))
        {
            if (rightFootHit.collider.tag != "Player")// && Vector3.Angle (rightFootHit.normal, transform.up) < 45.0f) 
            {
                rightHit = true;
				Debug.DrawLine (rightFootRay.origin, rightFootHit.point, Color.green);
                float rightHitLocalTranslation = Vector3.Project((rightFootHit.point - transform.position),groundNormal).magnitude; //transform.InverseTransformPoint(rightFootHit.point).y ;
                if (rightFootHighestLocalTranslation == 0 || rightHitLocalTranslation > rightFootHighestLocalTranslation)
                {
                    rightFootHighestPoint = Vector3.Lerp(rightFootHighestPoint, rightFootHit.point, 0.5f);
                    rightFootHighestLocalTranslation = Mathf.Min(maxUp, rightHitLocalTranslation);

                    if (Vector3.Angle(rightFootHit.normal, transform.up) < 45.0f)
                    {
                        rightFootNormal = Vector3.Lerp(rightFootNormal, rightFootHit.normal, 10f * Time.deltaTime);
                    }
                    else
                    {
                        rightFootNormal = Vector3.Lerp(rightFootNormal, rightFootTransform.up, 10f * Time.deltaTime);//Vector3.RotateTowards(leftFootNormal, rightFootHit.normal, 10f * Mathf.Deg2Rad, 1f);
                    }
                }
            }
		}

        if (!rightHit)
        {
            //Debug.DrawRay(rightFootRay.origin, rightFootRay.direction * traceLength, Color.grey);
            //rightFootHighestPoint = (rightFootRay.origin + rightFootRay.direction * traceLength);
        }
      
        if (leftHit)
        {
            //leftFootTranslation = leftFootHighestLocalTranslation;
                //Mathf.Lerp(leftFootTranslation, leftFootHighestLocalTranslation, (leftFootHighestLocalTranslation > leftFootTranslation) ? 1f : footTranslationSpeed * Time.deltaTime); // (leftFootHighestLocalTranslation > leftFootTranslation) ? 1f : 
            leftFootTarget = parent.InverseTransformPoint( leftFootTransform.position + parent.up * (leftFootHighestLocalTranslation + customTranslation) );
            leftWeightNew = 1.0f;
            //Debug.DrawRay(leftFootRay.origin, parent.up * leftFootHighestLocalTranslation, Color.yellow);
            //leftFootParentTranslation = parent.InverseTransformPoint(leftFootRay.origin + leftFootRay.direction * leftFootTranslation);
        }
        else {
            //leftFootTranslation = traceLength;
            leftFootTarget = parent.InverseTransformPoint(leftFootPos);//parent.InverseTransformPoint(leftFootTransform.position + parent.up * (-traceLength + customTranslation));
        }

        if (rightHit)
        {
            //rightFootTranslation = rightFootHighestLocalTranslation;
                //Mathf.Lerp(rightFootTranslation, rightFootHighestLocalTranslation, (rightFootHighestLocalTranslation > rightFootTranslation) ? 1f : footTranslationSpeed * Time.deltaTime); // (rightFootHighestLocalTranslation > rightFootTranslation) ? 1f :
            rightFootTarget = parent.InverseTransformPoint( rightFootTransform.position + parent.up * (rightFootHighestLocalTranslation + customTranslation) );
            rightWeightNew = 1.0f;
            //Debug.DrawRay(rightFootRay.origin, parent.up * rightFootHighestLocalTranslation, Color.magenta);
            //rightFootParentTranslation = parent.InverseTransformPoint(rightFootRay.origin + rightFootRay.direction * rightFootTranslation);
        }
        else {
            //rightFootTranslation = traceLength;
            rightFootTarget = parent.InverseTransformPoint(rightFootPos);//parent.InverseTransformPoint(rightFootTransform.position + parent.up * (-traceLength + customTranslation));
        }
        
        Vector3 lowestFootPosForParent = parent.InverseTransformPoint( (leftFootHighestLocalTranslation < rightFootHighestLocalTranslation) ? leftFootHighestPoint : rightFootHighestPoint );

        bodyTranslation = Mathf.Max(originalTranslation-maxDown, Mathf.Min(originalTranslation, lowestFootPosForParent.y));
        //Mathf.Max(originalTranslation - maxDown, Mathf.Min(originalTranslation, lowestFootPosForParent.y) ) ;
        //translation = translation-(translation % translationStep);

        leftWeight = 
        //leftWeightNew;
        Mathf.Lerp(leftWeight, leftWeightNew, 2f * Time.deltaTime);
		rightWeight = 
        //rightWeightNew;
        Mathf.Lerp(rightWeight, rightWeightNew, 2f * Time.deltaTime);

        //targetLocalPos = new Vector3(targetLocalPos.x, Mathf.Lerp(targetLocalPos.y, bodyTranslation, bodyTranslationSpeed * Time.deltaTime), targetLocalPos.z);

        leftFootTargetSmooth = leftFootTarget + Vector3.Project(leftFootTargetSmooth - leftFootTarget, transform.up);
        //Debug.DrawLine(parent.TransformPoint(leftFootTargetSmooth), parent.TransformPoint(leftFootTarget), Color.magenta);
        leftFootTargetSmooth = Vector3.Lerp(leftFootTargetSmooth, leftFootTarget, (leftFootHighestLocalTranslation > leftFootTranslation) ? footTranslationSpeed * Time.deltaTime * 5f : footTranslationSpeed * Time.deltaTime);

        rightFootTargetSmooth = rightFootTarget + Vector3.Project(rightFootTargetSmooth - rightFootTarget, transform.up);
        //Debug.DrawLine(parent.TransformPoint(rightFootTargetSmooth), parent.TransformPoint(rightFootTarget), Color.magenta);
        rightFootTargetSmooth = Vector3.Lerp(rightFootTargetSmooth, rightFootTarget, (rightFootHighestLocalTranslation > rightFootTranslation) ? footTranslationSpeed * Time.deltaTime  * 5f : footTranslationSpeed * Time.deltaTime);

        animator.SetIKPosition(AvatarIKGoal.LeftFoot, parent.TransformPoint(leftFootTargetSmooth) );
		animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, leftWeight);
        if (useRotation)
        {
            Quaternion targetFootRotation = Quaternion.FromToRotation(transform.up, leftFootNormal) * leftFootTransform.rotation * Quaternion.Euler(leftFootAdditionalRotation);

            //Debug.DrawRay(leftFootTransform.position, targetFootRotation * Vector3.up, Color.green);
            //Debug.DrawRay(leftFootTransform.position, targetFootRotation * Vector3.forward, Color.blue);
            //Debug.DrawRay(leftFootTransform.position, targetFootRotation * Vector3.right, Color.red);

            animator.SetIKRotation(AvatarIKGoal.LeftFoot, targetFootRotation);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, leftWeight);
        }
        //Debug.DrawRay(leftFootTransform.position, leftFootNormal, Color.yellow);

        animator.SetIKPosition(AvatarIKGoal.RightFoot, parent.TransformPoint(rightFootTargetSmooth) );
		animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, rightWeight);
        if (useRotation)
        {
            Quaternion targetFootRotation = Quaternion.FromToRotation(transform.up, rightFootNormal) * rightFootTransform.rotation * Quaternion.Euler(rightFootAdditionalRotation);

            //Debug.DrawRay(rightFootTransform.position, targetFootRotation * Vector3.up, Color.green);
            //Debug.DrawRay(rightFootTransform.position, targetFootRotation * Vector3.forward, Color.blue);
            //Debug.DrawRay(rightFootTransform.position, targetFootRotation * Vector3.right, Color.red);

            animator.SetIKRotation(AvatarIKGoal.RightFoot, targetFootRotation);
            animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, rightWeight);
        }
        //Debug.DrawRay(rightFootTransform.position, rightFootNormal, Color.magenta);
        
    }

    public bool useTranslation = true;

    public bool useRotation = true;

	void LateUpdate(){

        //Vector3 targetPos = parent.TransformPoint (targetLocalPos);

        //Debug.DrawLine (parent.position, targetPos, parent.position.y < targetPos.y ? Color.green :  Color.red);
        if (useTranslation)
        {
            targetLocalPos = new Vector3(targetLocalPos.x, Mathf.Lerp(targetLocalPos.y, bodyTranslation, bodyTranslationSpeed * Time.deltaTime), targetLocalPos.z);

            transform.localPosition = targetLocalPos;
        }
    }
    
    private void OnDrawGizmos()
    {
        if (enabled && Application.isPlaying)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere((leftFootRay.origin), footRadius);
            Gizmos.DrawLine(leftFootRay.origin, leftFootHighestPoint);
            Gizmos.DrawSphere((rightFootRay.origin), footRadius);
            Gizmos.DrawLine(rightFootRay.origin, rightFootHighestPoint);

            Gizmos.color = Color.blue;
            Gizmos.DrawLine(leftFootHighestPoint, leftFootHighestPoint + parent.up * (leftFootHighestLocalTranslation + customTranslation));
            Gizmos.DrawLine(rightFootHighestPoint, rightFootHighestPoint + parent.up * (rightFootHighestLocalTranslation + customTranslation));

            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(parent.TransformPoint(leftFootTarget), footRadius);
            Gizmos.DrawWireSphere(parent.TransformPoint(rightFootTarget), footRadius);

            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(leftFootHighestPoint, 0.05f);
            Gizmos.DrawSphere(rightFootHighestPoint, 0.05f);
        }
    }
}