﻿//#define DRAW_RAYS

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class HumanoidLookAt : MonoBehaviour {

	public Vector3 lookAtWeights = new Vector3(0.2f, 0.6f, 1.0f);
    public float clampWeight = 0.5f;

	AnimatorStatesHandler state;

	protected Animator animator;
	protected Vector3 headLocalPos;
	protected float weight;

	//IK
	[Header("IK Settings")]
	public Transform lookAtTarget;
	public float lookAtSpeed = 1.0f;
	public float YawMaxAngle = 145.0f;
    public float PitchMaxAngle = 90.0f;
    public float lookDistance = 1000f;

	public float AimAngleX = 12.0f;
	public float AimAngleY = 12.0f;

    public float yaw;
    public float pitch;

    void Start(){
        animator = GetComponent<Animator> ();
		state = GetComponent<AnimatorStatesHandler> ();
		headLocalPos = transform.InverseTransformPoint( animator.GetBoneTransform (HumanBodyBones.Head).position );
	}

    void OnAnimatorIK(int Layer) {
        Vector3 headPos = transform.position + transform.rotation * headLocalPos;
        if (Layer == 0) {

			Vector3 lookDir = lookAtTarget!=null ? lookAtTarget.position - headPos : state.lookRot * Vector3.forward;

            Vector3 lookDirOnGround = Vector3.ProjectOnPlane(lookDir, transform.up);

            
            yaw = Vector3.SignedAngle(lookDirOnGround, transform.forward, transform.up);
            
            pitch = Vector3.SignedAngle(Vector3.ProjectOnPlane(lookDir, transform.right), lookDirOnGround, transform.right);
            /*
            if (Mathf.Abs(yaw) > YawMaxAngle)
            {
                yaw = Mathf.Sign(yaw) * YawMaxAngle;
            }

            if (Mathf.Abs(pitch) > PitchMaxAngle)
            {
                pitch = Mathf.Sign(pitch) * PitchMaxAngle;
            }
            */
            Vector3 lookAtPosition = headPos + lookDir * lookDistance;//(transform.rotation * Quaternion.Euler(0f, -yaw, 0f) * Quaternion.Euler(-pitch, 0f, 0f)  * Vector3.forward) * lookDistance;

            animator.SetLookAtPosition(lookAtPosition);

            //Debug.DrawLine(headPos, lookAtPosition, Color.green);

            //weight = Mathf.Lerp(weight, 1.0f, lookAtSpeed * Time.smoothDeltaTime);
            
            if (yaw < YawMaxAngle && pitch < PitchMaxAngle) { // Vector3.Cross(camdir,characterForward).y > 0)//
                weight = Mathf.Lerp(weight, 1.0f, lookAtSpeed * Time.smoothDeltaTime);

#if DRAW_RAYS
                Debug.DrawRay(headPos, lookDir, Color.green);
#endif
            } else {
                weight = Mathf.Lerp(weight, 0.0f, 2.0f * Time.smoothDeltaTime);
            #if DRAW_RAYS
                Debug.DrawRay(headPos, lookDir, Color.gray);
            #endif
            }

            animator.SetLookAtWeight(weight, lookAtWeights.x, lookAtWeights.y, lookAtWeights.z, clampWeight);
        }
    }
}
