﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AnimatorStatesHandler))]
public class LookToCamera : MonoBehaviour {

	AnimatorStatesHandler state;

	// Use this for initialization
	void Start () {
		state = GetComponent<AnimatorStatesHandler> ();
	}
	
	// Update is called once per frame
	void Update () {
		state.lookRot = Quaternion.LookRotation (-Camera.main.transform.forward, transform.up);
		state.UpdateState(Time.deltaTime);
	}
}
