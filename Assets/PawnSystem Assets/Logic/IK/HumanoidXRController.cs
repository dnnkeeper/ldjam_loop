﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
/*        
public class HumanoidXRController : MonoBehaviour {

    Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        InputTracking.trackingAcquired += InputTracking_trackingAcquired;
        InputTracking.trackingLost += InputTracking_trackingLost;
    }

    public bool leftXR, rightXR;

    private void InputTracking_trackingAcquired(XRNodeState obj)
    {
        if (obj.nodeType == XRNode.LeftHand)
        {
            leftXR = true;
        }
        else
        if (obj.nodeType == XRNode.RightHand)
        {
            rightXR = true;
        }
    }

    private void InputTracking_trackingLost(XRNodeState obj)
    {
        if (obj.nodeType == XRNode.LeftHand)
        {
            leftXR = false;
        }
        else
        if (obj.nodeType == XRNode.RightHand)
        {
            rightXR = false;
        }
    }

    public Transform leftHandTarget, rightHandTarget;

    private void OnEnable()
    {
        if (leftHandTarget == null)
        {
            leftHandTarget = new GameObject("leftHandTarget").transform;
            leftHandTarget.SetParent(animator.GetBoneTransform(HumanBodyBones.Head));
            //leftHandTarget.localPosition = Vector3.zero;
            leftHandTarget.position = animator.GetBoneTransform(HumanBodyBones.LeftHand).position;
        }

        if (rightHandTarget == null)
        {
            rightHandTarget = new GameObject("RightHandTarget").transform;
            rightHandTarget.SetParent(animator.GetBoneTransform(HumanBodyBones.Head));
            //rightHandTarget.localPosition = Vector3.zero;
            rightHandTarget.position = animator.GetBoneTransform(HumanBodyBones.RightHand).position;

        }

        leftHandTarget.position = animator.GetBoneTransform(HumanBodyBones.LeftHand).position;
        rightHandTarget.position = animator.GetBoneTransform(HumanBodyBones.RightHand).position;
    }

    private void Update()
    {
        Transform headTransform = animator.GetBoneTransform(HumanBodyBones.Head);
        if (leftXR)
        {
            leftHandTarget.position = headTransform.position + headTransform.TransformPoint(InputTracking.GetLocalPosition(XRNode.LeftHand));
        }
        if (rightXR)
        {
            rightHandTarget.position = headTransform.position + headTransform.TransformPoint(InputTracking.GetLocalPosition(XRNode.RightHand));
        }
    }

    private void OnAnimatorIK(int layerIndex)
    {
        

        animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.position );
        animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f );


        animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);
        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
        
    }
}
*/