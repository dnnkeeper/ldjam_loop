﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class AnimationsEditorUtility
{
    [MenuItem("Assets/Animations/Generate Root Motion")]
    private static void GenerateRootMotion()
    {
        var clip = Selection.activeObject as AnimationClip;
        if (clip == null){
            Debug.LogError(Selection.activeObject+" is not an AnimationClip");
            return;
        }

        string assetPath = AssetDatabase.GetAssetPath(Selection.activeObject);

        if (clip != null && (clip.hideFlags != (HideFlags.HideInHierarchy | HideFlags.NotEditable)))
        {
            int nameIndex = assetPath.LastIndexOf("/");

            string baseAssetPath = assetPath.Substring(0, nameIndex);

            string baseAssetName = assetPath.Split('/').Last<string>(); //assetPath.Substring(nameIndex, assetPath.Length);

            var clipSettings = AnimationUtility.GetAnimationClipSettings(clip);

            string newPath = baseAssetPath + "/" + clip.name.Split('|').Last<string>() + "_RootMotion.anim";

            AssetDatabase.CopyAsset(assetPath, newPath);
        }

        AssetDatabase.Refresh();
    }

    // Note that we pass the same path, and also pass "true" to the second argument.
    [MenuItem("Assets/Animations/Generate Root Motion", true)]
    private static bool NewMenuOptionValidation()
    {
        // This returns true when the selected object is a Texture2D (the menu item will be disabled otherwise).
        return Selection.activeObject != null && Selection.activeObject.GetType() == typeof(AnimationClip);
    }
}
