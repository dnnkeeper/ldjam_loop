﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class TexturesEditorUtility
{
    [MenuItem("Assets/Textures/Extract Alpha Channel")]
    private static void ExtractAlphaChannel()
    {
        var texture = Selection.activeObject as Texture2D;

        if (!texture.isReadable)
            SetTextureImporterFormat(texture, true);

        var texturePath = AssetDatabase.GetAssetPath(texture);
        var newPath = Path.GetDirectoryName(texturePath) + "/" + Path.GetFileNameWithoutExtension(texturePath) + "_alpha.png";

        var newTexture = new Texture2D(texture.width, texture.height, TextureFormat.R16, false);

        var pixels = texture.GetPixels();
        for (int i = 0; i < pixels.Length-1; i++)
        {
            pixels[i].r = pixels[i].a;
        }

        newTexture.SetPixels(pixels);
        var bytes = newTexture.EncodeToPNG();

        File.WriteAllBytes(newPath, bytes);
        Debug.Log("Created " + newPath);
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Textures/Add Alpha Channel")]
    private static void AddAlphaChannel()
    {
        var textureWithColor = Selection.activeObject as Texture2D;

        if (!textureWithColor.isReadable)
            SetTextureImporterFormat(textureWithColor, true);

        var texturePath = AssetDatabase.GetAssetPath(textureWithColor);

        Texture2D textureWithAlpha = new Texture2D(textureWithColor.width, textureWithColor.height, TextureFormat.RGBA32, false);
        
        string pathToTextureWithAlpha = EditorUtility.OpenFilePanel("Alpha source", texturePath, "png");
        if (pathToTextureWithAlpha.Length != 0)
        {
            //textureAlpha = AssetDatabase.LoadAssetAtPath<Texture2D>(pathToTextureWithAlpha);
            //if (!textureAlpha.isReadable)
            //    SetTextureImporterFormat(textureAlpha, true);
            var fileContent = File.ReadAllBytes(pathToTextureWithAlpha);
            textureWithAlpha.LoadImage(fileContent);
        }
        
        var newPath = Path.GetDirectoryName(texturePath) + "/" + Path.GetFileNameWithoutExtension(texturePath) + "_with_alpha.png";

        var newTexture = new Texture2D(textureWithColor.width, textureWithColor.height, TextureFormat.RGBA32, false);

        //var pixelsColor = textureWithColor.GetPixels();
        //var pixelsAlpha = textureWithAlpha.GetPixels();
        //for (int i = 0; i < pixelsColor.Length - 1; i++)
        //{
        //    pixelsColor[i].a = pixelsAlpha[i].a;
        //}

        for (int i = 0; i < newTexture.width; i++)
        {
            for (int j = 0; j < newTexture.height; j++)
            {
                var color = textureWithColor.GetPixel(i, j);
                color.a = textureWithAlpha.GetPixel(i, j).a;
                newTexture.SetPixel(i, j, color);
            }
        }

        //newTexture.SetPixels(pixelsAlpha);
        var bytes = newTexture.EncodeToPNG();

        File.WriteAllBytes(newPath, bytes);
        Debug.Log("Created " + newPath);
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Textures/Convert Texture To PNG")]
    private static void SaveTextureAsPNG()
    {
        foreach (var selectedObject in Selection.objects)
        {
            var texture = selectedObject as Texture2D; //Selection.activeObject as Texture2D;

            if (!texture.isReadable)
                SetTextureImporterFormat(texture, true);

            var texturePath = AssetDatabase.GetAssetPath(texture);
            var newPath = Path.GetDirectoryName(texturePath) + "/" + Path.GetFileNameWithoutExtension(texturePath) + ".png";

            var newTexture = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);

            newTexture.SetPixels(texture.GetPixels());
            var bytes = newTexture.EncodeToPNG();

            var textureMetafilePath = texturePath + ".meta";
            File.Delete(texturePath);
            Debug.Log("Deleted " + texturePath);
            File.Move(textureMetafilePath, newPath + ".meta");

            File.WriteAllBytes(newPath, bytes);
            Debug.Log("Created " + newPath);
            AssetDatabase.Refresh();

            var tImporter = AssetImporter.GetAtPath(newPath) as TextureImporter;
            if (tImporter != null)
            {
                tImporter.isReadable = false;
                tImporter.crunchedCompression = false;

                AssetDatabase.ImportAsset(newPath);
                AssetDatabase.Refresh();
            }
        }
    }

    [MenuItem("Assets/Textures/Convert Texture To JPG")]
    private static void SaveTextureAsJPG()
    {
        var texture = Selection.activeObject as Texture2D;

        if (!texture.isReadable)
            SetTextureImporterFormat(texture, true);

        var texturePath = AssetDatabase.GetAssetPath(texture);
        var newPath = Path.GetDirectoryName(texturePath) + "/" + Path.GetFileNameWithoutExtension(texturePath) + ".jpg";

        var newTexture = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);

        newTexture.SetPixels(texture.GetPixels());
        var bytes = newTexture.EncodeToJPG();

        var textureMetafilePath = texturePath + ".meta";
        File.Delete(texturePath);
        Debug.Log("Deleted " + texturePath);
        File.Move(textureMetafilePath, newPath + ".meta");

        File.WriteAllBytes(newPath, bytes);
        Debug.Log("Created " + newPath);
        AssetDatabase.Refresh();

        var tImporter = AssetImporter.GetAtPath(newPath) as TextureImporter;
        if (tImporter != null)
        {
            tImporter.isReadable = false;
            tImporter.crunchedCompression = false;

            AssetDatabase.ImportAsset(newPath);
            AssetDatabase.Refresh();
        }
    }

    public static void SetTextureImporterFormat(Texture2D texture, bool isReadable)
    {
        if (null == texture) return;

        string assetPath = AssetDatabase.GetAssetPath(texture);
        var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if (tImporter != null)
        {
            tImporter.isReadable = isReadable;
            tImporter.crunchedCompression = false;

            AssetDatabase.ImportAsset(assetPath);
            AssetDatabase.Refresh();
        }
    }


    // Note that we pass the same path, and also pass "true" to the second argument.
    [MenuItem("Assets/Textures/Convert Texture To PNG", true), MenuItem("Assets/Textures/Convert Texture To JPG", true), MenuItem("Assets/Textures/Extract Alpha Channel", true), MenuItem("Assets/Textures/Add Alpha Channel", true)]
    private static bool NewMenuOptionValidation()
    {
        // This returns true when the selected object is a Texture2D (the menu item will be disabled otherwise).
        return Selection.activeObject != null && Selection.activeObject.GetType() == typeof(Texture2D);
    }
}
