using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityStandardAssets.CrossPlatformInput
{
    public class JoystickWorldSpace : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        public Vector3 camSize;

        public enum AxisOption
        {
            // Options for which axes to use
            Both, // Use both
            OnlyHorizontal, // Only horizontal
            OnlyVertical // Only vertical
        }

        public float MovementRange = 1f;

        public float movementRangeWS
        {
            get
            {
                //if (cam != null)
                //    return (cam.ScreenToWorldPoint(new Vector3(MovementRange, 0, cam.nearClipPlane)) - cam.ViewportToWorldPoint(new Vector2(0.5f, 0.5f)) ).magnitude;
                return MovementRange;
            }
        }

        public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
        public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
        public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

        Vector3 startLocalPosition;
        bool m_UseX; // Toggle for using the x axis
        bool m_UseY; // Toggle for using the Y axis
        CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
        CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

        void OnEnable()
        {
            //CreateVirtualAxes();
            startLocalPosition = transform.localPosition;
            CreateVirtualAxes();
        }

        void Start()
        {
            //m_StartPos = transform.position;
            //CreateVirtualAxes();
        }

        void UpdateVirtualAxes(Vector3 currenLocalPos)
        {
            var delta = startLocalPosition - currenLocalPos; //transform.TransformVector(startLocalPosition - currenLocalPos);
            delta.y = -delta.y;
            delta /= movementRangeWS;
            if (m_UseX)
            {
                m_HorizontalVirtualAxis.Update(-delta.x);
            }

            if (m_UseY)
            {
                m_VerticalVirtualAxis.Update(delta.y);
            }
        }

        void CreateVirtualAxes()
        {
            // set axes to use
            m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
            m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

            // create new axes based on axes to use
            if (m_UseX)
            {
                if (m_HorizontalVirtualAxis == null)
                {
                    if (CrossPlatformInputManager.AxisExists(horizontalAxisName))
                    {
                        m_HorizontalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(horizontalAxisName);
                    }
                    else
                    {
                        m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
                        Debug.Log(m_HorizontalVirtualAxis.name + " created");
                        CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
                    }
                    
                }
                else
                {
                    Debug.Log(m_HorizontalVirtualAxis.name + " axis already created");
                }
            }
            if (m_UseY)
            {
                if (m_VerticalVirtualAxis == null)
                {
                    if (CrossPlatformInputManager.AxisExists(verticalAxisName))
                    {
                        m_VerticalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(verticalAxisName);
                    }
                    else
                    {
                        m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
                        Debug.Log(m_VerticalVirtualAxis.name + " created");
                        CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
                    }
                }
                else
                {
                    Debug.Log(m_VerticalVirtualAxis.name + " axis already created");
                }
            }
        }

        Camera cam;

        public void OnDrag(PointerEventData data)
        {
            Vector3 deltaPos = Vector3.zero;

            Canvas canvas = GetComponentInParent<Canvas>();

            Vector3 dataPosition = data.position;

            Vector3 startPositionWorldSpace = transform.parent.TransformPoint(startLocalPosition);

            Vector3 deltaVector = Vector3.zero;//(dataPosition - startPositionWorldSpace);

            if (canvas != null && canvas.renderMode != RenderMode.ScreenSpaceOverlay)
            {
                cam = canvas.worldCamera;



                Plane canvasPlane = new Plane(canvas.transform.forward, canvas.transform.position);

                var ray = cam.ScreenPointToRay(data.position);
                float hitDistance;
                if (canvasPlane.Raycast(ray, out hitDistance))
                {
                    //hitDistance = Mathf.Max(hitDistance, cam.nearClipPlane);

                    Vector3 hitPoint = ray.origin + ray.direction * hitDistance;

                    dataPosition =
                    hitPoint;

                    deltaVector = (dataPosition - startPositionWorldSpace);

                    transform.position =
                    startPositionWorldSpace + Vector3.ClampMagnitude(deltaVector, movementRangeWS);
                    //dataPosition;
                    //targetPos;
                }
                //cam.ScreenToWorldPoint(new Vector3(dataPosition.x, dataPosition.y, canvas.planeDistance)));
                //cam.ScreenToWorldPoint(new Vector3(dataPosition.x, dataPosition.y, canvas.planeDistance)); //canvas.worldCamera.transform.InverseTransformPoint(;

                //deltaVector = new Vector3(deltaVector.x, deltaVector.y, deltaVector.z);
                //deltaVector = cam.transform.InverseTransformPoint(deltaVector);
            }

            //Debug.DrawLine(Vector3.zero, Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Color.blue);
            //Debug.DrawLine(Vector3.zero, Camera.main.ScreenToWorldPoint(new Vector2(canvas.worldCamera.pixelWidth, canvas.worldCamera.pixelHeight)), Color.red);
            Debug.DrawLine(Vector3.zero, dataPosition, Color.red);

            if (m_UseX)
            {
                float delta = (int)(deltaVector.x);

                delta = Mathf.Clamp(delta, -movementRangeWS, movementRangeWS);
                deltaPos.x = delta;
            }

            if (m_UseY)
            {
                float delta = (int)(deltaVector.y);

                delta = Mathf.Clamp(delta, -movementRangeWS, movementRangeWS);
                deltaPos.y = delta;
            }

            //Vector3 targetPos = m_StartPos + deltaPos; //new Vector3(m_StartPos.x + deltaPos.x, m_StartPos.y + deltaPos.y, m_StartPos.z + deltaPos.z);
            //Debug.DrawLine(Vector3.zero, targetPos, Color.yellow);

            if (canvas != null && canvas.renderMode != RenderMode.ScreenSpaceOverlay)
            {
                camSize = canvas.worldCamera.ViewportToWorldPoint(new Vector3(0, 0, canvas.planeDistance)) - canvas.worldCamera.ViewportToWorldPoint(new Vector3(1, 1, canvas.planeDistance));

                //MovementRange = (int)(MovementRange*camSize.magnitude);
            }


            //Debug.Log("UpdateVirtualAxes " + transform.localPosition);
            UpdateVirtualAxes(transform.localPosition);
        }


        public void OnPointerUp(PointerEventData data)
        {
            transform.localPosition = startLocalPosition;
            UpdateVirtualAxes(startLocalPosition);
        }


        public void OnPointerDown(PointerEventData data) { }

        void OnDisable()
        {
            // remove the joysticks from the cross platform input
            if (m_UseX)
            {
                Debug.Log(m_HorizontalVirtualAxis + " removed");
                CrossPlatformInputManager.UnRegisterVirtualAxis(m_HorizontalVirtualAxis.name);
                m_HorizontalVirtualAxis.Remove();
                m_HorizontalVirtualAxis = null;
            }
            if (m_UseY)
            {
                Debug.Log(m_VerticalVirtualAxis + " removed");
                CrossPlatformInputManager.UnRegisterVirtualAxis(m_VerticalVirtualAxis.name);
                m_VerticalVirtualAxis.Remove();
                m_VerticalVirtualAxis = null;
            }
        }
    }
}