﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace VoiceChat.Networking
{
    [RequireComponent(typeof(VoiceChatPlayer))]
    public class VoiceChatNetworkPlayer : NetworkBehaviour
    {
        public VoiceChatRecorder voiceChatRecorder;

        public VoiceChatPlayer player;

        private void Awake()
        {
            if (player == null)
                player = GetComponent<VoiceChatPlayer>();
            
            voiceChatRecorder.NewSample += OnNewSample;
        }

        void OnNewSample(VoiceChatPacket packet)
        {
            var packetMessage = new VoiceChatPacketMessage
            {
                packet = packet,
            };

            //if (LogFilter.logDebug)
            //{
            //    Debug.Log("Got a new Voice Sample. Streaming!");
            //}

            //NetworkManager.singleton.client.SendUnreliable(VoiceChatMsgType.Packet, packetMessage);
            if (isLocalPlayer) {
                Cmd_ServerReceivePackage(packetMessage);
            }
        }

        [Command]
        void Cmd_ServerReceivePackage(VoiceChatPacketMessage data)
        {
            //var data = netMsg.ReadMessage<VoiceChatPacketMessage>();
            //Debug.Log("Cmd_ServerReceivePackage!");
            Rpc_ClientReceivePackage(data);
        }

        [ClientRpc]
        void Rpc_ClientReceivePackage(VoiceChatPacketMessage data)
        {
            if (!isLocalPlayer)
            {
                //Debug.Log("Rpc_ClientReceivePackage!");
                player.OnNewSample(data.packet);
            }
        }

    }
}
