using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityChanger : MonoBehaviour
{
    bool changed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G)){
            if (changed){
                Physics.gravity = new Vector3(0f,-9.81f, 0f);

            }
            else
                Physics.gravity = new Vector3(-9.81f,0f,0f);
            changed = !changed;

        }
    }
}
