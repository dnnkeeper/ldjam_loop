﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentrifugalForceField : MonoBehaviour {

    //public bool disableGravity;

    public float angularVelocity = 1f;

    public HashSet<ConstantForce> forces = new HashSet<ConstantForce>();
    
    void OnTriggerEnter(Collider other)
    {
        Rigidbody otherRB = other.attachedRigidbody;
        if (otherRB)
        {
            // if (disableGravity)
            // {
            //     otherRB.useGravity = false;
            // }
            //other.attachedRigidbody.useGravity = false;
            ConstantForce forceComponent = otherRB.gameObject.GetComponent<ConstantForce>();
            if (forceComponent == null)
            {
                forceComponent = otherRB.gameObject.AddComponent<ConstantForce>();
                //Debug.Log("Add ConstantForce " + forceComponent);
                otherRB.SendMessage("OnCustomGravity", forceComponent, SendMessageOptions.DontRequireReceiver);
                forces.Add(forceComponent);
            }
            else{
                //Debug.LogError(otherRB+ " already has ConstantForce Component! ", otherRB);
            }
        }
    }

    void FixedUpdate()
    {
        foreach (ConstantForce forceComponent in forces)
        {
            Rigidbody otherRB = forceComponent.GetComponent<Rigidbody>();
            if (otherRB)
            {
                if (otherRB.useGravity)
                {
                    forceComponent.force = otherRB.mass * angularVelocity * (forceComponent.transform.position - transform.position);
                }
                else
                    forceComponent.force = Vector3.zero;

                //Debug.DrawRay(forceComponent.transform.position, forceComponent.force, Color.red);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        Rigidbody otherRB = other.attachedRigidbody;
        if (otherRB)
        {
            ConstantForce forceComponent = otherRB.GetComponent<ConstantForce>();
            if (forceComponent != null && forces.Contains(forceComponent))
            {
                forces.Remove(forceComponent);
                //Debug.Log("Remove ConstantForce " + forceComponent);
                Destroy(forceComponent);
            }
        }
    }

    /*
    void OnTriggerStay(Collider other)
    {
        if (other.attachedRigidbody)
        {
            other.attachedRigidbody.AddForce( angularVelocity * (other.transform.position - transform.position) );
        }
    }*/
}
