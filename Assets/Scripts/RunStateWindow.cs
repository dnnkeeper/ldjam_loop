﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

using UnityEditor;
using UnityEditor.Animations;
using Unity.EditorCoroutines.Editor;

public class RunStateWindow : EditorWindow
{
    public Transform transform;
  
    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Run State Test")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        RunStateWindow window = (RunStateWindow)EditorWindow.GetWindow(typeof(RunStateWindow));
        window.Show();
    }

    Animator animator;
    AnimatorState selectedState;
    
    void OnGUI()
    {
        GUILayout.Label("Run State Test", EditorStyles.boldLabel);
        
        animator = (Animator)EditorGUILayout.ObjectField("Animator", animator, typeof(Animator), true);
        
        if (animator != null)
        {
            transform = animator.transform;

            selectedState = (AnimatorState)EditorGUILayout.ObjectField("Animator State", selectedState, typeof(Transform), true);
            if (Selection.activeObject != null)
            {
                selectedState = Selection.activeObject as AnimatorState;
            }

            if (selectedState != null && selectedState.motion != null && GUILayout.Button("Run State")){

                RunState();
            }
            
        }
    }

    IEnumerator RunState_Routine()
    {
        Animator animator = transform.GetComponent<Animator>();

        var runtimeController = animator.runtimeAnimatorController as AnimatorController;

        Debug.Log(string.Format("Layer Count: {0}", animator.layerCount));

        var runtimeControllerLayers = runtimeController.layers;
        int stateLayerIndex = 0;
        string stateName = selectedState.name;
        
        float t = 0;
        
        Debug.Log("animator.Play " + stateName + " " + stateLayerIndex);
        
        yield return null;

        animator.Play(stateName, stateLayerIndex, 0);

        yield return null;

        float clipLength = 0;

        AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
        foreach (AnimationClip clip in clips)
        {
            if (clip.name == selectedState.motion.name)
            {
                Debug.Log(clip.name+" length = " + clip.length);
                clipLength = clip.length;
                break;
            }
        }

        var stateInfo = animator.GetCurrentAnimatorStateInfo(stateLayerIndex);

        Debug.Log("stateInfo.length = "+ stateInfo.length);
        
        while (t <= clipLength)
        {
            float dt = 1f/60f;

            t += dt;

            Debug.Log(t);

            animator.Update(dt);

            yield return new EditorWaitForSeconds(dt);
        }

        Debug.Log("play end "+t+">"+stateInfo.length);
    }

    [ContextMenu("Run State")]
    public void RunState()
    {
        EditorCoroutineUtility.StartCoroutine(RunState_Routine(), this);
    }
}
