﻿using PawnSystem.Motors;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PawnSystem.Networking
{
    /// <summary>
    /// Basic network player class for IBasicPawnControls state replication (lookRot and moveDir)
    /// </summary>
    public class NetworkPlayerBasic : NetworkPlayerReplicator {

        public IBasicPawnControls pawn;

        [SyncVar]
        Quaternion lookRot;

        [SyncVar]
        Vector3 moveDir;

        protected override void Awake()
        {
            base.Awake();

            pawn = GetComponent<IBasicPawnControls>();
        }

        public override void OnLocalPlayerNetUpdate()
        {
            base.OnLocalPlayerNetUpdate();
            
            Cmd_SetServerLookMove(pawn.desiredRotation, pawn.desiredVelocity);
        }

        protected override void UpdateRemotePlayer()
        {
            base.UpdateRemotePlayer();

            pawn.desiredRotation = lookRot;
            pawn.desiredVelocity = moveDir;
        }

        protected override void UpdateServer()
        {
            base.UpdateServer();

            lookRot = pawn.desiredRotation;
            moveDir = pawn.desiredVelocity;
        }

        [Command]
        void Cmd_SetServerLookMove(Quaternion rot, Vector3 dir)
        {
            lookRot = rot;
            moveDir = dir;

            pawn.desiredRotation = lookRot;
            pawn.desiredVelocity = moveDir;
        }
    }
}