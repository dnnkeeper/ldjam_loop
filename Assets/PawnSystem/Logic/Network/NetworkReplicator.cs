﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;

namespace PawnSystem.Networking
{
    public interface INetControls
    {
        bool enabled
        {
            get;
            set;
        }

        void UpdateLocalControls();
        void EvaluateLocalControls();
        void Serialize(ref object[] objects);
        void Deserialize(ref object[] objects);
    }

    public interface INetReplicator
    {
        bool IsLocalPlayer();
    }

    public interface INetVectorReplicator : INetReplicator
    {
        void ReplicateVector(ref Vector3 vectorState);
    }

    public interface INetQuaternionReplicator : INetReplicator
    {
        void ReplicateQuaternion(ref Quaternion rotationState);
    }

    public interface INetBooleanReplicator : INetReplicator
    {
        void ReplicateBool(ref bool b);
    }

    public interface INetIntegerReplicator : INetReplicator
    {
        void ReplicateInt(ref int intState);
    }

    public interface INetBytesReplicator : INetReplicator
    {
        void ReplicateBytes(ref byte[] byteArray);
    }

    public class NetworkReplicator : NetworkBehaviour
    {
        [Range(0, 60f)]
        public int networkSendFrequency = 10;

        public int networkChannel = 1;

        protected static float THRESHOLD = Mathf.Epsilon;

        protected WaitForSeconds networkSendInterval;

        protected INetControls netInputComponent;

        protected object[] objects;

        virtual protected void Start()
        {
            netInputComponent = GetComponent<INetControls>();
            netInputComponent.enabled = false;

            foreach(MonoBehaviour component in GetComponents<MonoBehaviour>())
            {
                //Debug.Log("GetCommands "+ component);
                GetCommands(component);
            }
        }

        private void Update()
        {
            if (isLocalPlayer)
            {
                netInputComponent.UpdateLocalControls();
                netInputComponent.Serialize(ref objects);
                netInputComponent.EvaluateLocalControls();
            }
            else
            {
                netInputComponent.Deserialize(ref objects);
                netInputComponent.EvaluateLocalControls();
            }
            
        }

        public override int GetNetworkChannel()
        {
            return networkChannel;
        }

        public override float GetNetworkSendInterval()
        {
            return 1f/networkSendFrequency;
        }

        virtual public bool IsLocalPlayer()
        {
            return isLocalPlayer;
        }
        
        override public void OnStartLocalPlayer()
        {
            networkSendInterval = new WaitForSeconds(GetNetworkSendInterval());

            StartCoroutine(Do_NetUpdates());

            base.OnStartLocalPlayer();
        }

        virtual protected IEnumerator Do_NetUpdates()
        {
            while (isLocalPlayer)
            {
                SendCommands();
                yield return networkSendInterval;
            }
        }

        //Sending uNET Commands/RPCs with locally stored values; assigning values immideately if needed
        //NOTE: call it only if isLocalPlayer == true
        virtual protected void SendCommands()
        {
            //send some commands to server
        }

        public void GetCommands(MonoBehaviour component)
        {
            Dictionary<string, int> _dict = new Dictionary<string, int>();

            //Debug.Log(component + " type = " + component.GetType());

            MethodInfo[] methods = component.GetType().GetMethods(); 

            int i = 0;
            foreach (MethodInfo methodInfo in methods)
            {
                //Debug.Log(methodInfo);

                object[] attrs = methodInfo.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    SyncCommand syncCommandAttr = attr as SyncCommand;
                    if (syncCommandAttr != null)
                    {
                        string propName = methodInfo.Name;
                        _dict.Add(propName, i);
                        i++;
                        Debug.Log(propName+" ("+ methodInfo.GetParameters()[0].ParameterType + ") #" + i);
                    }
                }
            }
        }

        public void GetSyncEvents(MonoBehaviour component)
        {
            Dictionary<string, int> _dict = new Dictionary<string, int>();

            //Debug.Log(component + " type = " + component.GetType());

            EventInfo[] events = component.GetType().GetEvents();

            int i = 0;
            foreach (EventInfo eventInfo in events)
            {
                //Debug.Log(methodInfo);

                object[] attrs = eventInfo.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    SyncControlEvent syncCommandAttr = attr as SyncControlEvent;
                    if (syncCommandAttr != null)
                    {
                        string propName = eventInfo.Name;
                        _dict.Add(propName, i);
                        i++;
                        Debug.Log(propName + " (" + eventInfo.EventHandlerType + ") #" + i);
                    }
                }
            }
        }

    }

    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class SyncCommand : System.Attribute
    {
        public SyncCommand()
        {
        }
    }

    [System.AttributeUsage(System.AttributeTargets.Event)]
    public class SyncControlEvent: System.Attribute
    {
        public SyncControlEvent()
        {
        }
    }
}
