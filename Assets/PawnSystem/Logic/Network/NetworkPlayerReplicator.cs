﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Networking;

namespace PawnSystem.Networking
{
    public interface IControls
    {
        bool enabled
        {
            get;
            set;
        }

        /// <summary>
        /// UpdateControls method should process all local user input and store resulting controls somewhere.
        /// It will be called only on Local Player owning this object in multiplayer game.
        /// </summary>
        void UpdateControls();

        /// <summary>
        /// EvaluateControls should evaluate controls and feed them to all components requiring control.
        /// It will be called from network component when new controls arrives.
        /// </summary>
        void EvaluateControls();

        /// <summary>
        /// When state has changed significantly (e.q. new full state received from network) feed all components with it.
        /// </summary>
        void OnStateSync();
    }

    /// <summary>
    /// This class extends NetworkPlayerIdentity to simplify network synchronization of IControls objects. 
    /// It checks ownership of the object and invokes inputSource.UpdateControls() for owner and EvaluateControls() for everyone while inputSource.Update gets disabled.
    /// </summary>
    public abstract class NetworkPlayerReplicator : NetworkPlayerIdentity
    {
        [Range(0, 60f)]
        public int networkSendFrequency = 10;

        public int networkChannel = 1;

        protected static float THRESHOLD = Mathf.Epsilon;

        protected WaitForSeconds networkSendInterval;

        protected IControls inputSource;
        
        virtual protected void Awake()
        {
            inputSource = GetComponent<IControls>();
            //inputSource.enabled = false;
        }
        virtual protected void Update()
        {
            if (isLocalPlayer)
            {
                UpdateLocalPlayer();
            }
            else
            if (isClient)
            {
                UpdateRemotePlayer();
            }

            if (isServer)
            {
                UpdateServer();
            }            
        }

        /// <summary>
        /// Called every net update period of time only on Local Player owning this object.
        /// </summary>
        virtual public void OnLocalPlayerNetUpdate()
        {
            //CmdSendState(inputSource.state);
        }


        /// <summary>
        /// Called every frame on Local Player owning this object.
        /// </summary>
        virtual protected void UpdateLocalPlayer()
        {
            inputSource.UpdateControls();
            inputSource.EvaluateControls();
        }

        /// <summary>
        /// Called every frame on Server.
        /// </summary>
        virtual protected void UpdateServer()
        {
            inputSource.EvaluateControls();
        }

        /// <summary>
        /// Called every frame on Remote Client not owning this object.
        /// </summary>
        virtual protected void UpdateRemotePlayer()
        {
            inputSource.EvaluateControls();
        }

        override public int GetNetworkChannel()
        {
            return networkChannel;
        }

        override public float GetNetworkSendInterval()
        {
            return 1f/networkSendFrequency;
        }

        override public void OnStartLocalPlayer()
        {
            networkSendInterval = new WaitForSeconds(GetNetworkSendInterval());

            StartCoroutine(Do_NetUpdates());

            base.OnStartLocalPlayer();
        }

        private IEnumerator Do_NetUpdates()
        {
            while (isLocalPlayer && !isServer)
            {
                OnLocalPlayerNetUpdate();
                yield return networkSendInterval;
            }
        }
    }
}
