﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace PawnSystem.Networking
{

    public class PawnNetworkReplicator : NetworkReplicator, INetVectorReplicator, INetQuaternionReplicator
    {
        [SyncVar]
        public Vector3 moveDir;

        [SyncVar]
        public Quaternion lookRot;

        public void ReplicateVector(ref Vector3 vectorState)
        {
            if (isLocalPlayer)
            {
                moveDir = vectorState;
            }
            else
            {
                vectorState = moveDir;
            }
        }

        public void ReplicateQuaternion(ref Quaternion rotationState)
        {
            if (isLocalPlayer)
            {
                lookRot = rotationState;
            }
            else
            {
                rotationState = lookRot;
            }
        }

        public void ReplicateLookMove(ref Quaternion lookRotation, ref Vector3 moveDirection)
        {
            if (isLocalPlayer)
            {
                lookRot = lookRotation;
                moveDir = moveDirection;
            }
            else
            {
                lookRotation = lookRot;
                moveDirection = moveDir;
            }
        }

        override protected void SendCommands()
        {
            CmdSetRotation(lookRot);
            CmdSetVector(moveDir);
        }

        [Command]
        void CmdSetVector(Vector3 v)
        {
            moveDir = v;
        }

        [Command]
        void CmdSetRotation(Quaternion rot)
        {
            lookRot = rot;
        }
    }
}