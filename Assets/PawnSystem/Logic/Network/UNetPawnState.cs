﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using UPC.Utility;

#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif	

namespace PawnSystem.Networking
{
    public interface INetPawnState
    {
        bool IsLocalPlayer();
        void ReplicatePawnState(ref Vector3 moveDir, ref Quaternion lookRot);
    }

    public class UNetPawnState : NetworkBehaviour, INetPawnState
    {
        public enum CompressionType
        {
            COMPRESSION_Euler32x3,
            COMPRESSION_uint32,
            COMPRESSION_Euler16x3,
            COMPRESSION_Euler8x3
        }

        protected static float ANGLE_THRESHOLD = 0.001f;
        protected static float MOV_THRESHOLD = Mathf.Epsilon;

        [Header("Events")]

        [Tooltip("Should local player owning this object send rotation to the server?")]
        public bool sendRotation = true;

        [Tooltip("Should local player owning this object send mocement direction to the server?")]
        public bool sendMoveDir = true;

        [Space]

        [Header("Network")]

        [Tooltip("Should owner listen to server callbacks with his lookRot and moveDir?")]
        public bool callbacksListening = true;

        public CompressionType compression;

        //lookRot and moveDir values are used for invoking actions between network updates
        //local_value is updated localy when local player owns controls and is used when sending commands
        //transmitted_value keeps last transmitted value to prevent redundant data transmittion
        [SerializeField]
        protected Quaternion  transmitted_lookRot;
        [SerializeField]
        protected Vector3  transmitted_moveDir;
        [SerializeField]
        protected Vector3 direction;
        [SerializeField]
        protected Quaternion rotation;

        protected bool paused;

        public bool IsLocalPlayer()
        {
            return isLocalPlayer;
        }

        public void ReplicatePawnState(ref Vector3 moveDirection, ref Quaternion desiredRotation)
        {
            if (isLocalPlayer)
            {
                //Debug.Log("LOCAL PLAYER ReplicatePawnState "+ moveDirection);
                direction = moveDirection;
                rotation = desiredRotation;
            }
            else
            {
                moveDirection = direction;
                //Debug.Log("SET moveDirection = " + direction);
                desiredRotation = rotation;
            }
        }

        protected virtual void Awake()
        {
            rotation = transform.rotation;
        }

        protected virtual void OnApplicationFocus(bool focusStatus)
        {
            paused = !focusStatus;
        }

        protected WaitForSeconds networkSendInterval;

        override public void OnStartLocalPlayer()
        {
            networkSendInterval = new WaitForSeconds(GetNetworkSendInterval());

            StartCoroutine(Do_NetUpdates());

            base.OnStartLocalPlayer();
        }

        IEnumerator Do_NetUpdates()
        {
            while (isLocalPlayer)
            {
                SendCommands();
                yield return networkSendInterval;
            }
        }

        protected virtual void LateUpdate()
        {
            //direction = Vector3.zero;
        }

        //Sending uNET Commands/RPCs with locally stored values; assigning values immideately if needed
        //NOTE: call it only if isLocalPlayer == true
        protected virtual void SendCommands()
        {

            if (isServer)
            { //HOST: send RPCs to other clients and update local values

                if (sendRotation)
                {

                    if ((Quaternion.Angle(transmitted_lookRot, rotation) >= ANGLE_THRESHOLD))
                    {
                        //transmittedLookRot = local_lookRot;

                        switch (compression)
                        {
                            case CompressionType.COMPRESSION_Euler32x3:
                                RpcSetLookRotEuler(rotation.eulerAngles);
                                break;
                            case CompressionType.COMPRESSION_uint32:
                                RpcSetLookRot(rotation.compressQuat());
                                break;
                            case CompressionType.COMPRESSION_Euler16x3:
                                RpcSetLookRotShort(new ShortRotator(rotation));
                                break;
                            case CompressionType.COMPRESSION_Euler8x3:
                                RpcSetLookRotByte(new ByteRotator(rotation));
                                break;
                        }
                    }
                }
                
                if (sendMoveDir)
                {

                    if ((transmitted_moveDir - direction).sqrMagnitude >= MOV_THRESHOLD)
                    {
                        //transmittedMoveDir = local_moveDir;
                        RpcSetMoveDir(direction);
                    }
                }

            }
            else
            { //CLIENT: send COMMANDS to server and assign local values if needed

                if (sendRotation)
                {
                    if ((Quaternion.Angle(transmitted_lookRot, rotation) >= ANGLE_THRESHOLD))
                    {
                        //transmittedLookRot = local_lookRot;

                        switch (compression)
                        {
                            case CompressionType.COMPRESSION_Euler32x3:
                                CmdSetLookRotEuler(rotation.eulerAngles);
                                break;
                            case CompressionType.COMPRESSION_uint32:
                                CmdSetLookRot(rotation.compressQuat());
                                break;
                            case CompressionType.COMPRESSION_Euler16x3:
                                CmdSetLookRotShort(new ShortRotator(rotation));
                                break;
                            case CompressionType.COMPRESSION_Euler8x3:
                                CmdSetLookRotByte(new ByteRotator(rotation));
                                break;
                        }
                    }
                }

                if (sendMoveDir)
                {
                    if ((transmitted_moveDir - direction).sqrMagnitude >= MOV_THRESHOLD)
                    {
                        //Debug.Log("CmdSetMoveDir "+ direction);
                        //transmittedMoveDir = local_moveDir;
                        CmdSetMoveDir(direction);
                    }
                }

            }
        }

        Quaternion updatedLookRot;
        Vector3 updatedMoveDir;
        
        //ByteRotator compression (8-bit per angle)
        [Command]
        public virtual void CmdSetLookRotByte(ByteRotator lookRot)
        {
            this.rotation = lookRot.ToQuaternion();
            RpcSetLookRotByte(lookRot);
        }
        [ClientRpc]
        public virtual void RpcSetLookRotByte(ByteRotator val)
        {
            //Debug.Log("RpcSetLookRotByte");
            this.transmitted_lookRot = val.ToQuaternion();

            if (callbacksListening && !hasAuthority)
            {
                this.rotation = val.ToQuaternion();
            }
        }

        //ShortRotator compression (16-bit per angle)
        [Command]
        public virtual void CmdSetLookRotShort(ShortRotator lookRot)
        {
            this.rotation = lookRot.ToQuaternion();
            RpcSetLookRotShort(lookRot);
        }
        [ClientRpc]
        public virtual void RpcSetLookRotShort(ShortRotator val)
        {
            //Debug.Log("RpcSetLookRotShort");
            this.transmitted_lookRot = val.ToQuaternion();

            if (callbacksListening && !hasAuthority)
            {
                this.rotation = val.ToQuaternion();
            }
        }

        //Vector3 rotation (32-bit per angle)
        [Command]
        public virtual void CmdSetLookRotEuler(Vector3 val)
        {
            //Debug.Log("CmdSetLookRotEuler " + val);
            this.rotation = Quaternion.Euler(val);
            RpcSetLookRotEuler(val);
        }
        [ClientRpc]
        public virtual void RpcSetLookRotEuler(Vector3 val)
        {
            //Debug.Log("RpcSetLookRotEuler "+val);
            this.transmitted_lookRot = Quaternion.Euler(val);

            if (callbacksListening && !hasAuthority)
            {
                this.rotation = Quaternion.Euler(val);
            }
        }

        //uint rotation compression (32-bit quaternion)
        [Command]
        public virtual void CmdSetLookRot(uint cq)
        {
            rotation = cq.uncompressQuaterion();
            //Debug.Log (cq+" = "+lookRot);
            RpcSetLookRot(cq);
        }
        [ClientRpc]
        public virtual void RpcSetLookRot(uint cq)
        {
            //Debug.Log("RpcSetLookRot");
            this.transmitted_lookRot = cq.uncompressQuaterion();

            //if (!isLocalPlayer && !hasAuthority)
            if (callbacksListening && !hasAuthority)
            {
                this.rotation = cq.uncompressQuaterion();
            }
        }

        //Vector3 move direction remote call from owner client to server
        [Command]
        public virtual void CmdSetMoveDir(Vector3 val)
        {
            this.direction = val;
            //Debug.Log("RECEIVED CmdSetMoveDir " + val+" direction = "+direction);
            RpcSetMoveDir(val);
        }
        //Vector3 move direction remote call from server to everyone
        [ClientRpc]
        public virtual void RpcSetMoveDir(Vector3 val)
        {
            //Debug.Log("RpcSetMoveDir");
            this.transmitted_moveDir = val;

            //if (!isLocalPlayer && !hasAuthority)
            //if (callbacksListening && !hasAuthority)
            //{
                this.direction = val;
            //}
        }
        
    }
}
