﻿using UnityEngine;
using System.Collections;
using UnityEditor.SceneManagement;

#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

namespace PawnSystem.Motors
{
    [RequireComponent(typeof(IMotorControls))]
    public class InputController : MonoBehaviour
    {
        [Tooltip("Use some other object rotation as reference or calculate rotation here?")]
        public bool useRotationReference = false;
        public Transform rotationReference;

        public string lookXAxisName = "Mouse X";
        public string lookYAxisName = "Mouse Y";
        public string horizontalAxisName = "Horizontal";
        public string verticalAxisName = "Vertical";

        protected Quaternion desiredRotation;

        IMotorControls motor;

        private void Reset()
        {
            motor = GetComponent<IMotorControls>();
            
            if (motor == null)
            {
                motor = gameObject.AddComponent<RigidbodyCharacterMotor>();
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            motor = GetComponent<IMotorControls>();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateInput();
        }

        virtual protected void UpdateInput()
        {
            if (useRotationReference && rotationReference != null)
            {
                desiredRotation = rotationReference.rotation;

                motor.InputDesiredRotation(desiredRotation);
            }
            else
            {

#if CROSS_PLATFORM_INPUT
                float dX = CrossPlatformInputManager.GetAxis(lookXAxisName);

                float dY = CrossPlatformInputManager.GetAxis(lookYAxisName);

#else
                float dX = Input.GetAxis(lookXAxisName);

                float dY = Input.GetAxis(lookYAxisName);
#endif

                bool rotate = false;

#if UNITY_STANDALONE || UNITY_EDITOR
                if (Input.GetMouseButton(1))
                    rotate = true;
#else
                rotate = true;
#endif
                if (rotate)
                {
                    desiredRotation = Quaternion.Euler(new Vector3(0f, dX, 0f)) * desiredRotation; //global axis rotation

                    desiredRotation = desiredRotation * Quaternion.Euler(new Vector3(-dY, 0f, 0f)); //local axis rotation
                }

                motor.InputDesiredRotation(desiredRotation);
            }

#if CROSS_PLATFORM_INPUT
            float horizontalRaw = CrossPlatformInputManager.GetAxis(horizontalAxisName);
            float vericalRaw = CrossPlatformInputManager.GetAxis(verticalAxisName);

#else
            float horizontalRaw = Input.GetAxisRaw(horizontalAxisName);
            float vericalRaw = Input.GetAxisRaw(verticalAxisName);
#endif

            Vector3 moveDirection = new Vector3(horizontalRaw, 0f, vericalRaw);
            if (moveDirection == Vector3.zero)
                moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

            //Clamp maximum moveDir magnitude between 0 and 1
            if (new Vector2(moveDirection.x, moveDirection.z).magnitude > 1f)
            {
                moveDirection.Normalize();
            }

            moveDirection = desiredRotation * moveDirection;

            moveDirection = Vector3.ProjectOnPlane(moveDirection, transform.up);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                moveDirection += transform.up;
            }

            motor.InputMoveDirectionWorld(moveDirection);
        }
    }
}