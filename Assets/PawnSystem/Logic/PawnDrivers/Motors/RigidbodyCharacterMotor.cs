﻿using UnityEngine;
using System.Collections;
using UPC.Utility;
using System;

#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

//using PawnSystem.Networking;

namespace PawnSystem.Motors
{
    public interface IMotorControls
    {
        void InputDesiredRotation(Quaternion rotationReference);
        void InputDeltaRotation(float dX, float dY);
        void InputMoveDirectionLocal(Vector3 dir);
        void InputMoveDirectionWorld(Vector3 dir);

    }

    public enum CardinalDirection
    {
        IDLE,
        FORWARD,
        RIGHT,
        LEFT,
        BACKWARD
    }

    /// <summary>
    /// Character rigidbody motor for predictable acceleration and rotation
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyCharacterMotor : MonoBehaviour, IMotorControls
    {

        [SerializeField]
        Animator animator;

        public float rotationThreshold = 0;

        [SerializeField]
        bool isRotating;

        [SerializeField]
        float deltaRotationAngle;

        public float maxSpeed = 5f;

        [Tooltip("Maximum velocity change applied per second to strive maximum speed")]
        public float maxAcceleration = 5f;

        public float maxDeceleration = 5f;

        public float rotationSpeed = 10f;
        
        public float jumpForce = 5f;

        public float airSpeedMultiplier = 0.1f;

        public LayerMask groundLayerMask = 1;

        public float skinWidth = Mathf.Epsilon;

        //[ReadOnlyAttribute]
        public bool isGrounded;

        protected Vector3 _groundNormal;

        public float distanceToGround;
        
        protected float _lastJumpTime;

        protected Vector3 _desiredMoveDirection;

        [SerializeField]
        protected Vector3 _desiredMoveDirectionLocal;

        protected Quaternion _desiredRotation;

        public CardinalDirection cardinalDirection;

        protected Rigidbody _rb;
        protected Rigidbody rb
        {
            get { return (_rb ?? (_rb = GetComponent<Rigidbody>())); }
        }

        protected Collider _collider;
        new protected Collider collider
        {
            get { return (_collider ?? (_collider = GetComponent<Collider>())); }
        }

        public ConstantForce customGravity;

        protected Vector3 _customGravityVector;

        private void Reset()
        {
            animator = GetComponent<Animator>();
            if (animator == null)
                animator = GetComponentInChildren<Animator>();
        }

        public float cardinalAngle;

        virtual protected void Update()
        {
            float dt = Time.deltaTime;

            UpdateRotation(dt);

            if (animator != null)
            {
                animator.SetFloat("MoveDirX", _desiredMoveDirectionLocal.x);
                animator.SetFloat("MoveDirZ", _desiredMoveDirectionLocal.z);
                animator.SetBool("Accelerating", isAccelerating);
                animator.SetBool("Decelerating", isDecelerating);

            }
            if (_desiredMoveDirection.sqrMagnitude > 0f)
            {
                Vector3 desiredLookDirection = _desiredRotation * Vector3.forward;
                //cardinalAngle = Mathf.Rad2Deg * desiredLookDirection.AngleSignedWith(_desiredMoveDirection, transform.up);
                cardinalAngle = Mathf.Rad2Deg * transform.forward.AngleSignedWith(rb.velocity, transform.up);
                Debug.DrawRay(transform.position + transform.up, desiredLookDirection, Color.red);
                Debug.DrawRay(transform.position + transform.up, _desiredMoveDirection, Color.blue);

                animator.SetBool("MovingForward", false);
                animator.SetBool("MovingRight", false);
                animator.SetBool("MovingLeft", false);
                animator.SetBool("MovingBackward", false);

                if (cardinalAngle > -45f && cardinalAngle < 45f)
                {
                    cardinalDirection = CardinalDirection.FORWARD;
                    animator.SetBool("MovingForward", true);
                }
                else if (cardinalAngle > 45f && cardinalAngle < 135f)
                {
                    cardinalDirection = CardinalDirection.RIGHT;
                    animator.SetBool("MovingRight", true);
                }
                else if (cardinalAngle < -45f && cardinalAngle > -135f)
                {
                    cardinalDirection = CardinalDirection.LEFT;
                    animator.SetBool("MovingLeft", true);
                }
                else if (cardinalAngle > 135f || cardinalAngle < -135f)
                {
                    cardinalDirection = CardinalDirection.BACKWARD;
                    animator.SetBool("MovingBackward", true);
                }
            }
            else
            {
                cardinalDirection = CardinalDirection.IDLE;
            }
        }

        protected virtual void FixedUpdate()
        {
            ApplyForces();
        }

        virtual public void InputDesiredRotation(Quaternion rotationReference)
        {
            _desiredRotation = rotationReference;
        }

        virtual public void InputDeltaRotation(float dX, float dY)
        {
            _desiredRotation = Quaternion.Euler(new Vector3(0f, dX, 0f)) * _desiredRotation; //global axis rotation

            _desiredRotation = _desiredRotation * Quaternion.Euler(new Vector3(-dY, 0f, 0f)); //local axis rotation

            _desiredRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(_desiredRotation * Vector3.forward, Vector3.up));
        }

        virtual public void InputMoveDirectionLocal(Vector3 localDir)
        {
            _desiredMoveDirectionLocal = localDir;
            _desiredMoveDirection = rb.transform.rotation * localDir;
            OnDirectionUpdated();
        }

        virtual public void InputMoveDirectionWorld(Vector3 dir)
        {
            _desiredMoveDirection = dir;
            _desiredMoveDirectionLocal = Quaternion.Inverse(rb.transform.rotation) * dir;
            OnDirectionUpdated();
        }

        public virtual void OnDirectionUpdated()
        {
            if (_desiredMoveDirectionLocal.y > 0.5f)
            {
                Jump();
            }
        }

        public virtual void Jump()
        {
            if (isGrounded && Time.time - _lastJumpTime > 0.5f)
            {
                _lastJumpTime = Time.time;
                isGrounded = false;
                rb.AddForce(transform.up * jumpForce, ForceMode.VelocityChange);
            }
        }

        private void UpdateRotation(float dt)
        {
            _desiredRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(_desiredRotation * Vector3.forward, -_customGravityVector), -_customGravityVector);

            if (!isDecelerating && rotationSpeed > 0)
            {
                Quaternion rot =
                    //desiredRotation;
                    //Quaternion.RotateTowards(rb.rotation, desiredRotation != Quaternion.identity ? desiredRotation : rb.rotation, rotationSpeed * dt * 60f);
                    Quaternion.Lerp(rb.rotation, _desiredRotation != Quaternion.identity ? _desiredRotation : rb.rotation, rotationSpeed * dt);
                //Quaternion.Lerp(transform.rotation, desiredRotation != Quaternion.identity ? desiredRotation : transform.rotation, 60f * Time.deltaTime);

                //transform.rotation = rot;
                //rb.rotation = rot;
                rb.MoveRotation(rot);
            }
        }

        public void OnCustomGravity(ConstantForce customForce)
        {
            Debug.Log("customGravity = " + customForce);
            customGravity = customForce;
        }

        bool CheckIsGrounded(out float distanceToGround)
        {
            distanceToGround = Mathf.Infinity;
            return CheckGroundDistance(collider, skinWidth, groundLayerMask, _customGravityVector, out distanceToGround, out _groundNormal);
        }

        public static bool CheckGroundDistance(Collider collider, float skinWidth, LayerMask collisionMask, Vector3 gravityVector, out float distanceToGround, out Vector3 groundNormal)
        {
            Transform colliderTransform = collider.transform;

            groundNormal = colliderTransform.up;

            float height = Vector3.Project(collider.bounds.size, gravityVector).magnitude;

            distanceToGround = height*10f;
            //Debug.LogError(height);

            float halfHeight = height * 0.5f;

            if (Vector3.Angle(colliderTransform.up, -gravityVector) < 30.0f)
            {
                RaycastHit hit;
                Vector3 colliderCenter = colliderTransform.position + colliderTransform.rotation * collider.transform.InverseTransformPoint(collider.bounds.center);

                Vector3 capsuleFloor = colliderCenter - colliderTransform.up * halfHeight;

                Ray rayToGround = new Ray(colliderCenter, capsuleFloor - colliderCenter);

                Vector3 projectedExtents = Vector3.ProjectOnPlane(collider.bounds.extents, gravityVector);
                float castRadius = Mathf.Max(projectedExtents.x, projectedExtents.y) * (0.99f);

                float groundTestDistance = Mathf.Max(0.01f, halfHeight - castRadius);
                float castDistance = height*1.5f;

                //Debug.DrawLine(colliderCenter, colliderCenter + collider.transform.right * castRadius, Color.red, Time.deltaTime);
                //Debug.DrawLine(colliderCenter, colliderCenter + collider.transform.forward * castRadius, Color.red, Time.deltaTime);
                //Debug.DrawLine(colliderCenter, colliderCenter + collider.transform.up * halfHeight, Color.red, Time.deltaTime);
                
                //if (Physics.Raycast(rayToGround, out hit, height, collisionMask))
                if (Physics.SphereCast(rayToGround, castRadius, out hit, castDistance, collisionMask))
                {
                    groundNormal = hit.normal;

                    distanceToGround = hit.distance - groundTestDistance;

                    if (distanceToGround < skinWidth)
                    {
                        //Debug.DrawLine(rayToGround.origin, hit.point, Color.green, Time.fixedDeltaTime);
                        return true;
                    }
                    else
                    {
                        Debug.DrawLine(rayToGround.origin, hit.point, Color.yellow, Time.fixedDeltaTime);
                    }
                }
                else
                {
                    Debug.DrawLine(rayToGround.origin, rayToGround.origin + rayToGround.direction * castDistance, Color.red, Time.fixedDeltaTime);
                }
            }
            else
            {
                //Debug.DrawRay(capsuleColliderTransform.position, capsuleColliderTransform.up, Color.red, Time.deltaTime);
                //Debug.DrawRay(colliderTransform.position, -gravityVector, Color.yellow, Time.deltaTime);
            }

            return false;
        }

        public float acceleration;

        public bool isDecelerating;
        public bool isAccelerating;

        Vector3 desiredVelocity;

        public float currentSpeed;
        
        [SerializeField]
        Vector3 localAccelerationVector;
        protected virtual void ApplyForces()
        {
            float dt = Time.fixedDeltaTime;

            _customGravityVector = rb.useGravity ? Physics.gravity : Vector3.zero;

            if (customGravity != null)
            {
                _customGravityVector += customGravity.force;
            }

            bool b = CheckIsGrounded(out distanceToGround);
            if (isGrounded != b)
            {
                isGrounded = b;
                //Debug.Log(b ? "OnGrounded" : "InAir");
                SendMessage(b ? "OnGrounded" : "InAir", SendMessageOptions.DontRequireReceiver);
            }

            float modifier = (isGrounded ? 1f : airSpeedMultiplier);

            currentSpeed = rb.velocity.magnitude;

            isAccelerating = (_desiredMoveDirection.magnitude > 0 && (currentSpeed < 0.01f || Vector3.Dot(rb.velocity, _desiredMoveDirection) > 0) );

            isDecelerating = (currentSpeed > 0.01f && !isAccelerating);
            
            if (isDecelerating)
                desiredVelocity = Vector3.MoveTowards(desiredVelocity, Vector3.zero, maxDeceleration * dt);
            else
                desiredVelocity = Vector3.MoveTowards(desiredVelocity, Vector3.ProjectOnPlane(_desiredMoveDirection, -_customGravityVector).normalized * maxSpeed, (isAccelerating ? maxAcceleration : maxDeceleration) * dt);

            Vector3 velocityChange = desiredVelocity - Vector3.ProjectOnPlane(rb.velocity, -_customGravityVector.normalized);
            
            velocityChange *= modifier;

            Vector3 accelerationVector = velocityChange / dt;

            acceleration = accelerationVector.magnitude; 

            if (accelerationVector.sqrMagnitude > 0.01f)
            {
                rb.AddForce(accelerationVector, ForceMode.Acceleration);
            }
            else
            {
                isDecelerating = isAccelerating = false;
            }

            if (isDecelerating)
                acceleration *= -1f;

            //Debug.DrawRay (transform.position, desiredVelocity, Color.blue, dt);

            //_desiredMoveDirection = Vector3.zero;
        }

        private void LateUpdate()
        {
            //_desiredMoveDirection = Vector3.zero;
        }
    }
}