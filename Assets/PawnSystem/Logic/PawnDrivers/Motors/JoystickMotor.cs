﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if CROSSPLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

public class JoystickMotor : MonoBehaviour
{
    public string horizontalAxisName = "Horizontal";
    public string verticalAxisName = "Vertical";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    Vector3 rotationEuler;

    // Update is called once per frame
    void Update()
    {
#if CROSSPLATFORM_INPUT
        float dX = CrossPlatformInputManager.GetAxis(horizontalAxisName);

        float dY = CrossPlatformInputManager.GetAxis(verticalAxisName);
#endif
        float dX = Input.GetAxis(horizontalAxisName);

        float dY = Input.GetAxis(verticalAxisName);

        rotationEuler = Vector3.Lerp(rotationEuler, Vector3.zero, Time.deltaTime * 5f);

        rotationEuler = new Vector3(dY * 0.2f, 0, -dX * 0.2f);

        rotationEuler.x = Mathf.Clamp(rotationEuler.x, -5f, 5f);
        rotationEuler.z = Mathf.Clamp(rotationEuler.z, -5f, 5f);

        transform.localRotation = Quaternion.Euler(rotationEuler);
    }
}
