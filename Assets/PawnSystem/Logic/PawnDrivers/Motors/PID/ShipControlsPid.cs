﻿using System;
using UnityEngine;
using System.Collections;

namespace Motors{

	public class ShipControlsPid : MonoBehaviour {

		public Vector3 thrust = new Vector3(1,1,1);     //Total thrust per axis
		public Vector3 maxForces;                       //the amount of torque available for each axis, based on thrust

		protected Vector3 targetVelocity;               //user input determines how fast user wants ship to rotate
		protected Vector3 curVelocity;                  //holds the rigidbody.angularVelocity converted from world space to local

		public Vector3 maxV = new Vector3(4,4,4);       //max desired rate of change

		public Vector3 Kp = new Vector3(4, 4, 4);
		public Vector3 Ki = new Vector3(.007f,.007f,.007f);
		public Vector3 Kd = new Vector3(0,0,0); 

		protected PidController3Axis pControl = new PidController3Axis();

		public Vector3 inputs;

		protected Rigidbody rb;

		void Start() {
			Initialize();
			rb = GetComponent<Rigidbody> ();
		}

		protected virtual void Initialize(){        
			ApplyValues();  
		}

		protected virtual void ApplyValues(){
			SetForces();

			pControl.Kp = Kp;
			pControl.Ki = Ki;
			pControl.Kd = Kd;
			pControl.outputMax = maxForces;
			pControl.outputMin = maxForces * -1;
			pControl.SetBounds();
		}

		protected virtual void SetForces(){
			//this is where the bounding box is used to create pseudo-realistic torque;  If you want more detail, just ask.
			var shipExtents = ((MeshFilter)GetComponentInChildren(typeof(MeshFilter))).mesh.bounds.extents;
			maxForces.x = new Vector2(shipExtents.y,shipExtents.z).magnitude*thrust.x;
			maxForces.y = new Vector2(shipExtents.x,shipExtents.z).magnitude*thrust.y;  //normally would be x and z, but mesh is rotated 90 degrees in mine.  
			maxForces.z = new Vector2(shipExtents.x,shipExtents.y).magnitude*thrust.z;  //normally would be x and y, but mesh is rotated 90 degrees in mine.        
		}

		protected float getI(string axis) {
			return Input.GetAxisRaw(axis);          
		}

		protected Vector3 PitchYawRoll(Quaternion q){
			return new Vector3(
							Mathf.Atan2(2*q.x*q.w - 2*q.y*q.z, 1 - 2*q.x*q.x - 2*q.z*q.z),
							Mathf.Atan2(2*q.y*q.w - 2*q.x*q.z, 1 - 2*q.y*q.y - 2*q.z*q.z),
							Mathf.Asin(2*q.x*q.y + 2*q.z*q.w));
		}

		public void SetRotation(Quaternion rot){
			inputs = PitchYawRoll ( Quaternion.Inverse(transform.rotation) * rot ) ;
		}

		protected virtual void SetVelocities(){
			// collect inputs
			//var inputs = new Vector3(getI("Vertical"),getI("Horizontal"),0f);
			targetVelocity = Vector3.Scale (inputs, maxV);

			//take the rigidbody.angularVelocity and convert it to local space; we need this for comparison to target rotation velocities
			curVelocity = transform.InverseTransformDirection(rb.angularVelocity);
		}

		protected virtual void SetOutputs(){
			rb.AddRelativeTorque(pControl.output * Time.fixedDeltaTime, ForceMode.Impulse);  
		}

		protected virtual void RCS() {
			// Uncomment to catch inspector changes 
			ApplyValues();

			SetVelocities();                

			// run the controller
			pControl.Cycle(curVelocity, targetVelocity, Time.fixedDeltaTime);
			SetOutputs();
		}

		void FixedUpdate() {
			RCS();
		}

	}
}