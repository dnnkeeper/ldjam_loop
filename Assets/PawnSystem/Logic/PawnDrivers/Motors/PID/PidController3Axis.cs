﻿using UnityEngine;
using System.Collections;
using System;

public class PidController3Axis {

	public Vector3 Kp;
	public Vector3 Ki;
	public Vector3 Kd;

	public Vector3 outputMax;
	public Vector3 outputMin;

	public Vector3 preError;

	public Vector3 integral;
	public Vector3 integralMax;
	public Vector3 integralMin;

	public Vector3 output;

	public void SetBounds(){
		integralMax = Divide(outputMax, Ki);
		integralMin = Divide(outputMin, Ki);        
	}

	public Vector3 Divide(Vector3 a, Vector3 b){
		Func<float, float> inv = (n) => 1/(n != 0? n : 1);
		var iVec = new Vector3(inv(b.x), inv(b.x), inv(b.z));
		return Vector3.Scale (a, iVec);
	}

	public Vector3 MinMax(Vector3 min, Vector3 max, Vector3 val){
		return Vector3.Min(Vector3.Max(min, val), max);
	}

	public Vector3 Cycle(Vector3 PV, Vector3 setpoint, float Dt){
		var error = setpoint - PV;
		integral = MinMax(integralMin, integralMax, integral + (error * Dt));

		var derivative = (error - preError) / Dt;
		output = Vector3.Scale(Kp,error) + Vector3.Scale(Ki,integral) + Vector3.Scale(Kd,derivative);
		output = MinMax(outputMin, outputMax, output);

		preError = error;
		return output;
	}
}