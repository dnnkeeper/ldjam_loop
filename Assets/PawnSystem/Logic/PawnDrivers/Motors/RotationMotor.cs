﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace PawnSystem.Motors
{
    public class RotationMotor : MonoBehaviour
    {
        public bool fixedUpdate;

        [Range(0.1f, 60f)]
        public float rotationSpeed = 10.0f;

        public Vector3 discreteRotation = new Vector3(0,0,0);

        protected Quaternion desiredRotation;

        public Vector3 worldAxisRotation = new Vector3(0f,1f,0f);
        public Vector3 localAxisRotation = new Vector3(-1f, 0, 0f);

        // Use this for initialization
        protected virtual void Start()
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            if (rb != null)
                rb.constraints = RigidbodyConstraints.FreezeRotation;
            desiredRotation = transform.rotation;
        }

        public void OnDrag(BaseEventData eventData)
        {
            Vector2 swipeDelta = ((PointerEventData)eventData).delta;

            swipeDelta.x = swipeDelta.x/Screen.currentResolution.width * 400f;
            swipeDelta.y = swipeDelta.y/Screen.currentResolution.height * 300f;

            Rotate(new Vector3(swipeDelta.y, swipeDelta.x, 0f) );
        }

        public void Rotate(Vector3 eulerDeltaRotation)
        {
            Vector3 worldEulerDelta = new Vector3(worldAxisRotation.x * eulerDeltaRotation.x, worldAxisRotation.y * eulerDeltaRotation.y, worldAxisRotation.z * eulerDeltaRotation.z);
            Vector3 localEulerDelta = new Vector3(localAxisRotation.x * eulerDeltaRotation.x, localAxisRotation.y * eulerDeltaRotation.y, localAxisRotation.z * eulerDeltaRotation.z);

            desiredRotation = Quaternion.Euler(worldEulerDelta) * desiredRotation;
            desiredRotation = desiredRotation * Quaternion.Euler(localEulerDelta);
        }

        public void SetRotation(Quaternion rot)
        {
            desiredRotation = rot;
        }

        void FixedUpdate()
        {
            if (fixedUpdate)
                Tick();
        }

        void Update()
        {
            if (!fixedUpdate)
                Tick();
        }

        void Tick()
        {
            if (discreteRotation.sqrMagnitude > 0f)
            {
                var eulerAngles = desiredRotation.eulerAngles;

                if (discreteRotation.x > 0)
                    eulerAngles.x = Mathf.Abs(Mathf.Round(eulerAngles.x / discreteRotation.x)) * discreteRotation.x;
                if (discreteRotation.y > 0)
                    eulerAngles.y = Mathf.Abs(Mathf.Round(eulerAngles.y / discreteRotation.y)) * discreteRotation.y;
                if (discreteRotation.z > 0)
                    eulerAngles.z = Mathf.Abs(Mathf.Round(eulerAngles.z / discreteRotation.z)) * discreteRotation.z;

                desiredRotation = Quaternion.Lerp(desiredRotation, Quaternion.Euler(eulerAngles), Time.deltaTime * rotationSpeed * 0.5f);
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, desiredRotation, Time.deltaTime * rotationSpeed);
        }
    }
}