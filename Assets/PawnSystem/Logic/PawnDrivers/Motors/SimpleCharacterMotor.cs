﻿using UnityEngine;
using System.Collections;
#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif
using PawnSystem.Networking;

namespace PawnSystem.Motors
{
	public class SimpleCharacterMotor : MonoBehaviour {

		public bool relativeMotion, fixedUpdate;

		protected Vector3 moveDir;
        protected Quaternion lookRotation;

        protected bool jump;

	    public CharacterController characterController;

	    public float speed = 5f;

	    public float m_JumpSpeed = 5f;

        public string lookXAxisName = "Mouse X";
        public string lookYAxisName = "Mouse Y";
        public string horizontalAxisName = "Horizontal";
        public string verticalAxisName = "Vertical";

        [Tooltip("Use some other object rotation as reference or calculate rotation here?")]
        public bool useRotationReference = false;
        public Transform rotationReference;

        Vector3 m_MoveDir;
        
        private void Awake()
        {
            lookRotation = transform.rotation;
        }

        INetPawnState netPawnState;

        private void OnEnable()
        {
            netPawnState = GetComponent<INetPawnState>();
        }

        public void SetMoveDir(Vector3 val){
			moveDir = relativeMotion? transform.TransformDirection (val) : val;
		}

        public void SetLookRot(Quaternion rot)
        {
            lookRotation = Quaternion.LookRotation( Vector3.ProjectOnPlane( rot * Vector3.forward, Vector3.up) );

            //Debug.DrawRay(transform.position, new_rot * Vector3.forward, Color.blue);
            //Debug.DrawRay(transform.position, new_rot * Vector3.up, Color.green);
            //Debug.DrawRay(transform.position, new_rot * Vector3.right, Color.red); 

            //characterController.transform.rotation = lookRotation;
        }

        public void Jump(){
			jump = true;
		}

		void FixedUpdate(){
			if (fixedUpdate)
				Tick ();
		}

		void Update()
	    {
            if (netPawnState != null)
            {
                if (netPawnState.IsLocalPlayer())
                {
                    UpdateInput();
                }
                netPawnState.ReplicatePawnState(ref moveDir, ref lookRotation);
            }
            else
            {
                UpdateInput();
            }

            if (!fixedUpdate)
				Tick ();
	    }

        void UpdateInput()
        {
            if (useRotationReference && rotationReference != null)
            {
                lookRotation = rotationReference.rotation;
            }
            else
            {

#if CROSS_PLATFORM_INPUT
                float dX = CrossPlatformInputManager.GetAxis(lookXAxisName);
                float dY = CrossPlatformInputManager.GetAxis(lookYAxisName);
#else
                float dX = Input.GetAxis(lookXAxisName);
                float dY = Input.GetAxis(lookYAxisName);
#endif

                bool rotate = false;

#if UNITY_STANDALONE || UNITY_EDITOR
                if (Input.GetMouseButton(1))
#endif
                    rotate = true;

                if (rotate)
                {
                    lookRotation = Quaternion.Euler(new Vector3(0f, dX, 0f)) * lookRotation; //global axis rotation

                    lookRotation = lookRotation * Quaternion.Euler(new Vector3(-dY, 0f, 0f)); //local axis rotation
                }
            }

            lookRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(lookRotation * Vector3.forward, Vector3.up));

#if CROSS_PLATFORM_INPUT
            float horizontalRaw = CrossPlatformInputManager.GetAxis(horizontalAxisName);
            float vericalRaw = CrossPlatformInputManager.GetAxis(verticalAxisName);

#else
            float horizontalRaw = Input.GetAxisRaw(horizontalAxisName);
            float vericalRaw = Input.GetAxisRaw(verticalAxisName);
#endif

            moveDir = new Vector3(horizontalRaw, 0f, vericalRaw);
            if (moveDir == Vector3.zero)
                moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

            //Clamp maximum moveDir magnitude between 0 and 1
            if (new Vector2(moveDir.x, moveDir.z).magnitude > 1f)
            {
                moveDir.Normalize();
            }

            moveDir = characterController.transform.rotation * moveDir;
        }

		void Tick(){

            characterController.transform.rotation = Quaternion.Lerp(characterController.transform.rotation, lookRotation, Time.deltaTime * 10f);
            
            if (characterController.isGrounded)
			{
				m_MoveDir.x = moveDir.x * speed;
				m_MoveDir.y = -5.0f * Time.deltaTime;
				m_MoveDir.z = moveDir.z * speed;

				if (jump)
				{
					m_MoveDir.y = m_JumpSpeed;
					jump = false;
					//PlayJumpSound();
				}
			}
			else
			{
				m_MoveDir += Physics.gravity * Time.deltaTime ;
			}

			characterController.Move(m_MoveDir * Time.deltaTime);

		}
	}
}