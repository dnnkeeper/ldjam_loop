﻿using UnityEngine;
using System.Collections;

#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

//using PawnSystem.Networking;

namespace PawnSystem.Motors
{

    /// <summary>
    /// Simple rigidbody motor that exposes public methods for accelerating body at given directions
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyMotor : MonoBehaviour {

        //public bool fixedUpdate;

        public string lookXAxisName = "Mouse X";
        public string lookYAxisName = "Mouse Y";
        public string horizontalAxisName = "Horizontal";
        public string verticalAxisName = "Vertical";

        [Tooltip("Use some other object rotation as reference or calculate rotation here?")]
        public bool useRotationReference = false;
        public Transform rotationReference;

        [Tooltip("Treat input vectors as world space directions or rotate them according to rigidbody rotation before applying forces")]
        public bool worldSpace = true;

        public float maxSpeed = 5f;

        [Tooltip("Maximum velocity change applied per second to strive maximum speed")]
        public float maxAcceleration = 10f;

        public float maxDeceleration = 15f;

        public float rotationSpeed = 10f;
        
        public float jumpForce = 5f;

        public float airSpeedMultiplier = 0.1f;

        public LayerMask groundLayerMask = 1;

        public float skinWidth = Mathf.Epsilon;

        //[ReadOnlyAttribute]
        public bool bGrounded;

        protected Vector3 groundNormal;
        public float distanceToGround;
        
        protected float lastJumpTime;

        [SerializeField]
        protected Vector3 moveDirection;
        protected Quaternion desiredRotation;

        public AnimationCurve directionSpeedModCurve = new AnimationCurve(new Keyframe(0f,1f), new Keyframe(90f, 0.5f), new Keyframe(180f, 0.3f));

        protected Rigidbody _rb;
        protected Rigidbody rb
        {
            get { return (_rb ?? (_rb = GetComponent<Rigidbody>())); }
        }

        protected Collider _collider;
        new protected Collider collider
        {
            get { return (_collider ?? (_collider = GetComponent<Collider>())); }
        }

        public ConstantForce customGravity;
        protected Vector3 customGravityVector;

       
        public virtual Vector3 GetCurrentVelocity()
        {
            return rb.velocity;
        }

        public virtual float GetCurrentSpeed()
        {
            return Vector3.ProjectOnPlane(rb.velocity, -transform.up).magnitude;
        }

        public virtual bool GetIsGrounded()
        {
            return bGrounded;
        }
        
        public virtual void Jump()
        {
           if (bGrounded && Time.time - lastJumpTime > 0.5f)
            {
                lastJumpTime = Time.time;
                bGrounded = false;
                rb.AddForce(transform.up * jumpForce, ForceMode.VelocityChange);
            }
        }

        //INetPawnState netPawnState;

        [SerializeField]
        Animator animator;
        private void Start()
        {
            //animator = GetComponent<Animator>() ?? GetComponentsInChildren<Animator>();
        }
        private void OnEnable()
        {
            //netPawnState = GetComponent<INetPawnState>();
        }

        protected virtual void FixedUpdate()
        {
            //UpdateRotation(Time.fixedDeltaTime);

            ApplyForces();
        }

        virtual protected void Update()
        {
            float dt = Time.deltaTime;

            /*if (netPawnState != null)
            {
                if (netPawnState.IsLocalPlayer())
                {
                    UpdateInput();
                }
                netPawnState.ReplicatePawnState(ref moveDirection, ref desiredRotation);
            }
            else
            {*/
            UpdateInput();
            //}

            UpdateRotation(dt);
        }
        public float rotationThreshold = 0;
        [SerializeField]
        bool isRotating;
        [SerializeField]
        float deltaRotationAngle;
        private void UpdateRotation(float dt)
        {

            desiredRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(desiredRotation * Vector3.forward, -customGravityVector), -customGravityVector);

            if (rotationSpeed > 0)
            {
                              
                Quaternion rot =
                    //desiredRotation;
                    //Quaternion.RotateTowards(rb.rotation, desiredRotation != Quaternion.identity ? desiredRotation : rb.rotation, rotationSpeed * dt * 60f);
                    Quaternion.Lerp(rb.rotation, desiredRotation != Quaternion.identity ? desiredRotation : rb.rotation, rotationSpeed * dt);
                //Quaternion.Lerp(transform.rotation, desiredRotation != Quaternion.identity ? desiredRotation : transform.rotation, 60f * Time.deltaTime);

                //transform.rotation = rot;
                //rb.rotation = rot;
                rb.MoveRotation(rot);
                
            }
        }

        virtual protected void UpdateInput() { 
            if (useRotationReference && rotationReference != null)
            {
                desiredRotation = rotationReference.rotation;
            }
            else
            {

#if CROSS_PLATFORM_INPUT
                float dX = CrossPlatformInputManager.GetAxis(lookXAxisName);

                float dY = CrossPlatformInputManager.GetAxis(lookYAxisName);

#else
                float dX = Input.GetAxis(lookXAxisName);
                float dY = Input.GetAxis(lookYAxisName);
#endif

                bool rotate = false;

#if UNITY_STANDALONE || UNITY_EDITOR
                if (Input.GetMouseButton(1))
#endif
                    rotate = true;

                if (rotate)
                {
                    desiredRotation = Quaternion.Euler(new Vector3(0f, dX, 0f)) * desiredRotation; //global axis rotation

                    desiredRotation = desiredRotation * Quaternion.Euler(new Vector3(-dY, 0f, 0f)); //local axis rotation
                }
            }

            desiredRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(desiredRotation * Vector3.forward, Vector3.up));


#if CROSS_PLATFORM_INPUT
            float horizontalRaw = CrossPlatformInputManager.GetAxis(horizontalAxisName);
            float vericalRaw = CrossPlatformInputManager.GetAxis(verticalAxisName);

#else
            float horizontalRaw = Input.GetAxisRaw(horizontalAxisName);
            float vericalRaw = Input.GetAxisRaw(verticalAxisName);
#endif

            moveDirection = new Vector3(horizontalRaw, 0f, vericalRaw);
            if (moveDirection == Vector3.zero)
                moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

            //Clamp maximum moveDir magnitude between 0 and 1
            if (new Vector2(moveDirection.x, moveDirection.z).magnitude > 1f)
            {
                moveDirection.Normalize();
            }

            if (animator != null)
            {
                animator.SetFloat("MoveDirX", moveDirection.x);
                animator.SetFloat("MoveDirZ", moveDirection.z);
            }

            if (useRotationReference && rotationReference != null)
                moveDirection = rotationReference.rotation * moveDirection;
            else
                moveDirection = rb.transform.rotation * moveDirection;

            deltaRotationAngle = Vector3.Angle((desiredRotation * Vector3.forward), moveDirection);

            if (moveDirection.sqrMagnitude > 0f && deltaRotationAngle < 50f)
            { 
                desiredRotation = Quaternion.LookRotation(moveDirection, transform.up);
            }

            
        }

       public void OnCustomGravity(ConstantForce customForce)
        {
            Debug.Log("customGravity = " + customForce);
            customGravity = customForce;
        }

        bool CheckIsGrounded(out float distanceToGround)
        {
            distanceToGround = Mathf.Infinity;
            return CheckGroundDistance(collider, skinWidth, groundLayerMask, customGravityVector, out distanceToGround, out groundNormal);
        }

        public static bool CheckGroundDistance(Collider collider, float skinWidth, LayerMask collisionMask, Vector3 gravityVector, out float distanceToGround, out Vector3 groundNormal)
        {
            Transform colliderTransform = collider.transform;

            groundNormal = colliderTransform.up;

            float height = Vector3.Project(collider.bounds.size, Vector3.up).magnitude;

            distanceToGround = height*10f;
            //Debug.LogError(height);

            float halfHeight = height * 0.5f;

            if (Vector3.Angle(colliderTransform.up, -gravityVector) < 30.0f)
            {
                RaycastHit hit;
                Vector3 colliderCenter = colliderTransform.position + colliderTransform.rotation * collider.transform.InverseTransformPoint(collider.bounds.center);

                Vector3 capsuleFloor = colliderCenter - colliderTransform.up * halfHeight;

                Ray rayToGround = new Ray(colliderCenter, capsuleFloor - colliderCenter);

                Vector3 projectedExtents = Vector3.ProjectOnPlane(collider.bounds.extents, gravityVector);
                float castRadius = Mathf.Max(projectedExtents.x, projectedExtents.y) * 0.5f;

                float groundTestDistance = Mathf.Max(0.001f, halfHeight - castRadius);
                float castDistance = height*1.5f;

                // Debug.DrawLine(colliderCenter, colliderCenter + collider.transform.right * castRadius, Color.red, Time.deltaTime);
                // Debug.DrawLine(colliderCenter, colliderCenter + collider.transform.forward * castRadius, Color.red, Time.deltaTime);
                // Debug.DrawLine(colliderCenter, colliderCenter + collider.transform.up * halfHeight, Color.red, Time.deltaTime);
                
                //if (Physics.Raycast(rayToGround, out hit, height, collisionMask))
                if (Physics.SphereCast(rayToGround, castRadius, out hit, castDistance, collisionMask))
                {
                    groundNormal = hit.normal;

                    distanceToGround = hit.distance - groundTestDistance;

                    if (distanceToGround < skinWidth)
                    {
                        Debug.DrawLine(rayToGround.origin, hit.point, Color.green, Time.fixedDeltaTime);
                        return true;
                    }
                    else
                    {
                        Debug.DrawLine(rayToGround.origin, hit.point, Color.yellow, Time.fixedDeltaTime);
                    }
                }
                else
                {
                    Debug.DrawLine(rayToGround.origin, rayToGround.origin + rayToGround.direction * castDistance, Color.red, Time.fixedDeltaTime);
                }
            }
            else
            {
                //Debug.DrawRay(capsuleColliderTransform.position, capsuleColliderTransform.up, Color.red, Time.deltaTime);
                //Debug.DrawRay(colliderTransform.position, -gravityVector, Color.yellow, Time.deltaTime);
            }

            return false;
        }

        [SerializeField]
        Vector3 localAccelerationVector;
        protected virtual void ApplyForces()
        {
            float dt = Time.fixedDeltaTime;

            customGravityVector = rb.useGravity ? Physics.gravity : Vector3.zero;

            if (customGravity != null)
            {
                customGravityVector += customGravity.force;
            }

            bool b = CheckIsGrounded(out distanceToGround);
            if (bGrounded != b)
            {
                bGrounded = b;
                //Debug.Log(b ? "OnGrounded" : "InAir");
                SendMessage(b ? "OnGrounded" : "InAir", SendMessageOptions.DontRequireReceiver);
            }

            float modifier = (bGrounded ? 1f : airSpeedMultiplier);

            Vector3 desiredVelocity = Vector3.ProjectOnPlane((worldSpace ? moveDirection : transform.rotation * moveDirection), -customGravityVector).normalized * maxSpeed;

            desiredVelocity *= directionSpeedModCurve.Evaluate(Vector3.Angle(transform.forward, desiredVelocity));

            Vector3 velocityChange = desiredVelocity - Vector3.ProjectOnPlane(rb.velocity, -customGravityVector.normalized);
            
            velocityChange *= modifier;

            Vector3 accelerationVector = velocityChange / dt;

            if (accelerationVector.sqrMagnitude > 0f)
            {
                Vector3 localVelocity = rb.transform.InverseTransformVector(rb.velocity);

                localAccelerationVector = rb.transform.InverseTransformVector(accelerationVector);

                float accelerationLimit = (localVelocity.sqrMagnitude < 0.01f || Vector3.Dot(localVelocity, localAccelerationVector) < 0 ) ? maxDeceleration : maxAcceleration;

                localAccelerationVector = Vector3.ClampMagnitude(localAccelerationVector, accelerationLimit);

                accelerationVector = rb.transform.TransformVector(localAccelerationVector);

                rb.AddForce(accelerationVector, ForceMode.Acceleration);
            }

            //Debug.DrawRay (transform.position, desiredVelocity, Color.blue, dt);

            moveDirection = Vector3.zero;
        }

    }
}