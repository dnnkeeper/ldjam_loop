﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

using PawnSystem.Networking;

namespace PawnSystem.Motors
{
    public interface IBasicPawnControls
    {
        Quaternion desiredRotation { get; set; }
        Vector3 desiredVelocity { get; set; }
    }

    [RequireComponent(typeof(Rigidbody))]
    public class AnimatedRigidbodyMotor : MonoBehaviour, IControls, IBasicPawnControls
    {
        public AnimatorStatesHandler animatorStatesHandler;

        public bool updateLocalControls = true;

        [Tooltip("Use some other object rotation as reference or calculate rotation here?")]
        public bool useRotationReference = false;
        public Transform rotationReference;

        public string lookXAxisName = "Mouse X";
        public string lookYAxisName = "Mouse Y";
        public string horizontalAxisName = "Horizontal";
        public string verticalAxisName = "Vertical";

        public string sprintButtonName = "";

        float acceleration;

        public float maxAcceleration = 60f;
        public float accelerationRate = 0.1f;
        public float decelerationRate = 0.1f; 

        public float runSpeed = 5.6f;
        public float walkSpeed = 1.5f;

        public LayerMask groundLayerMask = 1;
        public float skinWidth = 0.01f;

        public bool useRootMotion;
        public bool useRootRotation;

        //public AnimationCurve directionSpeedModCurve = new AnimationCurve(new Keyframe(0f, 100.0f), new Keyframe(90f, 100.0f), new Keyframe(180f, 100.0f));

        protected Quaternion _desiredRotation;
        public Quaternion desiredRotation
        {
            get
            {
                return _desiredRotation;
            }

            set
            {
                _desiredRotation = value;
            }
        }

        protected Vector3 _desiredVelocity;
        public Vector3 desiredVelocity
        {
            get
            {
                return _desiredVelocity;
            }
            set
            {
                _desiredVelocity = value;
            }
        }

        Rigidbody rb;

        new Collider collider;

        Vector3 targetVelocity;

        //Vector3 animatorDeltaPosition;

        Quaternion rotation;

        public Vector3 customGravityValue;
        Vector3 customGravity{
            get {
                customGravityValue = customGravityForce!=null? customGravityForce.force : Physics.gravity; 
                return customGravityValue; 
            }
        }

        ConstantForce customGravityForce;
        void OnCustomGravity(ConstantForce f){

            customGravityForce = f;

            Debug.Log("OnCustomGravity " + customGravity, this);

        }

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();

            collider = GetComponent<Collider>();

            rotation = transform.rotation;
        }

        private void OnEnable()
        {
            if (animatorStatesHandler == null || !animatorStatesHandler.isActiveAndEnabled)
                animatorStatesHandler = GetComponentInChildren<AnimatorStatesHandler>();
            animatorStatesHandler.onAnimatorMove += onRootMotion;

            if (NetworkManager.singleton != null && NetworkManager.singleton.isNetworkActive)
                updateLocalControls = false;
        }

        private void OnDisable()
        {
            animatorStatesHandler.onAnimatorMove -= onRootMotion;
        }

        virtual protected void Update()
        {
            
            //Debug.DrawRay(transform.position, customGravity, Color.red);

            if (updateLocalControls)
            {
                //Local input process
                UpdateControls();

                //Local input application
                EvaluateControls();
            }
        }

        protected virtual void FixedUpdate()
        {
            UpdateForces(Time.fixedDeltaTime);
        }
        
        virtual protected void onRootMotion(Animator animator)
        {
            if (useRootRotation && !isJumping)
            {
                Quaternion dRot = animator.deltaRotation;
                rotation = rb.rotation * Quaternion.Euler(dRot.eulerAngles);
                rb.MoveRotation(rotation);
                //rotation = rb.rotation;
            }

            if (useRootMotion)
            {
                targetVelocity = animator.velocity;

                //animatorDeltaPosition += animator.deltaPosition;
                if (animatorStatesHandler.isClimbing)
                {
                    foreach(Collider c in GetComponentsInChildren<Collider>())
                        c.enabled = false;

                    rb.isKinematic = true;
                    rb.MovePosition(rb.position+animator.deltaPosition);
                }
                else{
                    foreach (Collider c in GetComponentsInChildren<Collider>())
                        c.enabled = true;

                    rb.isKinematic = false;
                }
            }
        }

        //public float angle;
        
        //public Vector3 pseudoVelocity;

        protected virtual void UpdateForces(float dt)
        {
            if (!useRootMotion)
            {
                targetVelocity = desiredVelocity;
            }

            if (!isGrounded || isLanding || isJumping){
                return;
            }

            Vector3 desiredVelocityOnGround = Vector3.ProjectOnPlane(isLanding ? targetVelocity * 0.01f : targetVelocity, -customGravity.normalized);

            Vector3 realVelocityOnGround = Vector3.ProjectOnPlane(rb.velocity, -customGravity.normalized);

            //Debug.DrawRay(transform.position+transform.up*0.01f, desiredVelocityOnGround, Color.cyan);

            //Debug.DrawRay(transform.position + transform.up * 0.02f, targetVelocity, Color.red);

            Vector3 velocityChange = desiredVelocityOnGround - realVelocityOnGround;
            //Vector3 velocityChange = (isLanding ? targetVelocity * 0.01f : targetVelocity) - rb.velocity;

            float angle = Vector3.Angle(desiredVelocityOnGround, realVelocityOnGround);

            if (angle > 90f )
            {
                velocityChange = Vector3.ClampMagnitude(velocityChange, decelerationRate * dt );
            }
            else
            {
                float desiredSpeed = desiredVelocityOnGround.magnitude;

                float realSpeed = realVelocityOnGround.magnitude;

                float adjustedSpeed = realSpeed + ((desiredSpeed > realSpeed) ? accelerationRate * dt : -decelerationRate * dt);
                //Mathf.MoveTowards(realSpeed, desiredSpeed, ((desiredSpeed > realSpeed) ? accelerationRate * dt : decelerationRate * dt));

                // if (desiredSpeed > 0.001f)
                // {
                //     desiredVelocityOnGround = desiredVelocityOnGround.normalized * adjustedSpeed;
                // }
                // else
                // {
                //     desiredVelocityOnGround = realVelocityOnGround.normalized * adjustedSpeed;
                // }

                velocityChange = (desiredVelocityOnGround - realVelocityOnGround) * (1.0f + rb.GetComponent<Collider>().material.dynamicFriction);
            }


            //Debug.DrawRay(transform.position, desiredVelocityOnGround, Color.green);

            //Debug.DrawRay(transform.position, realVelocityOnGround, Color.yellow);

            //Debug.DrawRay(transform.position, velocityChange, Color.magenta);

            var upChange = Vector3.Project(targetVelocity - rb.velocity, customGravity);
            
            if (upChange.sqrMagnitude > 0 && Vector3.Dot(upChange, -customGravity) > 0 ){
                velocityChange += upChange;// - customGravity * 0.05f * dt;
                Debug.DrawRay(transform.position, upChange, Color.magenta, 1f);
            }

            //velocityChange = targetVelocity - rb.velocity;

            // Debug.DrawRay(transform.position, upChange, Color.green, 10f);

            //if (!useRootMotion)
            //{
            //    angle = Vector3.Angle(transform.forward, desiredVelocityOnGround);

            //    desiredVelocityOnGround = desiredVelocityOnGround * (0.01f * directionSpeedModCurve.Evaluate(angle));
            //}

            acceleration = velocityChange.magnitude /  dt;

            rb.AddForce(velocityChange , ForceMode.VelocityChange);
        }
        
        virtual public void UpdateControls()
        {
            if (useRotationReference && rotationReference != null)
            {
                desiredRotation = rotationReference.rotation;
            }
            else
            {

#if CROSS_PLATFORM_INPUT
                float dX = CrossPlatformInputManager.GetAxis(lookXAxisName);

                float dY = CrossPlatformInputManager.GetAxis(lookYAxisName);

#else
                float dX = Input.GetAxis(lookXAxisName);
                float dY = Input.GetAxis(lookYAxisName);
#endif


#if UNITY_STANDALONE || UNITY_EDITOR
                if (Input.GetMouseButton(1))
#endif
                if (!useRotationReference)
                {
                    desiredRotation = Quaternion.Euler(new Vector3(0f, dX, 0f)) * desiredRotation; //global axis rotation

                    desiredRotation = desiredRotation * Quaternion.Euler(new Vector3(-dY, 0f, 0f)); //local axis rotation
                }
            }

            //Debug.DrawRay(transform.position + transform.up, desiredRotation * Vector3.forward, Color.green);

#if CROSS_PLATFORM_INPUT
            float horizontalRaw = CrossPlatformInputManager.GetAxis(horizontalAxisName);

            float vericalRaw = CrossPlatformInputManager.GetAxis(verticalAxisName);

#else
            float horizontalRaw = Input.GetAxisRaw(horizontalAxisName);
            float vericalRaw = Input.GetAxisRaw(verticalAxisName);
#endif


            Vector3 inputDir = new Vector3(horizontalRaw, 0f, vericalRaw);
            if (inputDir == Vector3.zero)
                inputDir = new Vector3(Input.GetAxisRaw(horizontalAxisName), 0f, Input.GetAxisRaw(verticalAxisName));

            //Clamp maximum moveDir magnitude between 0 and 1
            if (new Vector2(inputDir.x, inputDir.z).magnitude > 1f)
            {
                inputDir.Normalize();
            }

            inputDir = desiredRotation * inputDir;
            //Debug.DrawRay(transform.position + transform.up, inputDir, Color.cyan);

            bool bSprint = false;

#if CROSS_PLATFORM_INPUT
            if (!string.IsNullOrEmpty(sprintButtonName) && CrossPlatformInputManager.GetButton("Sprint"))
            {
                bSprint = true;
            }
#else
            if (!string.IsNullOrEmpty(sprintButtonName) && Input.GetButton(sprintButtonName))
            {
                bSprint = true;
            }            
#endif
            if (bSprint)
            {
                desiredVelocity = inputDir * runSpeed;
                
            }
            else
            {
                desiredVelocity = inputDir * walkSpeed;
            }

        }

        bool isGrounded;
        float groundedDelay;

        public float rotationSpeed = 5f;
        Vector3 groundNormal;

        float distanceToGround;
        public void EvaluateControls()
        {
            animatorStatesHandler.moveDir = desiredVelocity;
            animatorStatesHandler.lookRot = desiredRotation;
            animatorStatesHandler.customUp = customGravity.normalized;
            //Debug.DrawRay(transform.position + transform.up * 0.2f, desiredVelocity, Color.red);


            distanceToGround = Mathf.Infinity;
            
            bool groundCheck = RigidbodyMotor.CheckGroundDistance(collider, skinWidth, groundLayerMask, customGravity, out distanceToGround, out groundNormal);
            if (isGrounded)
            {
                if (!groundCheck)
                {
                    
                    if (groundedDelay > 0.08f) //0.32f
                    {
                        isGrounded = groundCheck;
                    }
                    groundedDelay += Time.deltaTime;
                }
                else
                {
                    groundedDelay = 0;
                }
            }
            else
            {
                isGrounded = groundCheck;
                groundedDelay = 0;
            }

            animatorStatesHandler.isGrounded = isGrounded;//RigidbodyMotor.CheckGroundDistance(collider, skinWidth, groundLayerMask, Physics.gravity, out distanceToGround, out groundNormal);
            animatorStatesHandler.height = distanceToGround;
            //float mod = speed;//GetCurrentSpeed();

            animatorStatesHandler.desiredSpeed = desiredVelocity.magnitude;// * mod;

            if (!useRootRotation)
            {
                rotation = desiredRotation;//Quaternion.Lerp(rotation, desiredRotation, Time.deltaTime * 4f);

                //DistanceMatching distanceMatchHandler = animatorStatesHandler.GetComponent<DistanceMatching>();

                //if (distanceMatchHandler != null)
                //{
                //    if (distanceMatchHandler.requireUpdate) {
                //        distanceMatchHandler.requireUpdate = false;
                //        // Distance to stop position with linear deceleration X = X0 + V*V/2*a
                //        distanceMatchHandler.matchPositionStop = transform.position +  rb.velocity.normalized * (rb.velocity.sqrMagnitude / (acceleration * 2f));
                //        Debug.DrawRay(transform.position+transform.up, rb.velocity, Color.magenta);
                //        //Debug.DrawLine(distanceMatchHandler.matchPositionStop, distanceMatchHandler.matchPositionStop + Vector3.up, Color.red);
                //        Debug.Log("acceleration " + acceleration);
                //        //Debug.Break();
                //    }
                //}
            }

            Quaternion targetRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(rotation * Vector3.forward, -customGravity.normalized), -customGravity.normalized);
            //Debug.DrawRay(transform.position + transform.up, targetRotation * Vector3.forward, Color.magenta);
            //rb.MoveRotation( Quaternion.RotateTowards(rb.rotation, targetRotation, (Time.deltaTime/Time.fixedDeltaTime) * Mathf.PI*2f ) );
            rb.MoveRotation(Quaternion.Lerp(rb.rotation, targetRotation, Time.deltaTime * rotationSpeed));
            //transform.rotation = targetRotation;

            //if (useRootMotion)
            //    moveDirection = Vector3.zero;
        }

        bool isLanding;

        void OnEnterLanding()
        {
            Debug.Log("OnLandingEnter");
            isLanding = true;
           
        }

        void OnExitLanding()
        {
            Debug.Log("OnLandingExit");
            isLanding = false;
            
        }

        bool isJumping;
        void OnEnterJumping()
        {
            Debug.Log("OnEnterJumping");
            isJumping = true;

        }

        void OnExitJumping()
        {
            Debug.Log("OnExitJumping");
            isJumping = false;
        }


        public void OnStateSync()
        {
            throw new System.NotImplementedException();
        }
    }
}
