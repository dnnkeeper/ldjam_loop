﻿using UnityEngine;

#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

public class FPSCamera : MonoBehaviour
{
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float pitchLimit = 45f;
    public float yawLimit = 80f;
    
    protected Vector3 independentPosition;
    protected Quaternion independentRotation;
    
    public Transform firstPersonTransform;
    public Transform upReference;
    float horizontalInput, verticalInput;
    

    // Use this for initialization
    void Start()
    {
        // Make the rigid body not change rotation
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb)
            rb.freezeRotation = true;
    }

    private void OnEnable()
    {
        independentPosition = transform.position;
        independentRotation = transform.rotation;
    }

   public void OnCameraTargetChanged(Transform NewTransform, Transform NewReference)
    {
        enabled = true;
        firstPersonTransform = NewTransform;
        upReference = NewReference;
        //Debug.Log("Camera target set to " + NewTransform);
    }

    void LateUpdate()
    {
        UpdateInput();

        transform.position = independentPosition;

        transform.rotation = independentRotation;

        //transform.rotation = Quaternion.Inverse(InputTracking.GetLocalRotation(XRNode.CenterEye));
    }

    void UpdateInput()
    {
        if (firstPersonTransform)
        {
            float dX = 0f;
            float dY = 0f;

            if (firstPersonTransform != null)
            {
#if MOBILE_INPUT && CROSS_PLATFORM_INPUT
                dX = CrossPlatformInputManager.GetAxis("Mouse X") * xSpeed  * 0.02f;
                dY = CrossPlatformInputManager.GetAxis("Mouse Y") * ySpeed * 0.02f;
#else
                dX = Input.GetAxis("Mouse X") * xSpeed * 0.02f;
                dY = Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
#endif
            }

            horizontalInput += dX;
            verticalInput += dY;

            independentRotation = Quaternion.Euler(0, dX, 0) * independentRotation;
            independentRotation = independentRotation * Quaternion.Euler(-dY, 0, 0);

            if (upReference != null)
            {
                independentRotation = limitRotation(upReference.rotation, independentRotation, pitchLimit, yawLimit);
                independentRotation = Quaternion.LookRotation(independentRotation * Vector3.forward, upReference.up);
            }
            else
            {
                independentRotation = limitRotation(firstPersonTransform.rotation, independentRotation, pitchLimit, yawLimit);
            }

            //independentRotation = Quaternion.Euler(Vector3.up * (horizontalInput)) * Quaternion.Euler(transform.right * (-verticalInput)) * firstPersonTransform.rotation;

            independentPosition = firstPersonTransform.position;
        }
    }

    public float angleX, angleY;

    public Quaternion limitRotation(Quaternion from, Quaternion to, float maxAngleX, float maxAngleY)
    {
        Quaternion rotation = to;
        Quaternion fromto = Quaternion.Inverse(from) * to;

        angleX = KeenAngle(fromto.eulerAngles.x);
        angleY = KeenAngle(fromto.eulerAngles.y);
        
        if (Mathf.Abs(angleX) > maxAngleX)
        {
            angleX = Mathf.Sign(angleX) * maxAngleX;
        }

        if (Mathf.Abs(angleY) > maxAngleY)
        {
            angleY = Mathf.Sign(angleY) * maxAngleY;
        }

        rotation = from * Quaternion.Euler(angleX, angleY, 0f);
        /*
        float a = Quaternion.Angle(from, to);
        if (a > maxAngle)
        {
            rotation = Quaternion.RotateTowards(from, to, maxAngle);
        }*/
        return rotation;
    }

    public static float KeenAngle(float angle)
    {
        angle %= 360F;
        if (angle > 180F)
            angle = - (360F - angle);
        return angle;
    }
    
    public Vector3 GetDir(bool Auto)
    {
        return Camera.main.transform.forward;
    }

}
