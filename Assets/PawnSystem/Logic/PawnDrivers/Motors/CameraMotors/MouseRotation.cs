﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseRotation : MonoBehaviour {

    public int mouseButtonId = 0;

    public float sensivity = 0.25f;

    Vector3 lastMousePos;

    void Awake()
    {
        lastMousePos = Input.mousePosition;
    }

    void LateUpdate()
    {
        if (mouseButtonId >= 0 && Input.GetMouseButtonDown(mouseButtonId))
        {
            lastMousePos = Input.mousePosition;
        }
        else
        if (mouseButtonId < 0 || Input.GetMouseButton(mouseButtonId))
        {
            Vector3 mousePos = Input.mousePosition;

            Vector3 mouseDelta = (mousePos - lastMousePos);

            lastMousePos = mousePos;

            //Rotate around global axis UP and local axis RIGHT
            transform.rotation = Quaternion.AngleAxis(mouseDelta.x * sensivity, Vector3.up) * Quaternion.AngleAxis(-mouseDelta.y * sensivity, transform.right) * transform.rotation;
        }
    }
}
