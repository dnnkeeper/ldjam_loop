﻿using UnityEngine;
using System.Collections;

#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class MouseOrbitImproved : MonoBehaviour
{

    public Transform target;
    public float distance = 5.0f;
    public float desiredDistance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;

    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;

    public float distanceMin = 0.5f;
    public float distanceMax = 15f;

    [Range(0.001f, 1.0f)]
    public float smooth = 0.3f;

    public bool fixedUpdate, unparent, keepRotation;

    float x = 0.0f;
    float y = 0.0f;

    protected float timestamp;
#if UNITY_ANDROID
	//Vector3 lastTouch = Vector3.zero;
#endif

    // Use this for initialization
    void Start()
    {
        timestamp = Time.time;

        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        if (target.parent != null)
        {
            Rigidbody rb = target.parent.GetComponent<Rigidbody>();
            if (rb)
            {
                if (rb.interpolation == RigidbodyInterpolation.None)
                {
                    if (!fixedUpdate)
                    {
                        Debug.LogWarning("camera enabled fixed update due to target.rigidbody interpolation");
                        fixedUpdate = true;
                    }
                }
                else if (fixedUpdate)
                {
                    Debug.LogWarning("camera disabled fixed update due to target.rigidbody interpolation");
                    fixedUpdate = false;
                }
            }
        }

        if (unparent)
            transform.parent = null;
    }

    void OnCameraTargetChanged(Transform target)
    {
        enabled = true;
        this.target = target;
        Debug.Log("Camera target set to " + target);
    }

    void Update()
    {
        GetInput();


    }

    void FixedUpdate()
    {

        if (fixedUpdate)
            FollowTarget();
    }

    void LateUpdate()
    {

        if (!fixedUpdate)
            FollowTarget();
    }

    void GetInput()
    {
        if (target != null)
        {

            //Vector3 mousePos = Input.mousePosition;
            //mousePos.x = (mousePos.x / Screen.width) - 0.5f;
            //mousePos.y = (mousePos.y / Screen.height) - 0.5f;
            float dt = 0.02f;

#if MOBILE_INPUT
			if (  Input.touchCount > 0 ) {
				x += CrossPlatformInputManager.GetAxis("Mouse X") * xSpeed * distance * dt;
				y -= CrossPlatformInputManager.GetAxis("Mouse Y") * ySpeed * dt;
			}
#else
            if (Screen.fullScreen || (Input.GetMouseButton(0) && Cursor.visible))
            {
                x += Input.GetAxis("Mouse X") * xSpeed * distance * dt;
                y -= Input.GetAxis("Mouse Y") * ySpeed * dt;
            }
#endif
        }
    }

    void FollowTarget()
    {
        if (target)
        {

            float deltaTime = Time.deltaTime; //Time.time - timestamp;
            timestamp = Time.time;

            if (deltaTime <= 0f)
                return;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            Quaternion rotation = Quaternion.Euler(y, x, 0);
            if (keepRotation)
                //rotation = target.rotation * rotation;
                rotation = Quaternion.FromToRotation(Vector3.up, target.up) * rotation;


            desiredDistance -= Input.GetAxis("Mouse ScrollWheel") * 5;
            desiredDistance = Mathf.Clamp(desiredDistance, distanceMin, distanceMax);
            Ray r = new Ray(target.position, transform.position - target.position);

            float dist = distanceMax;

            LayerMask CollisionLayerMask = LayerMask.GetMask("Default");

            bool collide = false;
            //foreach (RaycastHit hit in Physics.SphereCastAll(target.position, 0.3f, Vector3.Normalize(transform.position-target.position)*(desiredDistance+distanceMin)) )
            foreach (RaycastHit hit in Physics.RaycastAll(r, desiredDistance, CollisionLayerMask))
            {
                if (hit.collider != target.GetComponent<Collider>() && hit.collider.tag != "Player")
                {
                    //Debug.DrawLine(target.position,hit.point,Color.red);
                    if (hit.distance < dist)
                    {
                        dist = hit.distance;
                        collide = true;
                    }
                }
            }

            distance = collide ? Mathf.Clamp(dist - distanceMin, distanceMin, distanceMax) : desiredDistance;
            //distance = desiredDistance;

            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + target.position;

            float _smooth = smooth * deltaTime * 60.0f;

            //smooth =  Mathf.Clamp( Time.deltaTime * smooth, 0.1f, 0.99f); //* target.rigidbody.velocity.sqrMagnitude/10.0f

            transform.rotation =
                //rotation;
                Quaternion.Slerp(transform.rotation, rotation, _smooth);


            transform.position =
                //position;
                Vector3.Lerp(transform.position, position, _smooth);

        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    public Vector3 GetDir(bool Auto)
    {
        return Camera.main.transform.forward;
    }

}
