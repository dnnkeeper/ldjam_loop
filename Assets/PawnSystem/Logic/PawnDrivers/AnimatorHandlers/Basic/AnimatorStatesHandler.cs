﻿#define DRAW_GRAB_RAYS

using UnityEngine;
using System.Collections;

using UPC.Utility;
using System;

[RequireComponent(typeof(Animator))]
public class AnimatorStatesHandler : MonoBehaviour, IStateEventsReceiver
{
    protected Animator animator;

    [Header("Settings:")]

    public bool syncAnimSpeed, alignWithGroundNormal, lookMoveSync;

    const float alignSpeed = 2f;

    public float runningThreshold = 2.5f;

    public LayerMask climbLayerMask;

    [Space]

    #region Input

    [Header("Controls:")]
    
    public Quaternion lookRot;

    public Vector3 moveDir;

    public Vector3 groundNormal;

    public bool isGrounded;

    public Vector3 realVelocity;

    public float realSpeed;

    public float desiredSpeed;

    public float height;

    #endregion

    #region Output

    protected Vector3 lookDirOnGround;
    public Vector3 LookDirOnGround
    {
        get { return lookDirOnGround; }
    }

    protected Vector3 moveDirOnGround;
    public Vector3 MoveDirOnGround
    {
        get { return moveDirOnGround; }
    }
		
    [SerializeField]
    protected Vector2 moveDir2D;
    public Vector2 MoveDir2D
    {
        get { return moveDir2D; }
    }

    [SerializeField]
    protected float lookAngle;

    [Range(0f,1f)]
    public float _stateTime;

	#endregion

	protected int _StateTimeId, _MoveDirXId, _MoveDirYId, _TravelingSpeedId, _IsWalkingId, _IsRunningId, _IsGroundedId, _RealSpeedId, _LookAngleId, _AngularSpeedId, _PlantNTurnId, _RealSpeedXId, _RealSpeedYId, _HeightId = 0;

    protected int _GrabHeightId, _GrabId, _JumpId;
    protected float lastAnimatorMoveTime;

	protected Vector3 lastPos;

    virtual protected void Reset()
    {
        groundNormal = transform.up;
    }

    virtual public void Initialize(Animator animator)
    {
        if (animator == null)
        {
            Debug.LogError("NO ANIMATOR ATTACHED");
            enabled = false;
        }
		
        this.animator = animator;
        _StateTimeId = Animator.StringToHash("StateTime");
        _MoveDirXId = Animator.StringToHash("MoveDirX");
        _MoveDirYId = Animator.StringToHash("MoveDirY");
		_TravelingSpeedId = Animator.StringToHash("TravelingSpeed");
		_IsWalkingId = Animator.StringToHash("isWalking");
		_IsRunningId =  Animator.StringToHash("isRunning");
		_IsGroundedId =  Animator.StringToHash("isGrounded");
		_RealSpeedId =  Animator.StringToHash("RealSpeed");
        _LookAngleId = Animator.StringToHash("LookAngle");


        _AngularSpeedId = Animator.StringToHash("AngularSpeed");
        _PlantNTurnId = Animator.StringToHash("PlantNTurn");
        _RealSpeedXId = Animator.StringToHash("RealSpeedX");
        _RealSpeedYId = Animator.StringToHash("RealSpeedY");
        _HeightId = Animator.StringToHash("Height");

        _GrabId = Animator.StringToHash("Grab");
        _JumpId = Animator.StringToHash("Jump");

        _GrabHeightId = Animator.StringToHash("GrabHeight");
    }
    
    virtual public void OnEnable(){
		lastPos = transform.position;
        Initialize(animator??GetComponent<Animator>());
    }

    virtual public void AlignWithGroundNormal()
    {
        if (groundNormal.sqrMagnitude == 0)
        {
            groundNormal = transform.up;
        }

        if (alignWithGroundNormal)
        {
            Quaternion newRot = Quaternion.LookRotation(transform.forward, groundNormal);
            //Debug.DrawRay(transform.position, newRot * Vector3.up, Color.green);
            //Debug.DrawRay(transform.position, newRot * Vector3.forward, Color.blue);
            //Debug.DrawRay(transform.position, newRot * Vector3.right, Color.red);
            transform.rotation = Quaternion.Lerp(transform.rotation, newRot, 1f);
        }
        //Debug.DrawRay(transform.position, groundNormal, Color.green);
    }

    public float maxClimbHeight = 4.0f;
    public float characterControllerRadius = 0.3f;

    public float grabHeight;

    GameObject _GrabbedObject;

    Vector3 _GrabPosLocal;
    public Vector3 _GrabPos{
        get
        {
            return _GrabbedObject.transform.TransformPoint(_GrabPosLocal);
        }
    }
    public Quaternion _GrabRot = Quaternion.identity;

    bool forward_step_blocked;
    RaycastHit forward_step_hit;
    void ControlJumpNClimb(bool jump, ref Vector3 lookDirOnGround, out bool climbCheck, float deltaTime)
    {
        forward_step_blocked = false;


        bool wantToJump = jump;
        bool blocked = false;
        climbCheck = false;

        float blockRayMagnitude = characterControllerRadius * 4f;
        int raysCount = 5;
        float minRaycastHeight = 0.8f;
        float maxRaycastHeight = 1.86f;
        float heightStep = (maxRaycastHeight - minRaycastHeight)/raysCount;
        Ray blockRay = new Ray();
        RaycastHit blockRayHit = new RaycastHit();
        for (int i = 0; i < raysCount; i++)
        {
            blockRay.origin = transform.position + transform.up * (minRaycastHeight + heightStep * i) - lookDirOnGround * characterControllerRadius;
            blockRay.direction = lookDirOnGround;

            foreach (RaycastHit obstacleHit in Physics.RaycastAll(blockRay, blockRayMagnitude, climbLayerMask))
            {
                if (obstacleHit.collider.tag != "Player")
                {
                    if (Vector3.Dot(-obstacleHit.normal, blockRay.direction) > 0.5f)
                    {
                        blockRayHit = obstacleHit;
                        blocked = true;
                        
                    }
                    break;
                }
            }
            if (blocked)
                break;
            else
                Debug.DrawRay(blockRay.origin, blockRay.direction * blockRayMagnitude, Color.green);
        }
        if (!blocked){
            return;
        }
        
        forward_step_blocked = true;
        forward_step_hit = blockRayHit;

        Debug.DrawLine(blockRay.origin, blockRayHit.point, Color.red);
        Debug.DrawRay(blockRayHit.point, blockRayHit.normal * 0.1f, Color.blue);

        grabHeight = maxClimbHeight;

        //Reduce max climb height while jumping/falling
        var maxGrabHeight = (!isGrounded || isJumping) ? maxClimbHeight / 2f : maxClimbHeight;
        Debug.DrawLine(transform.position, transform.position + transform.up * maxGrabHeight, Color.magenta);

        bool secondBlocked = false;
        RaycastHit secondRayBlockHit = new RaycastHit();
        foreach (RaycastHit secondBlockHit in Physics.RaycastAll(blockRay.origin, -blockRayHit.normal, blockRayMagnitude, climbLayerMask))
        {
            secondBlocked = true;
            secondRayBlockHit = secondBlockHit;
            Debug.DrawLine(blockRay.origin, secondBlockHit.point, Color.white);
            break;
        }
        if (!secondBlocked)
        {
            Debug.DrawRay(blockRay.origin, -blockRayHit.normal * blockRayMagnitude, Color.black);
            return;
        }
        Vector3 pivotPointAtObstacle = transform.position + Vector3.ProjectOnPlane(secondRayBlockHit.point - transform.position, transform.up);
        Vector3 grabTraceStart = pivotPointAtObstacle + Vector3.Cross(transform.up, -blockRayHit.normal) * 0.3f + transform.up * maxGrabHeight - blockRayHit.normal *0.06f;
        RaycastHit grab_hit;
        if (Physics.SphereCast(new Ray(grabTraceStart, -transform.up), 0.1f, out grab_hit, maxGrabHeight-1.0f, climbLayerMask))
        {
            if (Vector3.Angle(grab_hit.normal, transform.up) > 15f)
            {
                Debug.DrawRay(grab_hit.point, grab_hit.normal, Color.red);
                return;
            }

            _GrabbedObject = grab_hit.collider.gameObject;

            _GrabPosLocal = _GrabbedObject.transform.InverseTransformPoint(grab_hit.point + Vector3.up * 0.06f + blockRayHit.normal * 0.1f);
            
            Vector3 v = Vector3.ProjectOnPlane(-secondRayBlockHit.normal, transform.up);
            
            _GrabRot = v != Vector3.zero ? Quaternion.LookRotation(v, transform.up) : transform.rotation;

            Debug.DrawLine(grab_hit.point, grab_hit.point+_GrabRot*Vector3.forward*0.2f, Color.blue);
            Debug.DrawLine(grab_hit.point, grab_hit.point + _GrabRot * Vector3.up * 0.2f, Color.green);
            Debug.DrawLine(grab_hit.point, grab_hit.point + _GrabRot * Vector3.right * 0.2f, Color.red);

            Debug.DrawLine(blockRay.origin, secondRayBlockHit.point, Color.yellow);

            Vector3 upperPoint = grab_hit.point + Vector3.ProjectOnPlane(transform.position - grab_hit.point, transform.up);
            Debug.DrawLine(_GrabPos, upperPoint, Color.magenta);
            Vector3 characterCenterPoint = transform.position + transform.up * 0.5f;
            Vector3 step_up = upperPoint - characterCenterPoint;
            Ray characterJumpRay = new Ray(characterCenterPoint, step_up );

            //Debug.DrawRay(characterJumpRay.origin, characterJumpRay.direction, Color.yellow);

            if (Physics.Raycast(characterJumpRay, out var up_hit, step_up.magnitude, climbLayerMask))
            {
                Debug.DrawLine(characterJumpRay.origin, up_hit.point, Color.red);

                return;
            }

            Debug.DrawRay(characterJumpRay.origin, characterJumpRay.direction*step_up.magnitude, Color.magenta);

            Vector3 step_forward = grabTraceStart - characterJumpRay.origin;

            if (Physics.Raycast(characterJumpRay.origin, step_forward, out var forward_hit, climbLayerMask))
            {
                Debug.DrawLine(characterJumpRay.origin, forward_hit.point, Color.red);

                return;
            }

            Debug.DrawRay(characterJumpRay.origin, step_forward, Color.green);

            Debug.DrawLine(grabTraceStart, _GrabPos, Color.green);
            
            grabHeight = Vector3.Project(grab_hit.point-transform.position, transform.up).magnitude;

            if (wantToJump)
            {
                Debug.Log("Climb height "+grabHeight);
                climbCheck = true;
            }

            wantToJump = false;
        }
        else{
            Debug.DrawRay(grabTraceStart, -transform.up, Color.white);
        }
        

    }
    public Vector3 velocityOnGround;

    public float dampTime = 0.15f;

    public bool canClimb;

    public bool isClimbing;

    virtual public void UpdateAnimParams(float deltaTime)
    {

        float travelingSpeed = moveDir2D.magnitude;// * desiredSpeed;

        moveDir2D = moveDir2D.normalized;

        var currentStateInfo = animator.GetCurrentAnimatorStateInfo(0);

        _stateTime = (currentStateInfo.normalizedTime * 10f) % 10f / 10f;

        bool isInTurn = currentStateInfo.IsTag("Turn");
        bool isInTransitionToTurn = animator.GetNextAnimatorStateInfo(0).IsTag("Turn");
        bool isLanding = currentStateInfo.IsTag("Landing") || animator.GetNextAnimatorStateInfo(0).IsTag("Landing");
        animator.SetBool("isLanding", isLanding);

        if( !isLanding && !animator.GetNextAnimatorStateInfo(0).IsTag("Landing") && isJumping && isJumping != currentStateInfo.IsTag("Jump") && !animator.GetNextAnimatorStateInfo(0).IsTag("Jump") )
        {
            isJumping = false;

            transform.parent.SendMessage("OnExitLanding", SendMessageOptions.DontRequireReceiver);

            Debug.Log("End Jumping");
        }
        bool isInTransition = animator.IsInTransition(0);

        animator.SetFloat(_StateTimeId, _stateTime);
        animator.SetFloat(_MoveDirXId, moveDir2D.x, dampTime, deltaTime);
        animator.SetFloat(_MoveDirYId, moveDir2D.y, dampTime, deltaTime);

        if (forward_step_blocked)
        {
            if (Vector3.Angle(lookDirOnGround, -forward_step_hit.normal) < 30f)
            {
                travelingSpeed = 0f;
            }
        }

        animator.SetFloat(_TravelingSpeedId, travelingSpeed, 0.32f, deltaTime);
        animator.SetBool(_IsWalkingId, animator.GetFloat(_TravelingSpeedId) > 0.1f);
        animator.SetBool(_IsRunningId, travelingSpeed > runningThreshold);
        
        animator.SetFloat(_RealSpeedId, realSpeed, isLanding?1f : 0.05f, deltaTime);

        if (isClimbing != currentStateInfo.IsTag("Climb")){
            isClimbing = !isClimbing;
            SendMessage("OnClimbing", isClimbing, SendMessageOptions.DontRequireReceiver);
        }
        
        float oldLookAngle = animator.GetFloat(_LookAngleId);

        bool jumpButton = Input.GetKey(KeyCode.Space);

        if (jumpButton && !forward_step_blocked)
        {
            animator.SetBool(_JumpId, true);

            //ControlJumpNClimb(true, ref lookDirOnGround, out bool bGrab, deltaTime);

            // var rb = GetComponentInParent<Rigidbody>();
            // var jumpForce = (rb.velocity.normalized - customGravity.normalized) * jumpSpeed;
            // rb.AddForce(jumpForce, ForceMode.VelocityChange);

        }
        else
        {
            animator.SetBool(_JumpId, false);
        }

        if (isClimbing)
        {
            //Debug.Log("isClimb");
            //Rotate character to match grab rotation
            transform.rotation = Quaternion.Lerp(transform.rotation, _GrabRot, 3.0f * deltaTime);

            if (isInTransition)
            {
                //collisionCenter = defaultCollisionCenter;
            }
            else
            {
                canClimb = true;

                if (_stateTime > animator.GetFloat("MatchStart"))
                {
                    //collisionCenter = characterController.transform.InverseTransformDirection(transform.up * 1.2f - characterForward * 0.33f);
                    //collisionHeight = 1.2f;
                }
                //Debug.Log("MatchTarget");

                Vector3 wmask = new Vector3(1, 1, 1);
                animator.MatchTarget(_GrabPos, Quaternion.identity, AvatarTarget.RightHand, new MatchTargetWeightMask(wmask, 0), animator.GetFloat("MatchStart"), animator.GetFloat("MatchEnd"));//0.181f, 0.297f);
                //Debug.DrawRay(_GrabPos, -customUp, Color.green, deltaTime);
            }
        }
        else
        {
            if (!isGrounded)
            {
                //applyRootMotion = false;
            }

            // if (inRoll)
            // {
            //     collisionCenter = Vector3.up * 0.6f;
            //     collisionHeight = 1.2f;
            // }
            // else
            // {
            //     collisionCenter = defaultCollisionCenter;
            //     collisionHeight = characterHeight;
            // }

            ControlJumpNClimb(jumpButton, ref lookDirOnGround, out canClimb, deltaTime);
        }


        //Special cases for turning
        if (isInTurn || isInTransitionToTurn)
        {
            float desiredTurnAngle = Vector3.SignedAngle(preturnForward, lookDirOnGround, customUp); // preturnForward.AngleSignedWith(lookDirOnGround, customUp) * Mathf.Rad2Deg;

            //ignore transform.forward rotation while turning and keep pre-turn forward vector as reference for lookAngle parameter
            lookAngle = desiredTurnAngle;
            //desiredTurnAngle;

            //if desired rotation went over 180 degrees and changed its sign
            if (Mathf.Sign(desiredTurnAngle) != Mathf.Sign(plannedTurnAngle) && Mathf.Abs(desiredTurnAngle) > 10f)
            {
                //increase angle over 180 degrees
                lookAngle = plannedTurnAngle + (180f - Mathf.Abs(desiredTurnAngle) + 180f - Mathf.Abs(plannedTurnAngle)) * Mathf.Sign(plannedTurnAngle);
            }

            //lookAngle = Mathf.Lerp(oldLookAngle, lookAngle, deltaTime * 4f);

            //restrict angle decreasing while playing turn animation
            if ( Mathf.Abs(lookAngle) < Mathf.Abs(oldLookAngle) )
            {
                lookAngle = oldLookAngle;//Mathf.Lerp(oldLookAngle, lookAngle, deltaTime);
            }
            
            //DrawAngle(transform.position + transform.up * 0.1f, preturnForward, groundNormal, lookAngle, deltaTime, Color.cyan, Color.magenta);
        }
        else
        {
            //DrawAngle(transform.position + transform.up * 0.1f, transform.forward, groundNormal, lookAngle, deltaTime, Color.green, Color.red);
        }

        //Debug.DrawRay(transform.position + transform.up * 0.1f, transform.forward * 2.0f, Color.yellow, deltaTime);

        animator.SetFloat(_LookAngleId, lookAngle);

        Vector3 newVelocityOnGround = Vector3.ProjectOnPlane(realVelocity, groundNormal);

        

        float angularSpeed = isInTurn ? 0f : Mathf.Clamp( lookAngle / 0.33f, -180.0f, 180.0f); //isInTurn? 0f : 

        //angularSpeed = velocityOnGround.AngleSignedWith(newVelocityOnGround, transform.up)/deltaTime * Mathf.Rad2Deg / 4f;

        velocityOnGround = newVelocityOnGround;
        
        animator.SetFloat(_AngularSpeedId, angularSpeed, 0.33f, deltaTime); // 

        if (isGrounded != animator.GetBool(_IsGroundedId))
        {
            animator.SetBool(_IsGroundedId, isGrounded);
            gameObject.SendMessage("OnGrounded", isGrounded, SendMessageOptions.DontRequireReceiver);
        }

        float velocityAngle = Vector3.SignedAngle(transform.forward, velocityOnGround, groundNormal) * Mathf.Deg2Rad; //transform.forward.AngleSignedWith(velocityOnGround, groundNormal);

        //Debug.DrawRay(transform.position + transform.up * 0.1f, velocityOnGround, Color.yellow);

        Vector2 realVelocity2D = new Vector2(Mathf.Sin(velocityAngle), Mathf.Cos(velocityAngle)) * velocityOnGround.magnitude;

        animator.SetFloat(_RealSpeedXId, realVelocity2D.x, 0.05f, deltaTime);

        animator.SetFloat(_RealSpeedYId, realVelocity2D.y, 0.05f, deltaTime);

        animator.SetFloat(_HeightId, height, 0.05f, deltaTime);

        animator.SetFloat(_GrabHeightId, grabHeight);
        
        animator.SetBool(_GrabId, canClimb);

        //if (inturn)
        //animator.SetBool(_PlantNTurnId, PlantNTurn);
    }

    public float rotationSpeed = 0f;

    public Vector3 customUp;

    public float jumpSpeed = 5f;
    bool isJumping;
    void OnJump(){
        isJumping = true;
        transform.parent.SendMessage("OnEnterLanding", SendMessageOptions.DontRequireReceiver);
        
        var jumpForce = -customUp * jumpSpeed;

        Debug.DrawRay(rb.position, rb.velocity, Color.red, 10f);

        rb.AddForce(jumpForce, ForceMode.VelocityChange);
        
        Debug.Log("Jump speed "+rb.velocity.magnitude);

    }
    Rigidbody rb;
    new Collider collider;
    private void Awake() {
        rb = GetComponentInParent<Rigidbody>();
        collider = rb.GetComponent<Collider>();
    }

    virtual protected void Update(){
        float deltaTime = Time.deltaTime;///Time.timeScale;

        groundNormal = transform.up; //-customGravity.normalized;

        AlignWithGroundNormal();

        //if (velocityOnGround.sqrMagnitude > 0.1f)
        //    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(velocityOnGround, transform.up), rotationSpeed * deltaTime);
        //else
        //    transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, rotationSpeed * deltaTime);

        UpdateState(deltaTime);

        UpdateAnimParams(deltaTime);
    }
    
    virtual public void SetLookRot(Quaternion val)
    {
        lookRot = val;
    }

    virtual public void SetMoveDir(Vector3 val)
    {
        moveDir = val;
    }

    virtual public void UpdateState(float deltaTime)
    {

        moveDirOnGround = Vector3.ProjectOnPlane(moveDir, -customUp).normalized;

        if (lookMoveSync)
            if (moveDir.sqrMagnitude > 0)
                lookDirOnGround = moveDirOnGround;
            else
                lookDirOnGround = transform.forward;
        else
            lookDirOnGround = Vector3.ProjectOnPlane(lookRot * Vector3.forward, groundNormal).normalized;

        // Debug.DrawRay(transform.position + transform.up * 0.15f, moveDirOnGround, Color.yellow);

        // Debug.DrawRay(transform.position + transform.up * 1f, lookRot * Vector3.forward, Color.blue);

        Debug.DrawRay(transform.position + transform.up * 0.1f, transform.forward, Color.cyan);

        Debug.DrawRay(transform.position + transform.up * 0.1f, lookDirOnGround, Color.blue);

        var myForwardOnGground = Vector3.ProjectOnPlane(transform.forward, -customUp);

        lookAngle = Vector3.SignedAngle(myForwardOnGground, lookDirOnGround, -customUp);// myForwardOnGground.AngleSignedWith (lookDirOnGround, -customUp) * Mathf.Rad2Deg;

        //float moveAngle = transform.forward.AngleSignedWith(moveDirOnGround, groundNormal);
        float moveAngle = Vector3.SignedAngle(lookDirOnGround, moveDirOnGround, -customUp) * Mathf.Deg2Rad; // lookDirOnGround.AngleSignedWith(moveDirOnGround, -customUp);
        
        //DrawAngle(transform.position, transform.forward, -customGravity.normalized, lookAngle, deltaTime, Color.red, Color.green, 1f);
        
        moveDir2D = new Vector2(Mathf.Sin(moveAngle), Mathf.Cos(moveAngle)) * desiredSpeed;
        
        realVelocity = (transform.position - lastPos) / deltaTime;

        realSpeed = realVelocity.magnitude;

        /*
        if (realSpeed > 0.01f)
        {
            Debug.DrawLine(lastPos, transform.position, Color.black, 1f);
            Debug.DrawRay(transform.position+transform.up*0.1f, transform.forward * 1.1f, Color.blue);
        }*/

        lastPos = transform.position;
    }

    void DrawAngle(Vector3 pos, Vector3 fwd, Vector3 up, float angle, float dt, Color c1, Color c2, float length = 1.0f)
    {
        float sign = Mathf.Sign(angle);
        float absAngle = Mathf.Abs(angle);
        for (int i = 0; i < absAngle; i+=5)
        {
            Debug.DrawLine(pos, pos + Quaternion.AngleAxis(sign * i, up) * fwd * length, Color.Lerp(c1, c2, i / absAngle), dt);
        }
        Debug.DrawLine(pos, pos + Quaternion.AngleAxis(sign * absAngle, up) * fwd * length * 1.5f, c2, dt);
        //Debug.DrawLine (pos, pos + Quaternion.AngleAxis(sign*180, up) * fwd * 2.0f, c2, dt);
        //Debug.DrawLine (pos, pos + Quaternion.AngleAxis(sign*90, up) * fwd * 2.0f, Color.Lerp(c1, c2, 0.5f), dt);
        Debug.DrawLine(pos, pos + Quaternion.AngleAxis(0, up) * fwd * length * 1.5f, c1, dt);
    }

    public event Action<Animator> onAnimatorMove;
    
    protected virtual void OnAnimatorMove()
	{
		//Quaternion dRot = animator.deltaRotation;

		Vector3 dPos = animator.deltaPosition;
        
		//transform.rotation *= dRot;

		float deltaTime = Time.time - lastAnimatorMoveTime;

		lastAnimatorMoveTime = Time.time;

        if (syncAnimSpeed && animator.GetCurrentAnimatorStateInfo(0).IsTag("WalkRun") )
        {
            Vector3 animatorVelocity = dPos / deltaTime;

            animator.speed = 1f * Mathf.Clamp(desiredSpeed / animatorVelocity.magnitude, 0.5f, 3f);
        }
        else
        {
            animator.speed = 1f;
        }

        if (onAnimatorMove != null)
            onAnimatorMove.Invoke(animator);
    }


    void LandingEvent()
    {

    }

    void OnEnterLanding()
    {
        //Debug.Log("OnLandingEnter");
        //isLanding = true;
        transform.parent.SendMessage("OnEnterLanding", SendMessageOptions.DontRequireReceiver);
    }

    void OnExitLanding()
    {
        //Debug.Log("OnLandingExit");
        //isLanding = false;
        transform.parent.SendMessage("OnExitLanding", SendMessageOptions.DontRequireReceiver);
    }

    void Footstep()
	{

	}
    
    public Vector3 preturnForward;

    float plannedTurnAngle;

    void OnEnterTurn()
    {
        //AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        //Debug.Log("On Enter Turn " + stateInfo.IsTag("Turn") + " " + ((stateInfo.normalizedTime * 10f) % 10f / 10f) );
        preturnForward = transform.forward;

        plannedTurnAngle = Vector3.SignedAngle(preturnForward, lookDirOnGround, customUp); //preturnForward.AngleSignedWith(lookDirOnGround, customUp) * Mathf.Rad2Deg;
    }
    
    void OnExitTurn()
    {
        //AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        //Debug.Log("On EXIT Turn " + stateInfo.IsTag("Turn") + " " + ((stateInfo.normalizedTime * 10f) % 10f / 10f));
        
    }

    public virtual void OnBehaviourEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = ""){
    }

    public virtual void OnBehaviourUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = ""){
    }

    public virtual void OnBehaviourExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = ""){
    }

    public virtual void OnBehaviourMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = ""){
    }

    public virtual void OnBehaviourIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = ""){
    }
}