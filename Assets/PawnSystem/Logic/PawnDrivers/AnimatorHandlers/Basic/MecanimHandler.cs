﻿using System.Collections;
using System.Collections.Generic;
using PawnSystem.Motors;
using UnityEngine;
using UPC.Utility;

[RequireComponent(typeof(Animator))]
public class MecanimHandler : MonoBehaviour
{
    public float rotationLerpSpeed = 1.0f;

    public Vector3 LocalGroundVelocityVector
    {
        get { return localGroundVelocityVector; }
    }

    public Vector3 LocalGroundAccelerationVector
    {
        get { return localGroundAccelerationVector; }
    }

    public Vector3 RealVelocityVector
    {
        get { return realVelocityVector; }
    }

    public Vector3 GroundVelocityVector
    {
        get { return groundVelocityVector; }
    }

    public Vector3 GroundAccelerationVector
    {
        get { return groundAccelerationVector; }
    }

    [SerializeField]
    Vector3 localGroundVelocityVector, localGroundAccelerationVector;

    Vector3 realVelocityVector, groundVelocityVector, groundAccelerationVector, forwardVector;

    public float heightAboveGround;

    public float GroundSpeed
    {
        get { return groundSpeed; }
    }

    public float GroundAcceleration
    {
        get { return groundAcceleration; }
    }

    public float AngularSpeed
    {
        get { return angularSpeed; }
    }

    public float LookAngle
    {
        get { return lookAngle; }
    }

    [SerializeField]
    float groundSpeed, groundAcceleration, angularSpeed, lookAngle, moveAngle;

    Vector3 lastPosition;

    Animator animator;

    [Range(0f, 1f)]
    public float baseLayerStateTime;

    protected int _AccelerationId, _StateTimeId, _MoveAngleId, _AccelerationXId, _AccelerationYId, _GroundSpeedId, _IsWalkingId, _IsRunningId, _IsGroundedId, _RealSpeedId, _LookAngleId, _AngularSpeedId, _PlantNTurnId, _RealSpeedXId, _RealSpeedZId, _HeightId = 0;

    virtual public void Initialize()
    {
        _StateTimeId = Animator.StringToHash("StateTime");

        _GroundSpeedId = Animator.StringToHash("TravelingSpeed");

        _IsRunningId = Animator.StringToHash("isRunning");

        _IsGroundedId = Animator.StringToHash("isGrounded");

        _RealSpeedId = Animator.StringToHash("RealSpeed");

        _RealSpeedXId = Animator.StringToHash("RealSpeedX");

        _RealSpeedZId = Animator.StringToHash("RealSpeedZ");

        _AccelerationXId = Animator.StringToHash("AccelerationX");

        _AccelerationYId = Animator.StringToHash("AccelerationY");

        _LookAngleId = Animator.StringToHash("LookAngle");

        _MoveAngleId = Animator.StringToHash("MoveAngle");

        _AngularSpeedId = Animator.StringToHash("AngularSpeed");

        _AccelerationId = Animator.StringToHash("Acceleration");

        _HeightId = Animator.StringToHash("Height");
    }

    virtual public void OnEnable()
    {
        lastPosition = transform.position;

        animator = animator ?? GetComponent<Animator>();

        Initialize();
    }

    // Start is called before the first frame update
    void Start()
    {
        forwardVector = transform.forward;

        orientation = transform.rotation;
    }

    float timer;

    private void Update()
    {
        transform.rotation = orientation;
    }

    void FixedUpdate()
    {
        float dt = Time.fixedDeltaTime;

        timer += dt;

        //if (timer >= 1f/ updateRate)
        //{
            UpdateData(timer);

            UpdateAnimParams(timer);

            timer = 0f;
        //}
    }

    Vector3 matchPositionStart, matchPositionStop;

    [SerializeField]
    bool isAccelerating;

    [SerializeField]
    float groundAccelerationSmooth;

    Quaternion orientation;

    void OnDistanceMatchPredict()
    {
        //groundAccelerationVector = transform.rotation * localGroundAccelerationVector;
        //float groundAcceleration = Mathf.Sign(Vector3.Dot(groundAccelerationVector, groundVelocityVector)) * groundAccelerationVector.magnitude;

        float groundAcceleration = GetComponentInParent<RigidbodyCharacterMotor>().acceleration;  //

        if (groundAcceleration > 0)
            Debug.Log(" Predict START positions OnDistanceMatchEntered acceleration:"+ groundAcceleration);
        else
            Debug.Log(" Predict STOP positions OnDistanceMatchEntered acceleration:" + groundAcceleration);

        matchPositionStart = transform.position;

        var stopVector = groundVelocityVector.normalized * (groundSpeed * groundSpeed / (groundAcceleration * 2f));

        matchPositionStop = transform.position - stopVector;

        Debug.DrawRay(transform.position, stopVector, Color.magenta, 2.0f);

        var distanceMatching = GetComponent<DistanceMatching>();

        if (distanceMatching)
        {
            distanceMatching.matchPositionStart = matchPositionStart;
            distanceMatching.matchPositionStop = matchPositionStop;
        }

    }

    void UpdateData(float dt)
    {
        //Vector3 lookDir = transform.parent != null ? transform.parent.forward : transform.forward;

        Quaternion parentOrientation = transform.parent != null ? transform.parent.rotation : transform.rotation;

        lookAngle = Quaternion.Angle(orientation, parentOrientation); //Mathf.Rad2Deg * transform.forward.AngleSignedWith(lookDir, transform.up);

        angularSpeed = Mathf.Clamp(forwardVector.AngleSignedWith(parentOrientation * Vector3.forward, transform.up) * Mathf.Rad2Deg / dt, - 45f, 45f);

        forwardVector = parentOrientation * Vector3.forward;

        

        realVelocityVector = (transform.position - lastPosition) / dt;

        Vector3 newGroundVelocity = Vector3.ProjectOnPlane(realVelocityVector, transform.up);

        groundAccelerationVector = (newGroundVelocity - groundVelocityVector) / dt;

        localGroundAccelerationVector = Quaternion.Inverse(transform.rotation) * groundAccelerationVector;

        groundVelocityVector = newGroundVelocity;

        localGroundVelocityVector = Quaternion.Inverse(transform.rotation) * groundVelocityVector;

        groundSpeed = groundVelocityVector.magnitude;

        moveAngle = Vector3.SignedAngle(parentOrientation * Vector3.forward, groundVelocityVector, transform.up);

        var lookToVelocityAngle = Mathf.Abs(moveAngle);

        bool backward = false;

        if (lookToVelocityAngle > 90)
        {
            backward = true;

            lookToVelocityAngle = 180f - lookToVelocityAngle;
        }

        if (lookToVelocityAngle < 60f && groundSpeed > 0.1f)
        {
            orientation = Quaternion.Lerp(orientation, Quaternion.LookRotation(backward ? -groundVelocityVector : groundVelocityVector, transform.up), rotationLerpSpeed * dt);
        }
        else
        {
            orientation = Quaternion.Lerp(orientation, parentOrientation, rotationLerpSpeed * dt);
        }


        float newGroundAcceleration = Mathf.Sign( Vector3.Dot(groundAccelerationVector, groundVelocityVector) ) * groundAccelerationVector.magnitude;

        groundAccelerationSmooth = Mathf.Lerp(groundAccelerationSmooth, newGroundAcceleration, 2f * dt);

        if (Mathf.Abs(localGroundAccelerationVector.x) < 0.1f)
        {
            if (isAccelerating && groundAccelerationSmooth < -0.01f && newGroundAcceleration < 0f)
            {
                isAccelerating = false;

                //Debug.Log(groundAcceleration + " was accelerating, now stopping " + newGroundAcceleration);

                //matchPositionStart = transform.position;

                //matchPositionStop = transform.position - groundVelocityVector.normalized * (groundSpeed * groundSpeed / (newGroundAcceleration * 2f));

                //var distanceMatching = GetComponent<DistanceMatching>();
                
                //if (distanceMatching)
                //{
                //    distanceMatching.matchPositionStart = matchPositionStart;
                //    distanceMatching.matchPositionStop = matchPositionStop;
                //}

                groundAcceleration = newGroundAcceleration;

            }
            else if (!isAccelerating && groundAccelerationSmooth > 1.0f && newGroundAcceleration > 0f)
            {
                isAccelerating = true;

                groundAcceleration = newGroundAcceleration;

                //matchPositionStart = transform.position;

                //var distanceMatching = GetComponent<DistanceMatching>();

                //if (distanceMatching)
                //{
                //    distanceMatching.matchPositionStart = matchPositionStart;
                //}
            }
        }

        Debug.DrawRay(transform.position + transform.up * 0.1f, parentOrientation * Vector3.forward, Color.green);
        Debug.DrawRay(transform.position + transform.up * 0.1f, forwardVector, Color.red);

        lastPosition = transform.position;
    }

    virtual public void UpdateAnimParams(float deltaTime)
    {
        var baseLayerStateInfo = animator.GetCurrentAnimatorStateInfo(0);

        baseLayerStateTime = (baseLayerStateInfo.normalizedTime * 10f) % 10f / 10f;

        animator.SetFloat(_StateTimeId, baseLayerStateTime);
        
        animator.SetFloat(_GroundSpeedId, groundSpeed);

        animator.SetBool(_IsRunningId, groundSpeed > 3f);

        animator.SetFloat(_RealSpeedId, groundSpeed);
        
        animator.SetFloat(_LookAngleId, lookAngle);

        animator.SetFloat(_MoveAngleId, moveAngle);

        //animator.SetFloat(_AngularSpeedId, angularSpeed, 0.33f, deltaTime);

        animator.SetBool(_IsGroundedId, heightAboveGround > 0.01f);

        animator.SetFloat(_RealSpeedXId, localGroundVelocityVector.x);
        
        animator.SetFloat(_RealSpeedZId, localGroundVelocityVector.z);

        animator.SetFloat(_AccelerationXId, LocalGroundAccelerationVector.x, 0.3f, deltaTime);

        animator.SetFloat(_AccelerationYId, LocalGroundAccelerationVector.z, 0.3f, deltaTime);

        animator.SetFloat(_HeightId, heightAboveGround);

        animator.SetFloat(_AccelerationId, groundAcceleration);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(matchPositionStart+Vector3.up, 0.25f);

        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(matchPositionStop + Vector3.up, 0.25f);

        Gizmos.DrawLine(matchPositionStart + Vector3.up, matchPositionStop + Vector3.up);
    }
}
