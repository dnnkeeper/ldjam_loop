﻿using UnityEngine;
using System.Collections;

public interface IStateEventsReceiver
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    void OnBehaviourEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "");

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    void OnBehaviourUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "");

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    void OnBehaviourExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "");

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    void OnBehaviourMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "");

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    void OnBehaviourIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "");
}

public class StateBroadcaster : StateMachineBehaviour
{

    public string behaviourName;

    protected IStateEventsReceiver receiver;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (receiver == null)
            receiver = animator.gameObject.GetComponent<IStateEventsReceiver>();

        string message = "OnEnter" + behaviourName;
        if (receiver != null)
        {
            receiver.OnBehaviourEnter(animator, stateInfo, layerIndex, behaviourName);
            receiver.OnBehaviourEnter(animator, stateInfo, layerIndex, message);
        }
        //animator.SendMessage(behaviourName, SendMessageOptions.DontRequireReceiver);
        //Debug.Log("SendMessage "+ behaviourName);
        animator.SendMessage(message, SendMessageOptions.DontRequireReceiver);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (receiver != null)
        {
            receiver.OnBehaviourUpdate(animator, stateInfo, layerIndex, behaviourName);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        string message = "OnExit" + behaviourName;
        if (receiver != null)
        {
            receiver.OnBehaviourExit(animator, stateInfo, layerIndex, message);
        }
        animator.SendMessage(message, SendMessageOptions.DontRequireReceiver);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (receiver != null)
        {
            receiver.OnBehaviourMove(animator, stateInfo, layerIndex, behaviourName);
        }
    }

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (receiver != null)
        {
            receiver.OnBehaviourIK(animator, stateInfo, layerIndex, behaviourName);
        }
    }
}
