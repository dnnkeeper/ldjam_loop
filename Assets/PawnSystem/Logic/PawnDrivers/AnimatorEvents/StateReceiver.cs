﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct UnityEventExposed
{
    public string name;
    public UnityEvent action;
}

public class StateReceiver : MonoBehaviour, IStateEventsReceiver
{
    public UnityEventExposed[] customEvents;

    public void invokeCustomEventN(int n)
    {
        if (n >= 0 && customEvents.Length > n)
        {
            var customEvent = customEvents[n];

            if (customEvent.action != null)
            {
                Debug.Log(gameObject.name+" Invoke custom event " + customEvent.name, gameObject);

                customEvent.action.Invoke();
            }
        }
    }

    public void invokeCustomEvent(string sName)
    {
        foreach (var customEvent in customEvents)
        {
            if (customEvent.name.Equals(sName))
            {
                Debug.Log(gameObject.name+ " Invoke custom event " + customEvent.name, gameObject);

                customEvent.action.Invoke();

                break;
            }
        }
    }


    public void OnBehaviourEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "")
    {
        invokeCustomEvent(behaviourName);
        invokeCustomEvent(behaviourName+"Enter");
    }

    public void OnBehaviourExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "")
    {
        invokeCustomEvent(behaviourName+"Exit");
        //throw new System.NotImplementedException();
    }

    public void OnBehaviourIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "")
    {
        //throw new System.NotImplementedException();
    }

    public void OnBehaviourMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "")
    {
        //throw new System.NotImplementedException();
    }

    public void OnBehaviourUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, string behaviourName = "")
    {
        //throw new System.NotImplementedException();
    }
}
