﻿using UnityEngine;
using UnityEngine.Events;

namespace UPC.StateEvents
{
    [System.Serializable]
    public struct AnimationStateReflection
    {

        public string name;

        public UnityEvent onStateEnter, onStateExit, onStateTransition;

        bool _active;
        public bool active
        {
            get { return _active; }
            set
            {
                if (_active != value)
                {
                    _active = value;
                    if (_active)
                        onStateEnter.Invoke();
                    else
                        onStateExit.Invoke();
                }
            }
        }

        bool _transiting;
        public bool transiting
        {
            get { return transiting; }
            set
            {
                if (value && !_transiting)
                {
                    //Debug.Log ("onStateTransition "+name);
                    onStateTransition.Invoke();
                }
                _transiting = value;
            }
        }
    }

    [System.Serializable]
    public struct AnimationStateGroup
    {

        public string tag;

        public UnityEvent onGroupEnter, onGroupExit, onGroupTransition;

        bool _active;
        public bool active
        {
            get { return _active; }
            set
            {
                if (_active != value)
                {
                    _active = value;
                    if (_active)
                        onGroupEnter.Invoke();
                    else
                        onGroupExit.Invoke();
                }
            }
        }

        bool _transiting;
        public bool transiting
        {
            get { return transiting; }
            set
            {
                if (value && !_transiting)
                {
                    //Debug.Log ("onGroupTransition "+tag);
                    onGroupTransition.Invoke();
                }
                _transiting = value;
            }
        }
    }

    /// <summary>
    /// This component allows you to manage callbacks in mecanim states via editor UI and UnityEvents
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class MecanimStateEvents : MonoBehaviour
    {

        protected Animator animator;

        public int animationLayers;

        [SerializeField] public AnimationStateReflection[] states;
        [SerializeField] public AnimationStateGroup[] groups;

        // Use this for initialization
        void Start()
        {
            animator = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {

            for (int layer = 0; layer <= animationLayers; layer++)
            {

                bool inTransition = animator.IsInTransition(layer);
                //AnimatorTransitionInfo transitionInfo = animator.GetAnimatorTransitionInfo (layer);
                AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(layer);
                AnimatorStateInfo nextState = animator.GetNextAnimatorStateInfo(layer);

                int statesCount = states.Length;
                for (int i = 0; i < statesCount; i++)
                {
                    AnimationStateReflection state = states[i];
                    state.active = stateInfo.IsName(state.name);
                    state.transiting = inTransition && nextState.IsName(state.name);
                }

                int groupsCount = groups.Length;
                for (int j = 0; j < groupsCount; j++)
                {
                    AnimationStateGroup group = groups[j];
                    group.active = stateInfo.IsTag(group.tag);
                    group.transiting = inTransition && nextState.IsTag(group.tag);
                }
            }
        }
    }
}