﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using System.Linq;
using System.IO;

public class AnimationDistanceCurveGenerator : AssetPostprocessor
{
    public void OnPostprocessModel(GameObject root)
    {
        Object[] objects = AssetDatabase.LoadAllAssetsAtPath(assetPath);

        foreach (Object obj in objects)
        {
            var importer = assetImporter as ModelImporter;


            AnimationClip clip = obj as AnimationClip;

            if (clip != null && (clip.hideFlags != (HideFlags.HideInHierarchy | HideFlags.NotEditable)))
            {
                int nameIndex = assetPath.LastIndexOf("/");

                string baseAssetPath = assetPath.Substring(0, nameIndex);

                string baseAssetName = assetPath.Split('/').Last<string>(); //assetPath.Substring(nameIndex, assetPath.Length);

                //Debug.Log("assetPath: " + assetPath);

                //Debug.Log("baseAssetPath: " + baseAssetPath);

                //Debug.Log("baseAssetName: " + baseAssetName);

                //Debug.Log(clip.hideFlags);

                var rootCurve = AnimationUtility.GetEditorCurve(clip, EditorCurveBinding.FloatCurve("", typeof(Animator), "RootT.z"));

                if (rootCurve == null)
                {
                    //Debug.Log("no root curve found");
                }
                else
                {
                    string newPath = baseAssetPath + "/" + baseAssetName + "_DistanceCurves" + "/" + clip.name + "_DistanceCurve.asset";

                    Directory.CreateDirectory(baseAssetPath + "/" + baseAssetName + "_DistanceCurves");

                    var curveObject = AssetDatabase.LoadAssetAtPath<AnimationCurveObject>(newPath);

                    if (curveObject == null)
                    {
                        curveObject = ScriptableObject.CreateInstance<AnimationCurveObject>();

                        AssetDatabase.CreateAsset(curveObject, newPath);
                    }

                    var distanceCurve = new AnimationCurve();

                    float startingValue = rootCurve[0].value;

                    for (int i = 0; i < rootCurve.length; i++)
                    {
                        distanceCurve.AddKey(rootCurve[i].time, Mathf.Abs(rootCurve[i].value - startingValue));
                    }

                    curveObject.distanceCurve = distanceCurve;

                    EditorUtility.SetDirty(curveObject);

                    //clip.hideFlags = HideFlags.None;

                    AnimationUtility.SetEditorCurve(clip, EditorCurveBinding.FloatCurve("", typeof(Animator), "DistanceCurve"), distanceCurve);

                    //EditorUtility.SetDirty(clip);

                    AssetDatabase.WriteImportSettingsIfDirty(importer.assetPath);

                    AssetDatabase.SaveAssets();

                    //importer.SaveAndReimport();
                }
            }
        }
    }
    /*
    public void OnPreprocessAnimation()
    {
        var importer = assetImporter as ModelImporter;

        Object[] objects = AssetDatabase.LoadAllAssetsAtPath(assetPath);

        Dictionary<string, AnimationClip> animationClips = new Dictionary<string, AnimationClip>();

        foreach (Object obj in objects)
        {
            AnimationClip clip = obj as AnimationClip;
            if (clip != null)
            {
                //Debug.Log("Added "+ clip.name);
                animationClips.Add(clip.name, clip);
            }
        }

        var importerClipAnimations = importer.clipAnimations;

        for (int i = 0; i < importerClipAnimations.Length; i++) {

            var clipAnimation = importerClipAnimations[i];

            AnimationClip clip;
            if (animationClips.TryGetValue(clipAnimation.name, out clip))
            {
                //Debug.Log("animationClips contains "+ clipAnimation.name);
                AnimationCurve distanceCurve;

                CreateDistanceCurve(ref clip, out distanceCurve);

                var infoCurvesList = clipAnimation.curves.ToList();

                bool isInfoContainsDistanceCurve = false;

                for(int n=0; n < infoCurvesList.Count; n++)
                {
                    var infoCurve = infoCurvesList[n];

                    if (infoCurve.name == "DistanceCurve")
                    {
                        //infoCurve.curve = distanceCurve;

                        //infoCurvesList[n] = infoCurve;

                        isInfoContainsDistanceCurve = true;

                        //Debug.Log(clipAnimation.name + " already contains DistanceCurve");

                        break;
                    }
                }

                if (!isInfoContainsDistanceCurve)
                { 
                    ClipAnimationInfoCurve animationInfoCurve = new ClipAnimationInfoCurve();

                    animationInfoCurve.name = "DistanceCurve";

                    animationInfoCurve.curve = distanceCurve;

                    infoCurvesList.Add(animationInfoCurve);

                    //Debug.Log(clipAnimation.name + " new DistanceCurve!");
                }

                clipAnimation.curves = infoCurvesList.ToArray();

                importerClipAnimations[i] = clipAnimation;
            }
            else
            {
                Debug.LogWarning("animationClips does not contain " + clipAnimation.name);
            }
        }

        importer.clipAnimations = importerClipAnimations;
        
        AssetDatabase.WriteImportSettingsIfDirty(importer.assetPath);

        AssetDatabase.SaveAssets();
        
    }*/

    public void CreateDistanceCurve(ref AnimationClip clip, out AnimationCurve distanceCurve)
    {
        distanceCurve = new AnimationCurve();

        if (clip != null && (clip.hideFlags != (HideFlags.HideInHierarchy | HideFlags.NotEditable)))
        {
            var rootCurve = AnimationUtility.GetEditorCurve(clip, EditorCurveBinding.FloatCurve("", typeof(Animator), "RootT.z"));

            if (rootCurve == null)
            {
                //Debug.Log("no root curve found");
            }
            else
            {
                float startingValue = rootCurve[0].value;

                for (int i = 0; i < rootCurve.length; i++)
                {
                    distanceCurve.AddKey(rootCurve[i].time, Mathf.Abs(rootCurve[i].value - startingValue));
                }

                AnimationUtility.SetEditorCurve(clip, EditorCurveBinding.FloatCurve("", typeof(Animator), "DistanceCurve"), distanceCurve);
            }
        }
    }
}
